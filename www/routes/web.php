<?php

use App\Http\Controllers\Admin\OptionController;
use Illuminate\Support\Facades\App as Locale;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Page;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$locale = Locale::currentLocale();

Auth::routes(['register' => false]);
Route::get('logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::group(['prefix' => '{locale?}', 'middleware' => 'locale'], function ($locale) {
    Route::get('', [App\Http\Controllers\PageController::class, 'index'])->name('index');
    Route::get('/about', [App\Http\Controllers\PageController::class, 'about'])->name('about');
    Route::post('/about/review/add',
        [\App\Http\Controllers\PageController::class, 'createReview'])->name('review.create');
    Route::get('/download/{file}',
        [App\Http\Controllers\PageController::class, 'aboutGetFile'])->name('about.get-file');
    Route::get('/event', [App\Http\Controllers\PageController::class, 'event'])->name('event');

    Route::get('/mconsult', [App\Http\Controllers\PageController::class, 'mconsult'])->name('mconsult');
    Route::get('/registration-and-accreditation',
        [\App\Http\Controllers\PageController::class, 'registrationAccreditation'])->name('accreditation');
    Route::get('/relocation-services', [\App\Http\Controllers\PageController::class, 'relocation'])->name('relocation');
    Route::get('/travel-and-leisure', [\App\Http\Controllers\PageController::class, 'travel'])->name('travel');
    Route::get('/visa-support', [\App\Http\Controllers\PageController::class, 'visa'])->name('visa');
    Route::get('/{number}/inner',
        [App\Http\Controllers\PageController::class, 'mconsultInner'])->name('mconsult-inner');
    Route::get('/sale/{number}',
        [App\Http\Controllers\PageController::class, 'mconsultSale'])->name('mconsult.sale');
    Route::get('/news', [App\Http\Controllers\PageController::class, 'news'])->name('news');
    Route::get('/publication', [App\Http\Controllers\PageController::class, 'publication'])->name('publication');
    Route::get('/sale', [App\Http\Controllers\PageController::class, 'sale'])->name('sale');

    Route::group(['prefix' => '/inform-agency', 'as' => 'inform-agency.'], function ($locale) {
        Route::get('', [App\Http\Controllers\InformAgency\PageController::class, 'index'])->name('index');
        Route::get('/news', [App\Http\Controllers\InformAgency\PageController::class, 'news'])->name('news');
        Route::get('/publications',
            [App\Http\Controllers\InformAgency\PageController::class, 'publications'])->name('publications');
        Route::get('/events', [App\Http\Controllers\InformAgency\PageController::class, 'events'])->name('events');
        Route::get('/inform-agency/{type}/{agency}',
            [App\Http\Controllers\InformAgency\PageController::class, 'item'])->name('item');
    });
    Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('show.login');
});

Route::get('{locale?}/admin', function () {
    return redirect()->route('admin.main.index');
});

Route::get('/admin', function () {
    return redirect()->route('admin.main.index');
});

Route::group(['prefix' => '/admin', 'as' => 'admin.', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => '/main', 'as' => 'main.'], function () {
        Route::get('', [App\Http\Controllers\Admin\MainController::class, 'index'])->name('index');
        Route::post('text/store/{lang}',
            [App\Http\Controllers\Admin\MainController::class, 'textStore'])->name('store');
        Route::post('/clients/store',
            [App\Http\Controllers\Admin\MainController::class, 'clientStore'])->name('clients.store');
        Route::get('/clients/{client}',
            [App\Http\Controllers\Admin\MainController::class, 'clientEdit'])->name('clients.edit');
        Route::post('/clients/{client}/update',
            [App\Http\Controllers\Admin\MainController::class, 'clientUpdate'])->name('clients.update');
        Route::post('/clients/{client}/delete',
            [App\Http\Controllers\Admin\MainController::class, 'clientDelete'])->name('clients.delete');
    });

    Route::group(['prefix' => '/about-us', 'as' => 'about.'], function () {
        Route::get('', [App\Http\Controllers\Admin\AboutController::class, 'index'])->name('index');
        Route::post('/meta/store/{lang}',
            [App\Http\Controllers\Admin\AboutController::class, 'metaStore'])->name('meta.store');
        Route::group(['prefix' => '/reviews', 'as' => 'reviews.'], function () {
            Route::get('/{review}',
                [App\Http\Controllers\Admin\AboutController::class, 'reviewEdit'])->name('edit');
            Route::post('/{review}/update',
                [App\Http\Controllers\Admin\AboutController::class, 'reviewUpdate'])->name('update');
            Route::delete('/{review}/delete',
                [App\Http\Controllers\Admin\AboutController::class, 'reviewDelete'])->name('delete');
        });
        Route::group(['prefix' => '/team', 'as' => 'team.'], function () {
            Route::get('/{team}',
                [App\Http\Controllers\Admin\AboutController::class, 'teamEdit'])->name('edit');
            Route::post('/{team}/update',
                [App\Http\Controllers\Admin\AboutController::class, 'teamUpdate'])->name('update');
            Route::delete('/{team}/delete',
                [App\Http\Controllers\Admin\AboutController::class, 'teamDelete'])->name('delete');
        });
        Route::group(['prefix' => '/certificate', 'as' => 'certificate.'], function () {
            Route::get('/add',
                [App\Http\Controllers\Admin\AboutController::class, 'certificateAdd'])->name('add');
            Route::post('/store',
                [App\Http\Controllers\Admin\AboutController::class, 'certificateStore'])->name('store');
            Route::get('/{certificate}',
                [App\Http\Controllers\Admin\AboutController::class, 'certificateEdit'])->name('edit');
            Route::post('/{certificate}/update',
                [App\Http\Controllers\Admin\AboutController::class, 'certificateUpdate'])->name('update');
            Route::delete('/{certificate}/delete',
                [App\Http\Controllers\Admin\AboutController::class, 'certificateDelete'])->name('delete');
        });
    });

    Route::group(['prefix' => '/'.Page::PAGE_SRVM, 'as' => 'srvm.'], function () {
        Route::get('', [App\Http\Controllers\Admin\SRVMController::class, 'index'])->name('index');
    });;
    Route::group(['prefix' => '/'.Page::PAGE_VISA_SUPPORT, 'as' => 'visa.'], function () {
        Route::get('',
            [App\Http\Controllers\Admin\VisaSupportController::class, 'index'])->name('index');
        Route::get('{country}/edit',
            [\App\Http\Controllers\Admin\CountryController::class, 'edit'])->name('country.edit');
        Route::post('{country}/update',
            [\App\Http\Controllers\Admin\CountryController::class, 'update'])->name('country.update');
        Route::post('country/store',
            [\App\Http\Controllers\Admin\CountryController::class, 'store'])->name('country.store');
    });

    Route::group(['prefix' => '/'.Page::PAGE_RELOCATION, 'as' => 'relocation.'], function () {
        Route::get('',
            [App\Http\Controllers\Admin\RelocationController::class, 'index'])->name('index');
    });

    Route::group(['prefix' => '/'.Page::PAGE_REGISTRY_ACCREDITATION, 'as' => 'accreditation.'], function () {
        Route::get('',
            [App\Http\Controllers\Admin\RegitstryAccreditationController::class, 'index'])->name('index');
    });

    Route::group(['prefix' => '/'.Page::PAGE_TRAVEL_RELAXATION, 'as' => 'travel.'], function () {
        Route::get('',
            [App\Http\Controllers\Admin\TravelRelaxationController::class, 'index'])->name('index');
    });

    Route::group(['prefix' => '/'.Page::PAGE_INFORM_AGENCY, 'as' => 'agency.'], function () {
        Route::get('', [App\Http\Controllers\Admin\InformAgencyController::class, 'index'])->name('index');
        Route::get('{number}', [\App\Http\Controllers\Admin\InformAgencyController::class, 'edit'])->name('edit');
        Route::post('{agency}/update',
            [\App\Http\Controllers\Admin\InformAgencyController::class, 'update'])->name('update');
        Route::post('{number}/image/update',
            [\App\Http\Controllers\Admin\InformAgencyController::class, 'updateImage'])->name('update.image');
        Route::get('{number}/{type}/add',
            [\App\Http\Controllers\Admin\InformAgencyController::class, 'add'])->name('add');
        Route::post('create', [\App\Http\Controllers\Admin\InformAgencyController::class, 'create'])->name('create');
    });

    Route::group(['prefix' => '/options', 'as' => 'option.'], function () {
        Route::get('', [App\Http\Controllers\Admin\OptionController::class, 'index'])->name('index');
        Route::get('{key}/edit', [App\Http\Controllers\Admin\OptionController::class, 'edit'])->name('edit');

        Route::post('{key}/update', [App\Http\Controllers\Admin\OptionController::class, 'update'])->name('update');
    });

    Route::get('/information-support',
        [App\Http\Controllers\Admin\PageController::class, 'informationSupport'])->name('information-support');
    Route::get('/luxembourg', [App\Http\Controllers\Admin\PageController::class, 'luxembourg'])->name('luxembourg');
    Route::get('/package-services',
        [App\Http\Controllers\Admin\PageController::class, 'packageServices'])->name('package-services');
    Route::get('/relocation', [App\Http\Controllers\Admin\PageController::class, 'relocation'])->name('relocation');
    Route::get('/sale', [App\Http\Controllers\Admin\PageController::class, 'sale'])->name('sale');
    Route::get('/special-offers',
        [App\Http\Controllers\Admin\PageController::class, 'specialOffers'])->name('special-offers');
    Route::get('/special-offers-visa',
        [App\Http\Controllers\Admin\PageController::class, 'specialOffersVisa'])->name('special-offers-visa');
    Route::get('/specialists',
        [App\Http\Controllers\Admin\PageController::class, 'specialists'])->name('specialists');
    Route::get('/specialists-vcs',
        [App\Http\Controllers\Admin\PageController::class, 'specialistsVCS'])->name('specialists-vcs');
    Route::get('/vip', [App\Http\Controllers\Admin\PageController::class, 'vip'])->name('vip');

    Route::post('/{page}/store/{lang}',
        [App\Http\Controllers\Admin\MethodController::class, 'store'])->name('page.store');
    Route::post('/{page}/image/store',
        [\App\Http\Controllers\Admin\MethodController::class, 'storeImage'])->name('page.store.image');
    Route::post('/store/interaction/{interaction}',
        [App\Http\Controllers\Admin\MethodController::class, 'storeInteraction'])->name('page.store.interaction');

    Route::group(['prefix' => '', 'as' => 'service-type.'], function () {
        Route::get('{page}/{category}',
            [App\Http\Controllers\Admin\Services\ServiceTypeController::class, 'index'])->name('index');
        Route::post('{serviceType}/store',
            [\App\Http\Controllers\Admin\Services\ServiceTypeController::class, 'update'])->name('update');
        Route::get('{page}={category}\service',
            [App\Http\Controllers\Admin\Services\ServiceTypeController::class, 'add'])->name('add');
        Route::get('{page}={category}\mia',
            [App\Http\Controllers\Admin\Services\ServiceTypeController::class, 'addMIA'])->name('add.mia');
        Route::delete('{service}/delete',
            [\App\Http\Controllers\Admin\Services\ServiceTypeController::class, 'delete'])->name('delete');
        Route::post('service/add/store',
            [\App\Http\Controllers\Admin\Services\ServiceTypeController::class, 'store'])->name('store');

        Route::group(['prefix' => '', 'as' => 'sale.'], function () {
            Route::get('sale={page}',
                [App\Http\Controllers\Admin\Services\SaleController::class, 'index'])->name('index');
            Route::get('{sale}',
                [\App\Http\Controllers\Admin\Services\SaleController::class, 'edit'])->name('edit');
            Route::get('sale={page}/new/sale',
                [\App\Http\Controllers\Admin\Services\SaleController::class, 'add'])->name('add');
            Route::post('sale/add/store',
                [\App\Http\Controllers\Admin\Services\SaleController::class, 'store'])->name('store');
            Route::post('{sale}/update',
                [\App\Http\Controllers\Admin\Services\SaleController::class, 'update'])->name('update');
            Route::post('{number}/update/image',
                [\App\Http\Controllers\Admin\Services\SaleController::class, 'updateImage'])->name('update.image');
        });
    });


    Route::group(['prefix' => 'service/', 'as' => 'service.'], function () {
        Route::get('{serviceType}/{number}',
            [App\Http\Controllers\Admin\Services\ServiceController::class, 'edit'])->name('edit');
        Route::post('{service}/update',
            [\App\Http\Controllers\Admin\Services\ServiceController::class, 'update'])->name('update');
        Route::post('{number}/image/update',
            [\App\Http\Controllers\Admin\Services\ServiceController::class, 'updateImage'])->name('update.image');
    });

    Route::group(['prefix' => 'mia/', 'as' => 'mia.'], function () {
        Route::get('{serviceType}/{number}',
            [App\Http\Controllers\Admin\Services\MIAController::class, 'edit'])->name('edit');
        Route::post('/{mia}/update',
            [\App\Http\Controllers\Admin\Services\MIAController::class, 'update'])->name('update');
        Route::post('{number}/image/update',
            [\App\Http\Controllers\Admin\Services\MIAController::class, 'updateImage'])->name('update.image');
        Route::delete('{mia}/delete',
            [\App\Http\Controllers\Admin\Services\MIAController::class, 'delete'])->name('delete');
        Route::post('mia/add/store',
            [\App\Http\Controllers\Admin\Services\MIAController::class, 'store'])->name('store');
    });

    Route::group(['prefix' => 'variation/', 'as' => 'variation.'], function () {
        Route::get('{serviceType}/{variation}',
            [App\Http\Controllers\Admin\Services\VariationController::class, 'index'])->name('index');
        Route::post('{variation}/update',
            [\App\Http\Controllers\Admin\Services\VariationController::class, 'update'])->name('update');
    });

});
