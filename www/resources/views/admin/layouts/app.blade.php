<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}"/>
    <base href="../"/>
    <title>Main</title>
    <link rel="stylesheet" href="{{ asset('libs/semantic/dist/semantic.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          type="text/css"/>
    <link rel="stylesheet" href="{{ asset('libs/text-editor/richtext.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}" type="text/css"/>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <meta name="release" content="GUI - 0.5UI"/>
</head>
<body>
@yield('content')

<div class="ui modal" id="errorModal">
    <div class="header">Похоже возникла ошибка</div>
    <div class="content">
        @if($errors->any())
            @foreach ($errors->all() as $error)
                <p>- {{ $error }}</p>
            @endforeach
        @endif
    </div>
</div>
<script src="{{ asset('libs/semantic/dist/semantic.min.js') }}"></script>
<script src="{{ asset('libs/text-editor/jquery.richtext.min.js') }}"></script>
<script src="{{ asset('js/admin-main.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        @if($errors->any())
        $('#errorModal').modal('show')
        @endif
    });
</script>
</body>
</html>
