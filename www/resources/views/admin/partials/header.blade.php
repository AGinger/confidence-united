<header class="ui header header__top"><a class="header_back" href="/">< назад </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
        <a class="header_admin"
           href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">administrator</a>
    </form>
</header>
