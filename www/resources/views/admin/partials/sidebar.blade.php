<aside class="sidebar">
    <div class="ui sidebar inverted vertical menu ui visible sidebar">
        <div class="item">
            <div class="sidebar__logo"><img src="{{ asset('images/logo-main-sidebar.svg') }}" alt="logo"/></div>
        </div>
        <div class="item"><a class="item active" href="{{ route('admin.main.index') }}">Главная</a><a class="item"
                                                                                                      href="{{ route('admin.about.index') }}">О
                компании</a></div>
        <div class="item">
            <div class="subtitle">УСЛУГИ</div>
            <a class="item" href="{{ route('admin.srvm.index') }}"> Миграционный консалтинг</a>
            <div class="menu">
                <a class="item"
                   href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'vks']) }}">ВКС</a>
                <a class="item"
                   href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'vig-op']) }}">
                    Визовые иностранные граждане, общий порядок</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'bvig-op']) }}"
                   class="item">
                    Безвизовые иностранные граждане, общий порядок</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'akkreditovannye-sotrudniki']) }}"
                   class="item">
                    Аккредитованные сотрудники</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'v-ramkah-soglashenij-s-franciej-i-yuzhnoj-koreej']) }}"
                   class="item">
                    В рамках соглашений с Францией и Южной Кореей</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'razreshenie-na-vremennoe-prozhivanie']) }}"
                   class="item">Разрешение на временное проживание</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'vid-na-zhitelstvo']) }}"
                   class="item">
                    Вид на жительство</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_SRVM, 'category' => 'prochie']) }}"
                   class="item">Прочие</a>
                <a class="item"
                   href="{{ route('admin.service-type.sale.index', ['page' => \App\Models\Page::PAGE_SRVM]) }}">
                    Специальные предложения и акции</a>
            </div>
        </div>
        <div class="item">
            <a class="item" href="{{ route('admin.visa.index') }}">ВИЗОВАЯ ПОДДЕРЖКА</a>
            <div class="menu">
                <a href="{{ route('admin.service-type.sale.index', ['page' => \App\Models\Page::PAGE_VISA_SUPPORT]) }}" class="item">
                    Специальные предложения и акции</a>
            </div>
        </div>
        <div class="item">
            <a class="item" href="{{ route('admin.relocation.index') }}">Релокационные услуги</a>
            <div class="menu">
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_RELOCATION, 'category' => 'paketnye-uslugi']) }}"
                   class="item">
                    Пакетные услуги</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_RELOCATION, 'category' => 'otdelnye-servisy']) }}"
                   class="item">
                    Отдельные сервисы</a>
                <a href="{{ route('admin.service-type.sale.index', ['page' => \App\Models\Page::PAGE_RELOCATION]) }}" class="item">
                    Специальные предложения и акции</a>
            </div>
        </div>
        <div class="item">
            <a class="item" href="{{ route('admin.accreditation.index') }}">Регистрация и аккредитация</a>
            <div class="menu">
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_REGISTRY_ACCREDITATION,
                    'category' => 'akkreditaciya-predstavitelstvi-filialov-inostrannyh-kompanij']) }}"
                   class="item">Аккредитация представительств и филиалов иностранных компаний</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_REGISTRY_ACCREDITATION, 'category' => 'registraciya-rossijskih-kompanij']) }}"
                   class="item">Регистрация российских компаний</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_REGISTRY_ACCREDITATION, 'category' => 'prochie-uslugi']) }}"
                   class="item">
                    Прочие услуги</a>
                <a href="{{ route('admin.service-type.sale.index', ['page' => \App\Models\Page::PAGE_REGISTRY_ACCREDITATION]) }}"
                   class="item">Специальные предложения и акции</a></div>
        </div>
        <div class="item">
            <a class="item" href="{{ route('admin.travel.index') }}">Путешествия и отдых</a>
            <div class="menu">
                <a href="{{ route('admin.service-type.sale.index', ['page' => \App\Models\Page::PAGE_TRAVEL_RELAXATION]) }}"
                   class="item">Специальные предложения и акции</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_TRAVEL_RELAXATION, 'category' => 'turi']) }}"
                   class="item">Туры</a>
                <a href="{{ route('admin.service-type.index', ['page' => \App\Models\Page::PAGE_TRAVEL_RELAXATION, 'category' => 'otdelnye-servisy']) }}"
                   class="item">Отдельные сервисы</a>
            </div>
        </div>
        <div class="item">
            <a href="{{ route('admin.agency.index') }}" class="item">ИНФОРМАГЕНТСТВО</a>
        </div>
        <div class="item">
            <a href="{{ route('admin.option.index') }}" class="item">Настройки Мета-тегов</a>
        </div>

    </div>
</aside>
