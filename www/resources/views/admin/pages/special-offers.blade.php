@extends('admin.layouts.app')

@section('content')
@include('admin.partials.sidebar')
<article class="article">
    @include('admin.partials.header')
    <div class="header__sub">
        <div class="sub header">тип услуг</div>
        <h1 class="ui header">СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ И АКЦИИ</h1>
        <h2 class="sub header"></h2>
        <div class="ui top attached tabular menu">
            <div class="item" data-tab="0">Русский</div>
            <div class="item" data-tab="1">Английский</div>
            <div class="item" data-tab="2">Текст</div>
            <div class="item" data-tab="3">Клиенты</div>
        </div>
    </div>
</article>
<main class="page">
    <section class="main">
        <div class="ui bottom attached tab segment active" data-tab="0">
            <div class="sub header">Управляйте последовательностью вывода перетаскиванием строк.</div>
            <button class="ui blue button">Добавить</button>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th>№</th>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-label="Name">1</td>
                    <td data-label="Age">5</td>
                    <td data-label="Job">Скидка для членов Американской Торговой Палаты в РФ</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">2</td>
                    <td data-label="Age">45</td>
                    <td data-label="Job">Скидка 25% на все миграционные и визовые услуги на протяжении всего 2020-го года</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">3</td>
                    <td data-label="Age">134</td>
                    <td data-label="Job">Скидка 15% на оформление виз</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">4</td>
                    <td data-label="Age">85</td>
                    <td data-label="Job">Льготное оформление въездных виз</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">5</td>
                    <td data-label="Age">24</td>
                    <td data-label="Job">Скидка 30% на льготное оформление разрешение на въезд</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">6</td>
                    <td data-label="Age">247</td>
                    <td data-label="Job">Скидка 10% на разрешение на работу для нерезидентов РФ</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui bottom attached tab segment" data-tab="1">
            <p>1</p>
        </div>
    </section>
</main>
@endsection
