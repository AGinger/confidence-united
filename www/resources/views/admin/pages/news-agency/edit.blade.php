@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">изменение</div>
            <h1 class="ui header">ИЗМЕНЕНИЕ ДАННЫХ</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active">
                <div class="sub header">Изменение данных.</div>
                <form action="{{ route('admin.agency.update', ['agency' => $agency_ru->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="name">{{ $agency_ru->name }}</textarea>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $agency_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.agency.update', ['agency' => $agency_ru->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header header__subheader">Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $agency_ru->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $agency_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $agency_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>

                <img class="page__img" src="{{ asset($agency_ru->background_image) }}" alt=""/>
                <form action="{{ route('admin.agency.update.image', ['number' => $agency_ru->number]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл"/>
                        <input type="file" name="background_image" required/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                    <button type="submit" class="ui blue button">Сохранить</button>
                </form>

                <form action="{{ route('admin.agency.update', ['agency' => $agency_eng->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header">Содержание английской версии</h3>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="name">{{ $agency_eng->name }}</textarea>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $agency_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.agency.update', ['agency' => $agency_eng->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header header__subheader">Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $agency_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $agency_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $agency_eng->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>

                <img class="page__img" src="{{ asset($agency_eng->background_image) }}" alt=""/>
                <form action="{{ route('admin.agency.update.image', ['number' => $agency_ru->number]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл"/>
                        <input type="file" name="background_image" required/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                    <button type="submit" class="ui blue button">Сохранить</button>
                </form>
            </div>
        </section>
    </main>
@endsection
