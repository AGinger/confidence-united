@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">раздел</div>
            <h1 class="ui header">Информационное агентство</h1>
            <h2 class="sub header">МИГРАЦИОННЫЙ КОНСАЛТИНГ / СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ И АКЦИИ</h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Страница</div>
                <div class="item" data-tab="2">Новости</div>
                <div class="item" data-tab="1">Публикации</div>
                <div class="item" data-tab="3">Мероприятия</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="srvm">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_ru->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__subheader">Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_ru->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_eng->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__subheader">Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_eng->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="1">
                <h3 class="ui header">Публикации</h3>
                <a href="{{ route('admin.agency.add', ['number' => 1, 'type' => 'publications']) }}" class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($publications as $publication)
                        <tr>
                            <td data-label="Count">{{ $publication->number }}</td>
                            <td data-label="id">{{ $publication->id }}</td>
                            <td data-label="Name">{{ $publication->name }}</td>
                            <td data-label="Date">{{ $publication->updated_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.agency.edit', ['number' => $publication->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_nolink"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <h3 class="ui header">Новости</h3>
                <a href="{{ route('admin.agency.add', ['number' => 1, 'type' => 'news']) }}" class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $item)
                        <tr>
                            <td data-label="Count">{{ $item->number }}</td>
                            <td data-label="id">{{ $item->id }}</td>
                            <td data-label="Name">{{ $item->name }}</td>
                            <td data-label="Date">{{ $item->updated_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.agency.edit', ['number' => $item->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_nolink"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="3">
                <h3 class="ui header">Мероприятия</h3>
                <a href="{{ route('admin.agency.add', ['number' => 1, 'type' => 'events']) }}" class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($events as $event)
                        <tr>
                            <td data-label="Count">{{ $event->number }}</td>
                            <td data-label="id">{{ $event->id }}</td>
                            <td data-label="Name">{{ $event->name }}</td>
                            <td data-label="Date">{{ $event->updated_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.agency.edit', ['number' => $event->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_nolink"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
