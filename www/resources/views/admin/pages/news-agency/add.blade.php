@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">Создание данных</div>
            <h1 class="ui header">Создание данных</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active">
                <div class="sub header">Создание данных.</div>
                <form action="{{ route('admin.agency.create') }}" method="POST">
                    @csrf
                    <h3 class="ui header">Содержание русской версии</h3>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="name_ru" required></textarea>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description_ru" required></textarea>
                    </div>
                    <h3 class="ui header header__subheader">Метаданные</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta_ru" required/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta_ru" required/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta_ru" required></textarea>
                    </div>

                    <h3 class="ui header">Содержание английской версии</h3>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="name_eng" required></textarea>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description_eng" required></textarea>
                    </div>
                    <h3 class="ui header header__subheader">Метаданные</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta_eng" required/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta_eng" required/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta_eng" required></textarea>
                    </div>
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл"/>
                        <input type="file" name="background_image_eng" required/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>

                    <div class="ui input">
                        <input type="text" name="type" value="{{ $type }}" style="display: none"/>
                    </div>

                    <button type="submit" class="ui blue button">Добавить</button>
                </form>
            </div>
        </section>
    </main>
@endsection
