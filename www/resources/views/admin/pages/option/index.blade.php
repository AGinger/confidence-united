@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">раздел</div>
            <h1 class="ui header">Настройки Мета-тегов</h1>
            <h2 class="sub header">НАСТРОЙКИ / МЕТА-ТЕГИ</h2>
        </div>
    </article>
    <main class="page">
        <section>
            <div class="ui bottom">
                <h3 class="ui header">Мета-теги</h3>
                <table class="ui celled table">
                    <thead>
                        <tr>
                            <th>Key</th>
                            <th>Название</th>
                            <th>Значение</th>
                            <th>Описание</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($metaOptions as $metaOption)
                            <tr>
                                <td data-label="Key">{{ $metaOption->key }}</td>
                                <td data-label="Name">{{ $metaOption->title }}</td>
                                <td data-label="Value">{{ $metaOption->value }}</td>
                                <td data-label="Description">{{ $metaOption->description }}</td>
                                <td data-label="Action">
                                    <a href="{{ route('admin.option.edit', ['key' => $metaOption->key]) }}"
                                       class="page__icon page__icon_pen"></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
