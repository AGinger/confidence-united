@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">изменение</div>
            <h1 class="ui header">ИЗМЕНЕНИЕ ДАННЫХ {{ $option->key }}</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active">
                <div class="sub header">Изменение данных .</div>
                <form action="{{ route('admin.option.update', ['key' => $option->key]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название*</h4>
                        <div class="sub header header__subheader">Не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title" value="{{ old('title', $option->title) }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Значение*</h4>
                        <div class="sub header header__subheader">Не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="value" value="{{ old('value', $option->value) }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание*</h4>
                        <div class="sub header header__subheader">Не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="description" value="{{ old('description', $option->description) }}"/>
                        </div>
                    </div>

                    <button type="submit" class="ui blue button">Сохранить</button>
                </form>
            </div>
        </section>
    </main>
@endsection
