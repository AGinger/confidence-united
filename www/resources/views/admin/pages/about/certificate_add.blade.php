@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">создание сертификата</div>
            <h1 class="ui header">СОЗДАНИЕ СЕРТИФИКАТА</h1>
            <h2 class="sub header">
                <span>
                    <a href="{{ route('admin.about.index') }}">О КОМПАНИИ</a>
                    /
                    СЕРТИФИКАТЫ
                    /
                    ДОБАВЛЕНИЕ СЕРТИФИКАТА
                </span>
            </h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <form action="{{ route('admin.about.certificate.store') }}"
                  method="POST"
                  enctype="multipart/form-data">
                @csrf
                <h3 class="ui header header__title">Содержание</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle page__icon_question">Имя автора</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="name" value="" required/>
                    </div>
                </div>
                <img class="page__img" src="" alt=""/>
                <div class="sub header header__subheader">Иллюстрацию вписать в габариты 230x153</div>
                <div class="page-block-titleBtnText__btns">
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл"/>
                        <input type="file" name="image" required/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                    <button type="submit" class="ui blue button">Создать</button>
                </div>
                </div>
            </form>
        </section>
    </main>
@endsection
