@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">изменение работника</div>
            <h1 class="ui header">ИЗМЕНЕНИЕ РАБОТНИКА {{ $team->name_ru }} ({{ $team->name_eng }})</h1>
            <h2 class="sub header">
                <span>
                    <a href="{{ route('admin.about.index') }}">О КОМПАНИИ</a>
                    /
                    КОМАНДА
                    /
                    ИЗМЕНЕНИЕ РАБОТНИКА {{ $team->name_ru }} ({{ $team->name_eng }})
                </span>
            </h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <form action="{{ route('admin.about.team.update', ['team' => $team->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <h3 class="ui header header__title">Содержание</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle page__icon_question">Имя автора</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="name_ru" value="{{ $team->name_ru }}"/>
                    </div>
                </div>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle page__icon_question">Имя автора</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="name_eng" value="{{ $team->name_eng }}"/>
                    </div>
                </div>
                <img class="page__img" src="{{ asset($team->image_first) }}" alt=""/>
                <img class="page__img" src="{{ asset($team->image_second) }}" alt=""/>
                <div class="sub header header__subheader">Иллюстрацию вписать в габариты 230x153</div>
                <div class="page-block-titleBtnText__btns">
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл"/>
                        <input type="file" value="{{ $team->image_first }}" name="image_first"/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                    <div class="sub header header__subheader">Иллюстрацию вписать в габариты 230x153</div>
                    <div class="page-block-titleBtnText__btns">
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл"/>
                            <input type="file" name="image_second"/>
                            <a class="ui blue button fileUpload">Выбрать</a>
                        </div>
                        <button type="submit" class="ui blue button">Обновить</button>
                    </div>
                </div>
            </form>
        </section>
    </main>
@endsection
