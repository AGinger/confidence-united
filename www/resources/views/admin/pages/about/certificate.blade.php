@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">изменение сертификата</div>
            <h1 class="ui header">ИЗМЕНЕНИЕ СЕРТИФИКАТА {{ $certificate->name }}</h1>
            <h2 class="sub header">
                <span>
                    <a href="{{ route('admin.about.index') }}">О КОМПАНИИ</a>
                    /
                    СЕРТИФИКАТЫ
                    /
                    ИЗМЕНЕНИЕ СЕРТИФИКАТА {{ $certificate->name }}
                </span>
            </h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <form action="{{ route('admin.about.certificate.update', ['certificate' => $certificate->id]) }}"
                  method="POST"
                  enctype="multipart/form-data">
                @csrf
                <h3 class="ui header header__title">Содержание</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle page__icon_question">Имя автора</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ $certificate->name }}"/>
                    </div>
                </div>
                <img class="page__img" src="{{ asset($certificate->image) }}" alt=""/>
                <div class="sub header header__subheader">Иллюстрацию вписать в габариты 230x153</div>
                <div class="page-block-titleBtnText__btns">
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл"/>
                        <input type="file" name="image"/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                    <button type="submit" class="ui blue button">Обновить</button>
                </div>
                </div>
            </form>
        </section>
    </main>
@endsection
