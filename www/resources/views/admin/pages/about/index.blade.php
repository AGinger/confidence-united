@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">раздел</div>
            <h1 class="ui header">О компании</h1>
            <h2 class="sub header"></h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Русский</div>
                <div class="item" data-tab="1">Английский</div>
                <div class="item" data-tab="2">Отзывы рус.</div>
                <div class="item" data-tab="3">Отзывы анг.</div>
                <div class="item" data-tab="4">Команда</div>
                <div class="item" data-tab="5">Сертификт.</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="about">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle page__icon_question">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_ru->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title"> Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_ru->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.about.meta.store', ['lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Анонс (рус.)</h4>
                        <div class="sub header header__subheader">386 символов максимум включая пробелы</div>
                        <textarea class="ui segment textEditor" name="announce">{{ $meta_ru->announce }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.about.meta.store', ['lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Код видео русской версии</h4>
                        <div class="sub header header__subheader">Код из стороннего сервиса для внедрения в страницу,
                            разное
                            видео для русской и английской версий
                        </div>
                        <textarea class="ui segment textEditor" name="video_link">{{ $meta_ru->video_link }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.about.meta.store', ['lang' => $page_ru->language]) }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <h4 class="ui header">Презентация для ссылки с кнопки</h4>
                    <div class="page-block-titleBtnText">
                        <div class="sub header header__subheader">Желателен формат PDF и объём до 3 Мб.</div>
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл" readonly=""/>
                            <input type="file" name="presentation"/>
                            <a class="ui blue button fileUpload">Выбрать</a>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="1">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle page__icon_question">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_eng->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title"> Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_eng->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.about.meta.store', ['lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Анонс (англ.)</h4>
                        <div class="sub header header__subheader">386 символов максимум включая пробелы</div>
                        <textarea class="ui segment textEditor" name="announce">{{ $meta_eng->announce }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.about.meta.store', ['lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Код видео английской версии)</h4>
                        <div class="sub header header__subheader">Код из стороннего сервиса для внедрения в страницу,
                            разное
                            видео для русской и английской версий
                        </div>
                        <textarea class="ui segment textEditor" name="video_link">{{ $meta_eng->video_link }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.about.meta.store', ['lang' => $page_eng->language]) }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <h4 class="ui header">Презентация для ссылки с кнопки</h4>
                    <div class="page-block-titleBtnText">
                        <div class="sub header header__subheader">Желателен формат PDF и объём до 3 Мб.</div>
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл" readonly=""/>
                            <input type="file" name="presentation"/>
                            <button class="ui blue button fileUpload">Выбрать</button>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th>Автор</th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews_ru as $review)
                        <tr>
                            <td data-label="Count">{{ $review->id }}</td>
                            <td data-label="Name">{{ $review->author }}</td>
                            <td data-label="Text">{{ $review->description }}</td>
                            <td data-label="Action" style="display: flex; justify-content: center">
                                <a href="{{ route('admin.about.reviews.edit', ['review' => $review->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <form action="{{ route('admin.about.reviews.delete', ['review' => $review->id]) }}"
                                      method="POST" style="display: none">
                                    @csrf
                                    @method('delete')
                                    <a type="submit" class="page__icon page__icon_basket"></a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="3">
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th>Автор</th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reviews_eng as $review)
                        <tr>
                            <td data-label="Count">{{ $review->id }}</td>
                            <td data-label="Name">{{ $review->author }}</td>
                            <td data-label="Text">{{ $review->description }}</td>
                            <td data-label="Action" style="display: flex; justify-content: center">
                                <a href="{{ route('admin.about.reviews.edit', ['review' => $review->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <form action="{{ route('admin.about.reviews.delete', ['review' => $review->id]) }}"
                                      method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="page__icon page__icon_basket"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="4">
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <div class="sub header header__subheader">Строго 225х249</div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Имя русск.</th>
                        <th>Имя англ.</th>
                        <th>Фото 1.</th>
                        <th>Фото 2.</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($team as $person)
                        <tr>
                            <td data-label="Count">{{ $person->id }}</td>
                            <td data-label="NameRu">{{ $person->name_ru }}</td>
                            <td data-label="NameEn">{{ $person->name_eng }}</td>
                            <td data-label="img1"><img src="{{ asset($person->image_first) }}"/></td>
                            <td data-label="img2"><img src="{{ asset($person->image_second) }}"/></td>
                            <td data-label="Action" style="display: flex; justify-content: center">
                                <a href="{{ route('admin.about.team.edit', ['team' => $person->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <form action="{{ route('admin.about.team.delete', ['team' => $person->id]) }}"
                                      method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="page__icon page__icon_basket"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="5">
                <div class="sub header header__subheader">Строго 225х249</div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Имя</th>
                        <th>Фото 1.</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($certificates as $certificate)
                        <tr>
                            <td data-label="Count">{{ $certificate->id }}</td>
                            <td data-label="Name">{{ $certificate->name }}</td>
                            <td data-label="img"><img src="{{ asset($certificate->image) }}"/></td>
                            <td data-label="Action" style="display: flex; justify-content: center">
                                <a href="{{ route('admin.about.certificate.edit', ['certificate' => $certificate->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <form
                                    action="{{ route('admin.about.certificate.delete', ['certificate' => $certificate->id]) }}"
                                    method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="page__icon page__icon_basket"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="{{ route('admin.about.certificate.add') }}" class="ui blue button">+ Добавить</a>
            </div>
        </section>
    </main>
@endsection
