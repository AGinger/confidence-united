@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">изменение отзыва</div>
            <h1 class="ui header">ИЗМЕНЕНИЕ ОТЗЫВА {{ $review->author }}</h1>
            <h2 class="sub header">
                <span>
                    <a href="{{ route('admin.about.index') }}">О КОМПАНИИ</a>
                    /
                    ОТЗЫВЫ
                    /
                    ИЗМЕНЕНИЕ ОТЗЫВА {{ $review->author }}
                </span>
            </h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <form action="{{ route('admin.about.reviews.update', ['review' => $review->id]) }}" method="POST">
                @csrf
                <h3 class="ui header header__title">Содержание</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle page__icon_question">Имя автора</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="author" value="{{ $review->author }}"/>
                    </div>
                </div>
                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Отзыв</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <textarea class="ui segment textEditor"
                              name="description">{{ $review->description }}</textarea>
                    <button type="submit" class="ui blue button">Обновить</button>
                </div>
            </form>
        </section>
    </main>
@endsection
