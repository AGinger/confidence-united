@extends('admin.layouts.app')

@section('content')
@include('admin.partials.sidebar')
<article class="article">
    @include('admin.partials.header')
    <div class="header__sub">
        <div class="sub header">тип услуг</div>
        <h1 class="ui header">СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ И АКЦИИ</h1>
        <h2 class="sub header"></h2>
        <div class="ui top attached tabular menu">
            <div class="item" data-tab="0"></div>
            <div class="item" data-tab="1"></div>
            <div class="item" data-tab="2"></div>
            <div class="item" data-tab="3"></div>
            <div class="item" data-tab="4"></div>
            <div class="item" data-tab="5"></div>
            <div class="item" data-tab="6"></div>
            <div class="item" data-tab="7"></div>
            <div class="item" data-tab="8"></div>
            <div class="item" data-tab="9"></div>
            <div class="item" data-tab="10"></div>
            <div class="item" data-tab="11"></div>
            <div class="item" data-tab="12"></div>
            <div class="item" data-tab="13"></div>
            <div class="item" data-tab="14"></div>
            <div class="item" data-tab="15"></div>
            <div class="item" data-tab="16"></div>
        </div>
    </div>
</article>
<main class="page">
    <section class="main">
        <div class="ui bottom attached tab segment active" data-tab="0">
            <div class="sub header">Управляйте последовательностью вывода перетаскиванием строк.</div>
            <button class="ui blue button">Добавить</button>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th>№</th>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td data-label="Name">1</td>
                    <td data-label="Age">5</td>
                    <td data-label="Job">Информационная поддержка 1</td>
                    <td data-label="Job">12.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">2</td>
                    <td data-label="Age">45</td>
                    <td data-label="Job">Информационная поддержка 2</td>
                    <td data-label="Job">16.08.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">3</td>
                    <td data-label="Age">134</td>
                    <td data-label="Job">Информационная поддержка 3</td>
                    <td data-label="Job">5.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">4</td>
                    <td data-label="Age">85</td>
                    <td data-label="Job">Информационная поддержка 4</td>
                    <td data-label="Job">6.08.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">5</td>
                    <td data-label="Age">24</td>
                    <td data-label="Job">Информационная поддержка 5</td>
                    <td data-label="Job">22.08.20</td>
                    <td data-label="Job"></td>
                </tr>
                <tr>
                    <td data-label="Name">6</td>
                    <td data-label="Age">247</td>
                    <td data-label="Job">Информационная поддержка 6</td>
                    <td data-label="Job">3.07.20</td>
                    <td data-label="Job"></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="ui bottom attached tab segment" data-tab="1">
            <p>1</p>
        </div>
    </section>
</main>
@endsection
