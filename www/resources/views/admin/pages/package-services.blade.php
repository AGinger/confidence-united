@extends('admin.layouts.app')

@section('content')
@include('admin.partials.sidebar')
<article class="article">
    @include('admin.partials.header')
    <div class="header__sub">
        <div class="sub header">тип услуг</div>
        <h1 class="ui header">Пакетные услуги</h1>
        <h2 class="sub header">Релокационные услуги</h2>
        <div class="ui top attached tabular menu">
            <div class="item" data-tab="0">Содержание</div>
            <div class="item" data-tab="1">МИА</div>
        </div>
    </div>
</article>
<main class="page">
    <section class="main">
        <div class="ui bottom attached tab segment active" data-tab="0">
            <h3 class="ui header">Содержание</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Название русское</h4>
                <div class="sub header header__subheader">48 знаков максимум</div>
                <div class="ui input">
                    <input type="text" placeholder="Пакетные услуги"/>
                </div>
            </div>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Название английское</h4>
                <div class="sub header header__subheader">48 знаков максимум</div>
                <div class="ui input">
                    <input type="text" placeholder="Package services"/>
                </div>
                <button class="ui blue button">Сохранить</button>
            </div>
            <h4 class="ui header">Пакеты</h4>
            <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в слайдах можно перетаскиванием строк в таблицах.</div>
            <button class="ui blue button">+ Добавить</button>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th> №</th>
                    <th> ID</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                <tbody>
                <tr>
                    <td data-label="Count">0</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">ВИП</td>
                    <td data-label="date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_basket"></button>
                    </td>
                </tr>
                <tr>
                    <td data-label="Count">1</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Бизнес</td>
                    <td data-label="date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_basket"></button>
                    </td>
                </tr>
                <tr>
                    <td data-label="Count">2</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Стандарт</td>
                    <td data-label="date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_basket"></button>
                    </td>
                </tr>
                <tr>
                    <td data-label="Count">3</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Базовый</td>
                    <td data-label="date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_basket"></button>
                    </td>
                </tr>
                </tbody>
                </thead>
            </table>
        </div>
        <div class="ui bottom attached tab segment" data-tab="1">
            <h4 class="ui header">Привязка материалов информационного агентства (МИА)</h4>
            <div class="page-block-select page-block-select_2">
                <div class="ui basic floating dropdown button">
                    <div class="text">Слайд 1</div><i class="dropdown icon"></i>
                    <div class="menu">
                        <div class="item">Слайд 2</div>
                        <div class="item">Слайд 3</div>
                    </div>
                </div>
                <div class="ui basic floating dropdown button">
                    <div class="text">Слайд 1</div><i class="dropdown icon"></i>
                    <div class="menu">
                        <div class="item">Слайд 2</div>
                        <div class="item">Слайд 3</div>
                    </div>
                </div>
            </div>
            <button class="ui blue button">Добавить</button>
            <div class="ui positive message page__icon_info-positive">Сортировать МИА нужно непосредственно в дочерних услугах. Здесь осуществляется только привязка. Управляйте последовательностью вывода перетаскиванием строк.</div>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th> №</th>
                    <th> ID</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                <tbody>
                <tr>
                    <td data-label="Count">0</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Разрешение на работу сопровождающего члена семьи высококвалифицированного специалиста (ВКС)</td>
                    <td data-label="Date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_nolink"></button>
                    </td>
                </tr>
                <tr>
                    <td data-label="Count">1</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Многократная рабочая виза Высококвалифицированного специалиста</td>
                    <td data-label="Date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_nolink"></button>
                    </td>
                </tr>
                </tbody>
                </thead>
            </table>
        </div>
    </section>
</main>
@endsection
