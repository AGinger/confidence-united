@extends('admin.layouts.app')

@section('content')
@include('admin.partials.sidebar')
<article class="article">
    @include('admin.partials.header')
    <div class="header__sub">
        <div class="sub header">спецпредложение</div>
        <h1 class="ui header">Скидка для членов Американской Торговой Палаты в РФ</h1>
        <h2 class="sub header"></h2>
        <div class="ui top attached tabular menu">
            <div class="item" data-tab="0">1</div>
            <div class="item" data-tab="1">2</div>
        </div>
    </div>
</article>
<main class="page">
    <section class="main">
        <div class="ui bottom attached tab segment active" data-tab="0">
            <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в слайдах можно перетаскиванием строк в таблицах.</div>
            <button class="ui blue button">+ Добавить</button>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th> №</th>
                    <th> ID</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                <tbody>
                </thead>
                @foreach($sales as $sale)
                <tr>
                    <td data-label="Count">{{ $sale->number }}</td>
                    <td data-label="id">{{ $sale->id }}</td>
                    <td data-label="Name">{{ $sale->name_ru }}</td>
                    <td data-label="Date">{{ $sale->date }}</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_basket"></button>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="ui bottom attached tab segment" data-tab="1">
            <h3 class="ui header">Содержание</h3>
            <h4 class="ui header">Дата</h4>
            <div class="ui calendar" id="standard_calendar">
                <div class="ui input left icon"><i class="calendar icon"></i>
                    <input type="text" value="{{ $sale->date }}" placeholder="Date/Time"/>
                </div>
            </div>
            <h4 class="ui header">Оформление</h4>
            <div class="page-block-titleBtnText">
                <h5 class="ui header">Иллюстрация</h5>
                <div class="sub header header__subheader">Шрина 850 JPG качеством не ниже 65</div><img class="page__img" src="{{ asset($sale->background_image) }}" alt=""/>
                <div class="ui action input">
                    <input type="text" value="{{ $sale->background_image }}" readonly=""/>
                    <input type="file"/>
                    <button class="ui blue button fileUpload">Выбрать</button>
                </div>
                <button class="ui blue button">Сохранить</button>
            </div>
            <h3 class="ui header">Русскоязычная версия</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Название русское</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <div class="ui input">
                    <input type="text" value="{{ $sale->name_ru }}"/>
                </div>
            </div>
            <div class="page-block-titleAreaText">
                <h4 class="ui header header__subtitle">Текст русский</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <textarea class="ui segment textEditor" name="example">{{ $sale->description_ru }}</textarea>
                <button class="ui blue button">Сохранить</button>
            </div>
            <h3 class="ui header">Англоязычная версия</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Название английское</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <div class="ui input">
                    <input type="text" value="{{ $sale->name_eng }}"/>
                </div>
            </div>
            <div class="page-block-titleAreaText">
                <h4 class="ui header header__subtitle">Текст английский</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <textarea class="ui segment textEditor" name="example">{{ $sale->description_eng }}</textarea>
                <button class="ui blue button">Сохранить</button>
            </div>
        </div>
    </section>
</main>
@endsection
