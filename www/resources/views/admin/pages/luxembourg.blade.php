@extends('admin.layouts.app')

@section('content')
@include('admin.partials.sidebar')
<article class="article">
    @include('admin.partials.header')
    <div class="header__sub">
        <div class="sub header">услуга</div>
        <h1 class="ui header">Виза в Люксембург с туристической целью</h1>
        <h2 class="sub header">Визовая поддержка</h2>
        <div class="ui top attached tabular menu">
            <div class="item" data-tab="0">Русский</div>
            <div class="item" data-tab="1">Английский</div>
            <div class="item" data-tab="2">Оформление</div>
            <div class="item" data-tab="3">Варианты</div>
            <div class="item" data-tab="4">Привязка</div>
        </div>
    </div>
</article>
<main class="page">
    <section class="srvm">
        <div class="ui bottom attached tab segment active" data-tab="0">
            <h3 class="ui header">Содержание</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Название русское</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <div class="ui input">
                    <input type="text" placeholder="Визовая поддержка"/>
                </div>
            </div>
            <div class="page-block-titleAreaText">
                <h4 class="ui header header__subtitle">Описание русское</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <textarea class="ui segment textEditor" name="example">Комплексное сопровождение по всем вопросам, связанным с въездом, пребываением и осуществлением трудовой деятельности иностранных граждан на всей территории РФ </textarea>
                <button class="ui blue button">Сохранить</button>
            </div>
            <h3 class="ui header header__subheader">Метаданные русской версии</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Title</h4>
                <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                <div class="ui input">
                    <input type="text" placeholder="Миграционный консалтинг"/>
                </div>
            </div>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Keywords</h4>
                <div class="sub header header__subheader">70-80 символов</div>
                <div class="ui input">
                    <input type="text" placeholder="ключ 1, ключ 2, ключ 3…"/>
                </div>
            </div>
            <div class="page-block-titleAreaText">
                <h4 class="ui header header__subtitle">Description</h4>
                <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из ключевых.</div>
                <textarea class="ui segment textEditor" name="example">Комплексное сопровождение по всем вопросам, связанным с въездом, пребываением и осуществлением трудовой деятельности иностранных граждан на всей территории РФ </textarea>
                <button class="ui blue button">Сохранить</button>
            </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="1">
            <h3 class="ui header">Содержание</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Название английское</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <div class="ui input">
                    <input type="text" placeholder="Визовая поддержка"/>
                </div>
            </div>
            <div class="page-block-titleAreaText">
                <h4 class="ui header header__subtitle">Описание английское</h4>
                <div class="sub header header__subheader">Пояснение</div>
                <textarea class="ui segment textEditor" name="example">Comprehensive support on all issues related to the entry, stay and employment of foreign citizens throughout the Russian Federation</textarea>
                <button class="ui blue button">Сохранить</button>
            </div>
            <h3 class="ui header header__subheader">Метаданные русской версии</h3>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Title</h4>
                <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                <div class="ui input">
                    <input type="text" placeholder="Миграционный консалтинг"/>
                </div>
            </div>
            <div class="page-block-titleInputText">
                <h4 class="ui header header__subtitle">Keywords</h4>
                <div class="sub header header__subheader">70-80 символов</div>
                <div class="ui input">
                    <input type="text" placeholder="ключ 1, ключ 2, ключ 3…"/>
                </div>
            </div>
            <div class="page-block-titleAreaText">
                <h4 class="ui header header__subtitle">Description</h4>
                <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из ключевых.</div>
                <textarea class="ui segment textEditor" name="example">Comprehensive support on all issues related to the entry, stay and employment of foreign citizens throughout the Russian Federation</textarea>
                <button class="ui blue button">Сохранить</button>
            </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="2">
            <h3 class="ui header">Оформление</h3>
            <div class="page-block-titleBtnText">
                <h4 class="ui header">Фоновая иллюстрация</h4>
                <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div><img class="page__img" src="assets/img/img-srvm-2.png" alt=""/>
                <div class="ui action input">
                    <input type="text" placeholder="Загрузить файл" readonly=""/>
                    <input type="file"/>
                    <button class="ui blue button fileUpload">Выбрать</button>
                </div>
                <button class="ui blue button">Сохранить</button>
            </div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="3">
            <h3 class="ui header">Варианты оказания услуги</h3>
            <button class="ui blue button">+ Добавить</button>
            <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в слайдах можно перетаскиванием строк в таблицах.</div>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th> №</th>
                    <th> ID</th>
                    <th>Срок оформления дн.</th>
                    <th>Госпошлина ₽</th>
                    <th>Стоимость ₽</th>
                    <th>Действия</th>
                </tr>
                <tbody>
                <tr>
                    <td data-label="Count">0</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">5</td>
                    <td data-label="cost1">500</td>
                    <td data-label="cost2">500</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_basket"></button>
                    </td>
                </tr>
                </tbody>
                </thead>
            </table>
            <button class="ui blue button">Сохранить</button>
        </div>
        <div class="ui bottom attached tab segment" data-tab="4">
            <h4 class="ui header">Привязка к стране и цели поездки</h4>
            <div class="ui positive message page__icon_info-positive">Привязка к стране и цели обязательна.</div>
            <div class="page-block-select">
                <div class="ui basic floating dropdown button">
                    <div class="text">Слайд 1</div><i class="dropdown icon"></i>
                    <div class="menu">
                        <div class="item">Слайд 2</div>
                        <div class="item">Слайд 3</div>
                    </div>
                </div>
                <div class="ui basic floating dropdown button">
                    <div class="text">Слайд 1</div><i class="dropdown icon"></i>
                    <div class="menu">
                        <div class="item">Слайд 2</div>
                        <div class="item">Слайд 3</div>
                    </div>
                </div>
            </div>
            <button class="ui blue button">Добавить</button>
            <div class="ui positive message page__icon_info-positive">Сортировать МИА нужно непосредственно в дочерних услугах. Здесь осуществляется только привязка. Управляйте последовательностью вывода перетаскиванием строк.</div>
            <table class="ui celled table">
                <thead>
                <tr>
                    <th> №</th>
                    <th> ID</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Действия</th>
                </tr>
                <tbody>
                <tr>
                    <td data-label="Count">0</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Разрешение на работу сопровождающего члена семьи высококвалифицированного специалиста (ВКС)</td>
                    <td data-label="Date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_nolink"></button>
                    </td>
                </tr>
                <tr>
                    <td data-label="Count">1</td>
                    <td data-label="id">5</td>
                    <td data-label="Name">Многократная рабочая виза Высококвалифицированного специалиста</td>
                    <td data-label="Date">12.07.20</td>
                    <td data-label="Action">
                        <button class="page__icon page__icon_pen"></button>
                        <button class="page__icon page__icon_eye"></button>
                        <button class="page__icon page__icon_nolink"></button>
                    </td>
                </tr>
                </tbody>
                </thead>
            </table>
        </div>
        <div class="ui bottom attached tab segment" data-tab="5"></div>
    </section>
</main>
@endsection
