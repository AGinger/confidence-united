@extends('admin.layouts.app')

@section('content')
@include('admin.partials.sidebar')
<article class="article">
    @include('admin.partials.header')
    <div class="header__sub">
        <div class="sub header">спецпредложение</div>
        <h1 class="ui header">Информационная поддержка 1</h1>
        <h2 class="sub header"></h2>
        <div class="ui top attached tabular menu">
            <div class="item" data-tab="0">Русский</div>
            <div class="item" data-tab="1">Английский</div>
            <div class="item" data-tab="2">Текст</div>
            <div class="item" data-tab="3">Клиенты</div>
        </div>
    </div>
</article>
<main class="page">
    <section class="main">
        <div class="ui bottom attached tab segment active" data-tab="one">
            <h3 class="ui header">Содержание</h3>
            <h4 class="ui header" id="h4">Дата</h4>
            <div class="sub header">Пояснение</div>
            <div class="ui input focus">
                <input type="text" placeholder="Дата"/>
            </div>
            <h4 class="ui header" id="h4">Оформление</h4>
            <h3 class="ui header">Иллюстрация</h3>
            <div class="sub header">Шрина 850 JPG качеством не ниже 65</div>
            <button class="ui blue button">Сохранить</button>
            <h3 class="ui header">Русскоязычная версия</h3>
            <h4 class="ui header" id="h4">Название русское</h4>
            <div class="sub header">Пояснение</div>
            <div class="ui input focus">
                <input type="text" placeholder="Информационная поддержка 1"/>
            </div>
            <h3 class="ui header">Текст русский</h3>
            <div class="sub header">Пояснение</div>
            <textarea class="ui segment">Для членов Американской Торговой Палаты в РФ, Финско-Российской Торговой Палаты в РФ, Торгово-Промышленной Палаты РФ и других бизнес-сообществ РФ скидка на миграционные и визовые услуги составляет 30% от стандартной стоимости услуг Конфиденс Групп. Условия акции: не распространяются на действующие договоры с Конфиденс Групп; распространяются на услуги Конфиденс Групп в рамках договоров, заключенных в период с 01.06.2017 по 31.08.2017; распространяются только на безналичные расчеты между организациями; не распространяются на государственные пошлины, Консульские и другие сборы государственных и коммерческих организаций; скидка действует до 31.12.2017 года. За дополнительной информацией обращайтесь к консультантам Конфиденс Групп.</textarea>
            <button class="ui blue button">Сохранить</button>
            <h3 class="ui header">Англоязычная версия</h3>
            <h4 class="ui header" id="h4">Название английское</h4>
            <div class="sub header">Пояснение</div>
            <div class="ui input focus">
                <input type="text"
                       placeholder="Discount for members of the American Chamber of Commerce in the Russian Federation"/>
            </div>
            <h4 class="ui header" id="h4">Текст английский</h4>
            <div class="sub header">Пояснение</div>
            <textarea class="ui segment">For members of the American Chamber of Commerce in the Russian Federation, the Finnish-Russian Chamber of Commerce in the Russian Federation, the Chamber of Commerce and Industry of the Russian Federation and other business communities of the Russian Federation, a discount on migration and visa services is 30% of the standard cost of Confidence Group services. Promotion terms: do not apply to existing agreements with Confidence Group; apply to the services of the Confidence Group under the contracts concluded during the period from 06/01/2017 to 08/31/2017; apply only to non-cash payments between organizations; do not apply to state duties, Consular and other fees of state and commercial organizations; The discount is valid until 12/31/2017. For more information, contact the consultants of Confidence Group.</textarea>
            <button class="ui blue button">Сохранить</button>
        </div>
    </section>
</main>
@endsection
