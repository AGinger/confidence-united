@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">тип услуг</div>
            <h1 class="ui header">{{ $service_type->name_ru ?? 1 }}</h1>
            <h2 class="sub header"><span><a href="{{ route($parentRoute) }}">{{ mb_strtoupper($parentName)}}</a> / {{ $service_type->name_ru ?? 1 }}</span></h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Содержание</div>
                <div class="item" data-tab="1">МИА</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <form
                    action="{{ route('admin.service-type.update', ['serviceType' => $service_type->id ?? 1]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название русское</h4>
                        <div class="sub header header__subheader">48 знаков максимум</div>
                        <div class="ui input">
                            <input type="text" name="name_ru" value="{{ $service_type->name_ru ?? 1 }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">48 знаков максимум</div>
                        <div class="ui input">
                            <input type="text" name="name_eng" value="{{ $service_type->name_eng ?? 1 }}"/>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>

                <h3 class="ui header">Услуги</h3>
                <a href="{{ route('admin.service-type.add', ['page' => $page, 'category' => $category]) }}"
                   class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td data-label="Count">{{ $service->number }}</td>
                            <td data-label="id">{{ $service->id }}</td>
                            <td data-label="Name">{!! $service->name !!}</td>
                            <td data-label="Date">{{ $service->created_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.service.edit', ['serviceType' => $service_type->id ?? 1, 'number' => $service->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <form action="{{ route('admin.service-type.delete', ['service' => $service->id]) }}"
                                      method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="page__icon page__icon_basket"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="1">
                <h3 class="ui header">Привязка материалов информационного агентства (МИА)</h3>
                <h4 class="ui header header__subtitle page__icon_mark-positive">Добавить МИА</h4>
                <div class="page-block-select page-block-select_2">
                    <div class="ui basic floating dropdown button">
                        <div class="text">Слайд 1</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item">Слайд 2</div>
                            <div class="item">Слайд 3</div>
                        </div>
                    </div>
                    <div class="ui basic floating dropdown button">
                        <div class="text">Слайд 1</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item">Слайд 2</div>
                            <div class="item">Слайд 3</div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('admin.service-type.add.mia', ['page' => $page, 'category' => $category]) }}"
                   class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Сортировать МИА нужно непосредственно в
                    дочерних услугах. Здесь осуществляется только привязка. Управляйте последовательностью вывода
                    перетаскиванием строк.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mias as $mia)
                        <tr>
                            <td data-label="Count">{{ $mia->number }}</td>
                            <td data-label="id">{{ $mia->id }}</td>
                            <td data-label="Name">{!! $mia->name !!}</td>
                            <td data-label="Date">{{ $mia->created_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.mia.edit', ['serviceType' => $service_type->id ?? 1,'number' => $mia->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_nolink"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
