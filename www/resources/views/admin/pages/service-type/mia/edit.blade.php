@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">услуга</div>
            <h1 class="ui header">{{ $mia_ru->name }}</h1>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Русский</div>
                <div class="item" data-tab="1">Английский</div>
                <div class="item" data-tab="2">Оформление</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <h3 class="ui header">Содержание русскоязычной версии</h3>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_ru->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor" name="name">{{ $mia_ru->name }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_ru->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Анонс русский</h4>
                        <div class="sub header header__subheader">Максимум 187 символов</div>
                        <textarea class="ui segment textEditor" name="announcement">{{ $mia_ru->announcement }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_ru->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Максимум 375 символов</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $mia_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_ru->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header">Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $mia_ru->title_meta }}"/>
                        </div>
                    </div>

                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $mia_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $mia_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="1">
                <h3 class="ui header">Содержание англоязычной версии</h3>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_eng->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor" name="name">{{ $mia_eng->name }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_eng->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Анонс английский</h4>
                        <div class="sub header header__subheader">Максимум 187 символов</div>
                        <textarea class="ui segment textEditor" name="announcement">{{ $mia_eng->announcement }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_eng->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Максимум 375 символов</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $mia_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.mia.update', ['mia' => $mia_eng->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header">Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $mia_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $mia_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $mia_eng->title_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <h3 class="ui header">Изображение</h3>
                <form action="{{ route('admin.mia.update.image', ['number' => $mia_ru->number ]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="page-block-titleBtnText">
                        <h4 class="ui header">Фоновая иллюстрация</h4>
                        <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div>
                        <img class="page__img" src="{{ asset($mia_ru->background_image) }}" alt=""/>
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл" readonly=""/>
                            <input type="file" name="background_image"/>
                            <a class="ui blue button fileUpload">Выбрать</a>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
