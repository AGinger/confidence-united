@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">спецпредложение</div>
            <h1 class="ui header">Скидка {{ $sale_ru->name }}</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <h3 class="ui header">Содержание</h3>
            <h4 class="ui header">Дата</h4>
            <div class="ui calendar" id="standard_calendar">
                <div class="ui input left icon"><i class="calendar icon"></i>
                    <input type="text" name="date" value="{{ $sale_ru->date }}" placeholder="Date/Time"/>
                </div>
            </div>
            <h4 class="ui header">Оформление</h4>
            <div class="page-block-titleBtnText">
                <h5 class="ui header">Иллюстрация</h5>
                <div class="sub header header__subheader">Шрина 850 JPG качеством не ниже 65</div>
                <img class="page__img" src="{{ asset($sale_ru->background_image) }}" alt=""/>
                <form action="{{ route('admin.service-type.sale.update.image', ['number' => $sale_ru->number]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="ui action input">
                        <input type="text" value="{{ $sale_ru->background_image }}" readonly=""/>
                        <input type="file" name="background_image"/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                    <button type="submit" class="ui blue button">Сохранить</button>
                </form>
            </div>
            <form action="{{ route('admin.service-type.sale.update', ['sale' => $sale_ru->id]) }}" method="POST">
                @csrf
                <h3 class="ui header">Русскоязычная версия</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle">Название русское</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ $sale_ru->name }}"/>
                    </div>
                </div>
                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Текст русский</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <textarea class="ui segment textEditor" name="description">{{ $sale_ru->description }}</textarea>
                    <button class="ui blue button">Сохранить</button>
                </div>
            </form>
            <form action="{{ route('admin.service-type.sale.update', ['sale' => $sale_eng->id]) }}" method="POST">
                @csrf
                <h3 class="ui header">Англоязычная версия</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle">Название английское</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ $sale_eng->name }}"/>
                    </div>
                </div>
                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Текст английский</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <textarea class="ui segment textEditor" name="description">{{ $sale_eng->description }}</textarea>
                    <button type="submit" class="ui blue button">Сохранить</button>
                </div>
            </form>
        </section>
    </main>
@endsection
