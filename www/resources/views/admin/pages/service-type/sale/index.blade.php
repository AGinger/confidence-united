@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">тип услуг</div>
            <h1 class="ui header">СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ И АКЦИИ</h1>
            <h2 class="sub header"><span><a href="{{ route($parentRoute) }}">{{ mb_strtoupper($parentName)}}</a> / СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ И АКЦИИ</span></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <a href="{{ route('admin.service-type.sale.add', ['page' => $page]) }}" class="ui blue button">+
                    Добавить</a>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    <tbody>
                    </thead>
                    @foreach($sales as $sale)
                        <tr>
                            <td data-label="Count">{{ $sale->number }}</td>
                            <td data-label="id">{{ $sale->id }}</td>
                            <td data-label="Name">{{ $sale->name }}</td>
                            <td data-label="Date">{{ $sale->date }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.service-type.sale.edit', ['sale' => $sale->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_basket"></button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
