@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">спецпредложение</div>
            <h1 class="ui header">Скидка</h1>
            <h2 class="sub header"></h2>

        </div>
    </article>
    <main class="page">
        <section class="main">
                <h3 class="ui header">Содержание</h3>
                <form action="{{ route('admin.service-type.sale.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <h4 class="ui header">Дата</h4>
                    <div class="ui calendar" id="standard_calendar">
                        <div class="ui input left icon"><i class="calendar icon"></i>
                            <input type="text" name="date" placeholder="Date/Time"/>
                        </div>
                    </div>
                    <h4 class="ui header">Оформление</h4>
                    <div class="page-block-titleBtnText">
                        <h5 class="ui header">Иллюстрация</h5>
                        <div class="sub header header__subheader">Шрина 850 JPG качеством не ниже 65</div>
                        <div class="ui action input">
                            <input type="text" readonly=""/>
                            <input type="file" name="background_image"/>
                            <button class="ui blue button fileUpload">Выбрать</button>
                        </div>
                    </div>
                    <h3 class="ui header">Русскоязычная версия</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name_ru"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Текст русский</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor" name="description_ru"></textarea>
                    </div>
                    <h3 class="ui header">Англоязычная версия</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name_eng"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Текст английский</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor" name="description_eng"></textarea>
                    </div>

                    <div class="ui input">
                        <input type="text" name="type" value="{{ $type }}" style="display: none"/>
                    </div>

                    <button type="submit" class="ui blue button">Сохранить</button>
                </form>
        </section>
    </main>
@endsection
