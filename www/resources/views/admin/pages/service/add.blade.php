@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">услуга</div>
            <h1 class="ui header">Добавление услуги</h1>
            <h2 class="sub header">Добавление услуги</h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <form action="{{ route('admin.service-type.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <h3 class="ui header">Содержание русскоязычной версии</h3>
                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Название русское</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <textarea class="ui segment textEditor" name="name_ru"></textarea>
                </div>


                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Анонс русский</h4>
                    <div class="sub header header__subheader">Максимум 187 символов</div>
                    <textarea class="ui segment textEditor" name="announcement_ru"></textarea>
                </div>


                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Описание русское</h4>
                    <div class="sub header header__subheader">Максимум 375 символов</div>
                    <textarea class="ui segment textEditor"
                              name="description_ru"></textarea>
                </div>

                <h3 class="ui header">Метаданные русской версии</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle">Title</h4>
                    <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                    <div class="ui input">
                        <input type="text" name="title_meta_ru"/>
                    </div>
                </div>

                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle">Keywords</h4>
                    <div class="sub header header__subheader">70-80 символов</div>
                    <div class="ui input">
                        <input type="text" name="keywords_meta_ru"/>
                    </div>
                </div>
                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Description</h4>
                    <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                        ключевых.
                    </div>
                    <textarea class="ui segment textEditor"
                              name="description_meta_ru"></textarea>
                </div>


                <h3 class="ui header">Содержание англоязычной версии</h3>

                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Название английское</h4>
                    <div class="sub header header__subheader">Пояснение</div>
                    <textarea class="ui segment textEditor" name="name_eng"></textarea>
                </div>

                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Анонс английский</h4>
                    <div class="sub header header__subheader">Максимум 187 символов</div>
                    <textarea class="ui segment textEditor" name="announcement_eng"></textarea>
                </div>

                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Описание английское</h4>
                    <div class="sub header header__subheader">Максимум 375 символов</div>
                    <textarea class="ui segment textEditor"
                              name="description_eng"></textarea>
                </div>

                <h3 class="ui header">Метаданные английской версии</h3>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle">Title</h4>
                    <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                    <div class="ui input">
                        <input type="text" name="title_meta_eng"/>
                    </div>
                </div>
                <div class="page-block-titleInputText">
                    <h4 class="ui header header__subtitle">Keywords</h4>
                    <div class="sub header header__subheader">70-80 символов</div>
                    <div class="ui input">
                        <input type="text" name="keywords_meta_eng"/>
                    </div>
                </div>
                <div class="page-block-titleAreaText">
                    <h4 class="ui header header__subtitle">Description</h4>
                    <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                        ключевых.
                    </div>
                    <textarea class="ui segment textEditor"
                              name="description_meta_eng"></textarea>
                </div>

                <h3 class="ui header">Изображение</h3>

                <div class="page-block-titleBtnText">
                    <h4 class="ui header">Фоновая иллюстрация</h4>
                    <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div>
                    <div class="ui action input">
                        <input type="text" placeholder="Загрузить файл" readonly=""/>
                        <input type="file" name="background_image"/>
                        <a class="ui blue button fileUpload">Выбрать</a>
                    </div>
                </div>
                <div class="ui input">
                    <input type="text" name="type" value="{{ $type }}" style="display: none"/>
                </div>
                <div class="ui input">
                    <input type="text" name="category" value="{{ $category }}" style="display: none"/>
                </div>
                <button type="submit" class="ui blue button">Сохранить</button>

            </form>
        </section>
    </main>
@endsection
