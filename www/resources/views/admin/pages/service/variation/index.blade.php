@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">услуга</div>
            <h1 class="ui header">ДОБАВЛЕНИЕ/ИЗМЕНЕНИЕ УСЛУГИ</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active">
                <div class="sub header">Изменение данных.</div>
                <form action="{{ route('admin.variation.update', ['variation' => $variation->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleBtnText">
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Срок оформления дн.</h4>
                            <div class="ui input">
                                <input type="number" step="1" name="process_time"
                                       value="{{ $variation->process_time }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Название</h4>
                            <div class="ui input">
                                <input type="number" name="government_duty"
                                       value="{{ $variation->government_duty }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Название</h4>
                            <div class="ui input">
                                <input type="number" name="price" value="{{ $variation->price }}"/>
                            </div>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
