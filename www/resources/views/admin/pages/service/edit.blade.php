@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">услуга</div>
            <h1 class="ui header">{{ $service_ru->name }}</h1>
            <h2 class="sub header">
                <span>
                    <a href="{{ route($mainParentRoute) }}">{{ mb_strtoupper($mainParentName)}}</a>
                    /
                    <a href="{{ route('admin.service-type.index', [
                        'page' => $serviceType->page,
                        'category' => $serviceType->category
                    ]) }}">{{ mb_strtoupper($serviceType->name_ru)}}</a>
                    /
                    {{ $service_ru->name }}
                </span>
            </h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Русский</div>
                <div class="item" data-tab="1">Английский</div>
                <div class="item" data-tab="2">Оформление</div>
                <div class="item" data-tab="3">Варианты</div>
                <div class="item" data-tab="4">Привязка</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <h3 class="ui header">Содержание русскоязычной версии</h3>
                <form action="{{ route('admin.service.update', ['service' => $service_ru->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor" name="name">{{ $service_ru->name }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.service.update', ['service' => $service_ru->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Анонс русский</h4>
                        <div class="sub header header__subheader">Максимум 187 символов</div>
                        <textarea class="ui segment textEditor" name="announcement">{{ $service_ru->announcement }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.service.update', ['service' => $service_ru->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Максимум 375 символов</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $service_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.service.update', ['service' => $service_ru->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header">Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $service_ru->title_meta }}"/>
                        </div>
                    </div>

                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $service_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $service_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="1">
                <h3 class="ui header">Содержание англоязычной версии</h3>
                <form action="{{ route('admin.service.update', ['service' => $service_eng->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor" name="name">{{ $service_eng->name }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.service.update', ['service' => $service_eng->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Анонс английский</h4>
                        <div class="sub header header__subheader">Максимум 187 символов</div>
                        <textarea class="ui segment textEditor" name="announcement">{{ $service_eng->announcement_ru }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.service.update', ['service' => $service_eng->id]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Максимум 375 символов</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $service_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.service.update', ['service' => $service_eng->id]) }}" method="POST">
                    @csrf
                    <h3 class="ui header">Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $service_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $service_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $service_eng->title_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <h3 class="ui header">Изображение</h3>
                <form action="{{ route('admin.service.update.image', ['number' => $service_eng->number ]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="page-block-titleBtnText">
                        <h4 class="ui header">Фоновая иллюстрация</h4>
                        <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div>
                        <img class="page__img" src="{{ asset($service_ru->background_image) }}" alt=""/>
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл" readonly=""/>
                            <input type="file" name="background_image"/>
                            <a class="ui blue button fileUpload">Выбрать</a>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="3">
                <h3 class="ui header">Варианты оказания услуги</h3>
                <button class="ui blue button">+ Добавить</button>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Срок оформления дн.</th>
                        <th>Госпошлина ₽</th>
                        <th>Стоимость ₽</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($variations as $variation)
                        <tr>
                            <td data-label="Count">0</td>
                            <td data-label="id">{{ $variation->id }}</td>
                            <td data-label="Name">{{ $variation->process_time }}</td>
                            <td data-label="cost1">{{ $variation->government_duty }}</td>
                            <td data-label="cost2">{{ $variation->price }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.variation.index', ['serviceType' => $serviceType->id, 'variation' => $variation->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_basket"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <button class="ui blue button">Сохранить</button>
            </div>
            <div class="ui bottom attached tab segment" data-tab="4">
                <h3 class="ui header">Привязка МИА</h3>
                <div class="ui positive message page__icon_info-positive">Добавлять МИА можно только в родительском типе
                    услуг, тут можно только сортировать. Управляйте последовательностью вывода перетаскиванием строк. По
                    умолчанию сортировка по дате.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Тип</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td data-label="Count">0</td>
                        <td data-label="id">5</td>
                        <td data-label="Name">Правительство предусмотрит меры приглашающей стороны для соблюдения
                            иностранцем режима пребывания (проживания) в РФ
                        </td>
                        <td data-label="type">новость</td>
                        <td data-label="Date">5.09.20</td>
                        <td data-label="Action">
                            <button class="page__icon page__icon_pen"></button>
                            <button class="page__icon page__icon_eye"></button>
                            <button class="page__icon page__icon_nolink"></button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
