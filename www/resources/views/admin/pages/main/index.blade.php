@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">раздел</div>
            <h1 class="ui header">Главная страница</h1>
            <h2 class="sub header"></h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">{{ __('Русский') }}</div>
                <div class="item" data-tab="1">{{ __('Английский') }}</div>
                <div class="item" data-tab="2">{{ __('Текст') }}</div>
                <div class="item" data-tab="3">{{ __('Клиенты') }}</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle page__icon_question">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_ru->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title"> Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_ru->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>

            <div class="ui bottom attached tab segment" data-tab="1">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_eng->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_eng->description }}</textarea>
                        <button class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__title"> Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_eng->description_meta }}</textarea>
                        <button class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <h3 class="ui header header__title">Текстовые блоки главной страницы</h3>
                <h4 class="ui header header__title">Услуги</h4>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Услуги, анонс. (рус.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor" name="services">{{ $text_ru->services }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Услуги, анонс. (анг.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor" name="services">{{ $text_eng->services }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <h4 class="ui header header__title">Клиенты</h4>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Клиенты, анонс. (рус.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor" name="clients">{{ $text_ru->clients }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Клиенты, анонс. (анг.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor" name="clients">{{ $text_eng->clients }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <h4 class="ui header header__title">Информагентство</h4>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Информагентство, анонс. (рус.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="information_agency">{{ $text_ru->information_agency }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Информагентство, анонс. (анг.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="information_agency">{{ $text_eng->information_agency }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <h4 class="ui header header__title">Футер</h4>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Контакты, (рус.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="footer_contacts">{{ $text_ru->footer_contacts }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Социальные сети(рус.)</h4>

                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Twitter</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks" value="{{ $text_ru->social_networks }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Facebook</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks_second"
                                       value="{{ $text_ru->social_networks_second }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Linkedin</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks_third"
                                       value="{{ $text_ru->social_networks_third }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">VK</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks_fourth"
                                       value="{{ $text_ru->social_networks_fourth }}"/>
                            </div>
                            <button type="submit" class="ui blue button">Сохранить</button>
                        </div>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Социальные сети(англ.)</h4>

                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Twitter</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks" value="{{ $text_eng->social_networks }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Facebook</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks_second"
                                       value="{{ $text_eng->social_networks_second }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Linkedin</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks_third"
                                       value="{{ $text_eng->social_networks_third }}"/>
                            </div>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">VK</h4>
                            <div class="sub header header__subheader">Пояснение</div>
                            <div class="ui input">
                                <input type="text" name="social_networks_fourth"
                                       value="{{ $text_eng->social_networks_fourth }}"/>
                            </div>
                            <button type="submit" class="ui blue button">Сохранить</button>
                        </div>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Контакты, (анг.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="footer_contacts">{{ $text_eng->footer_contacts }}</textarea>
                        <button class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Офис в Москве. (рус.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="footer_office">{{ $text_ru->footer_office }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Офис в Москве. (анг.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="footer_office">{{ $text_eng->footer_office }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_ru->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Часы работы. (рус.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="footer_business_hours">{{ $text_ru->footer_business_hours }}</textarea>
                        <button class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form action="{{ route('admin.main.store', ['lang' => $text_eng->language]) }}" method="POST">
                    @csrf
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Часы работы. (анг.)</h4>
                        <div class="sub header header__subheader">140 символов с пробелами максимум.</div>
                        <textarea class="ui segment textEditor"
                                  name="footer_business_hours">{{ $text_eng->footer_business_hours }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="3">
                <h3 class="ui header header__title">Слайдер с логотипами клиентов</h3>
                <form action="{{ route('admin.main.clients.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="page-block-titleBtnText">
                        <h4 class="ui header">Добавить слайд</h4>
                        <div class="sub header header__subheader">Иллюстрацию вписать в габариты 230x153</div>
                        <div class="page-block-titleBtnText__btns">
                            <div class="ui action input">
                                <input type="text" placeholder="Загрузить файл"/>
                                <input type="file" name="image" required/>
                                <button class="ui blue button fileUpload">Выбрать</button>
                            </div>
                            <select name="slide" class="ui basic floating dropdown button">
                                <div class="text">Слайд 1</div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    <option value="1" class="item">Слайд 1</option>
                                    <option value="2" class="item">Слайд 2</option>
                                    <option value="3" class="item">Слайд 3</option>
                                </div>
                            </select>

                            <select name="type" class="ui basic floating dropdown button">
                                <div class="text">Тип</div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    <option value="1" class="item">Международный</option>
                                    <option value="0" class="item">Российский</option>
                                </div>
                            </select>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации
                    в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <h3 class="ui header header__title">Российские бренды</h3>
                <h4 class="ui header header__title">Слайд 1</h4>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th></th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients_ru as $client)
                        @if($client->slide == 1)
                            <tr>
                                <td data-label="Count">0</td>
                                <td data-label="id">{{$client->id}}</td>
                                <td data-label="img"><img src="{{ $client->image }}"/></td>
                                <td data-label="Name">{{ $client->name }}</td>
                                <td data-label="Action" style="display: flex; justify-content: center">
                                    <a href="{{ route('admin.main.clients.edit', ['client' => $client->id]) }}"
                                       class="page__icon page__icon_pen"></a>
                                    <form action="{{ route('admin.main.clients.delete', ['client' => $client->id]) }}"
                                          method="POST">
                                        @csrf
                                        <button type="submit" class="page__icon page__icon_basket"></button>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <h4 class="ui header header__title">Слайд 2</h4>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th></th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients_ru as $client)
                        @if($client->slide == 2)
                            <tr>
                                <td data-label="Count">0</td>
                                <td data-label="id">{{$client->id}}</td>
                                <td data-label="img"><img src="{{ asset($client->image) }}"/></td>
                                <td data-label="Name">{{ $client->name }}</td>
                                <td data-label="Action" style="display: flex; justify-content: center">
                                    <a href="{{ route('admin.main.clients.edit', ['client' => $client->id]) }}"
                                       class="page__icon page__icon_pen"></a>
                                    <form action="{{ route('admin.main.clients.delete', ['client' => $client->id]) }}"
                                          method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="page__icon page__icon_basket"></button>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>

                <h3 class="ui header header__title">Международные бренды</h3>
                <h4 class="ui header header__title">Слайд 1</h4>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th></th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients_int as $client)
                        @if($client->slide == 1)
                            <tr>
                                <td data-label="Count">0</td>
                                <td data-label="id">{{$client->id}}</td>
                                <td data-label="img"><img src="{{ $client->image }}"/></td>
                                <td data-label="Name">{{ $client->name }}</td>
                                <td data-label="Action" style="display: flex; justify-content: center">
                                    <a href="{{ route('admin.main.clients.edit', ['client' => $client->id]) }}"
                                       class="page__icon page__icon_pen"></a>
                                    <form action="{{ route('admin.main.clients.delete', ['client' => $client->id]) }}"
                                          method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="page__icon page__icon_basket"></button>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <h4 class="ui header header__title">Слайд 2</h4>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th></th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients_int as $client)
                        @if($client->slide == 2)
                            <tr>
                                <td data-label="Count">0</td>
                                <td data-label="id">{{$client->id}}</td>
                                <td data-label="img"><img src="{{ asset($client->image) }}"/></td>
                                <td data-label="Name">{{ $client->name }}</td>
                                <td data-label="Action" style="display: flex; justify-content: center">
                                    <a href="{{ route('admin.main.clients.edit', ['client' => $client->id]) }}"
                                       class="page__icon page__icon_pen"></a>
                                    <form action="{{ route('admin.main.clients.delete', ['client' => $client->id]) }}"
                                          method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="page__icon page__icon_basket"></button>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
