@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">изменение клиента</div>
            <h1 class="ui header">ИЗМЕНЕНИЕ ДАННЫХ КЛИЕНТА {{ $client->id }}</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="main">
            <div class="ui bottom attached tab segment active">
                <div class="sub header">Изменение данных.</div>
                <form action="{{ route('admin.main.clients.update', ['client' => $client->id]) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="page-block-titleBtnText">
                        <img class="page__img" src="{{ asset($client->image) }}" alt=""/>
                        <div class="sub header header__subheader">Иллюстрацию вписать в габариты 230x153</div>
                        <div class="page-block-titleBtnText__btns">
                            <div class="ui action input">
                                <input type="text" placeholder="Загрузить файл"/>
                                <input type="file" name="image"/>
                                <button class="ui blue button fileUpload">Выбрать</button>
                            </div>
                            <select name="slide" class="ui basic floating dropdown button">
                                <div class="text">Слайд 1</div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    <option value="1" class="item">Слайд 1</option>
                                    <option value="2" class="item">Слайд 2</option>
                                    <option value="3" class="item">Слайд 3</option>
                                </div>
                            </select>

                            <select name="type" class="ui basic floating dropdown button">
                                <div class="text">Тип</div>
                                <i class="dropdown icon"></i>
                                <div class="menu">
                                    <option value="1" class="item">Международный</option>
                                    <option value="0" class="item">Российский</option>
                                </div>
                            </select>
                        </div>
                        <div class="page-block-titleInputText">
                            <h4 class="ui header header__subtitle page__icon_question">Название</h4>
                            <div class="ui input">
                                <input type="text" name="name" value="{{ $client->name }}"/>
                            </div>
                        </div>
                        <button type="submit" class="ui blue button">Обновить</button>
                    </div>
                </form>
            </div>
        </section>
    </main>
@endsection
