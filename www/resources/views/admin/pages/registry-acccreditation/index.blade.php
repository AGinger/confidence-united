@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">направление деятельности</div>
            <h1 class="ui header">РЕГИСТРАЦИЯ И АККРЕДИТАЦИЯ</h1>
            <h2 class="sub header"></h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Русский</div>
                <div class="item" data-tab="1">Английский</div>
                <div class="item" data-tab="2">Оформл-е</div>
                <div class="item" data-tab="3">П-взаимод.</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="srvm">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle page__icon_question">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_ru->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_ru->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>

            <div class="ui bottom attached tab segment" data-tab="1">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle page__icon_question">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_eng->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_eng->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <form
                    action="{{ route('admin.page.store.image', ['page' => $page_eng->category]) }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <h3 class="ui header">Оформление</h3>
                    <div class="page-block-titleBtnText">
                        <h4 class="ui header">Фоновая иллюстрация</h4>
                        <div class="sub header header__subheader">1400x600 JPG качеством не выше 45</div>
                        <img class="page__img" src="{{ asset($page_ru->background_image) }}" alt=""/>
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл" readonly=""/>
                            <input type="file" name="background_image"/>
                            <a class="ui blue button fileUpload">Выбрать</a>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="3">
                <h3 class="ui header">Порядок взаимодействия</h3>
                <div class="page-block-select">
                    <select name="slide" class="ui basic floating dropdown button">
                        <i class="dropdown icon"></i>
                        <div class="menu tab">
                            @foreach($interaction_ru as $int)
                                <option value="{{ $int->slide }}" name="slide" class="item tablinks">
                                    Слайд {{ $int->slide }}</option>
                            @endforeach
                        </div>
                    </select>
                </div>

                <div class="tab">
                    @foreach($interaction_ru as $int)
                        <button class="tablinks ui blue button"
                                onclick="chooseSlide(event, 'ru({{ $int->slide }})', 'eng({{ $int->slide }})')">
                            Слайд {{ $int->slide }}</button>
                    @endforeach
                </div>

                @foreach($interaction_ru as $int)
                    <form
                        action="{{ route('admin.page.store.interaction', ['interaction' => $int->id]) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        <div id="ru({{ $int->slide }})" class="tabcontent" style="display: none">
                            <h3 class="ui header">Русскоязычная версия</h3>
                            <div class="page-block-titleInputText">
                                <h4 class="ui header header__subtitle">Заголовок</h4>
                                <div class="sub header header__subheader">максимум 21 символ, максимальная длина слова
                                    10
                                </div>
                                <div class="ui input">
                                    <input type="text" name="title" value="{{ $int->title }}" required/>
                                </div>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Клиент»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>

                                @foreach(\GuzzleHttp\json_decode($int->unit_client, true) as $client)
                                    <div class="ui input">
                                        <input type="text" name="client[]" value="{{ $client }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach

                                <button class="ui blue button">+</button>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Консультант»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>

                                @foreach(\GuzzleHttp\json_decode($int->unit_consultant, true) as $consultant)
                                    <div class="ui input">
                                        <input type="text" name="consultant[]" value="{{ $consultant }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach
                                <button class="ui blue button">+</button>
                                <h4 class="ui header">Иллюстрация русскоязычная</h4>
                                <div class="page-block-titleBtnText">
                                    <div class="sub header header__subheader">Готовый слайд точного размера 585х300
                                    </div>
                                    <img class="page__img" src="{{ asset($int->background_image) }}" alt=""/>
                                    <div class="ui action input">
                                        <input type="text" placeholder="Загрузить файл" readonly=""/>
                                        <input type="file" name="background_image"/>
                                        <a class="ui blue button fileUpload">Выбрать</a>
                                    </div>
                                    <button type="submit" class="ui blue button">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
                @foreach($interaction_eng as $int)
                    <form
                        action="{{ route('admin.page.store.interaction', ['interaction' => $int->id]) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        <div id="eng({{ $int->slide }})" class="tabcontent" style="display: none">
                            <h3 class="ui header">Англоязычная версия</h3>
                            <div class="page-block-titleInputText">
                                <h4 class="ui header header__subtitle">Заголовок</h4>
                                <div class="sub header header__subheader">максимум 21 символ, максимальная длина слова
                                    10
                                </div>
                                <div class="ui input">
                                    <input type="text" name="title" value="{{ $int->title }}" required/>
                                </div>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Client»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>
                                @foreach(\GuzzleHttp\json_decode($int->unit_client, true) as $client)
                                    <div class="ui input">
                                        <input type="text" name="client[]" value="{{ $client }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach
                                <button class="ui blue button">+</button>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Consultant»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>
                                @foreach(\GuzzleHttp\json_decode($int->unit_consultant, true) as $consultant)
                                    <div class="ui input">
                                        <input type="text" name="consultant[]" value="{{ $consultant }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach
                                <button class="ui blue button">+</button>
                            </div>
                            <h4 class="ui header">Иллюстрация англоязычная</h4>
                            <div class="page-block-titleBtnText">
                                <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div>
                                <img class="page__img" src="{{ asset($int->background_image) }}" alt=""/>
                                <div class="ui action input">
                                    <input type="text" placeholder="Загрузить файл" readonly=""/>
                                    <input type="file" name="background_image"/>
                                    <a class="ui blue button fileUpload">Выбрать</a>
                                </div>
                                <button type="submit" class="ui blue button">Сохранить</button>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
        </section>
    </main>
@endsection
