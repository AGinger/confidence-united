@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">направление деятельности</div>
            <h1 class="ui header">ВИЗОВАЯ ПОДДЕРЖКА/РЕДАКТИРОВАНИЕ</h1>
            <h2 class="sub header"></h2>
        </div>
    </article>
    <main class="page">
        <section class="visa-support">
            <h4 class="ui header">Страны</h4>
            <div class="page-block-input page-block-input_2">
                <form action="{{ route('admin.visa.country.update', ['country' => $country->id]) }}" method="POST">
                    @csrf
                    <div class="ui input">
                        <input type="text" name="name_ru" value="{{ $country->name_ru }}"/>
                        <input type="text" name="name_eng" value="{{ $country->name_eng }}"/>
                    </div>

                    <label class="text" for="number">Номер услуги</label>
                    <select name="service_number" id="service_number" class="ui basic floating dropdown button">
                        <option value="{{ $country->service_number }}" selected>{{ $country->service_number }}</option>
                        @foreach($services as $service)
                            @if($service->number !== $country->service_number)
                            <option value="{{ $service->number }}">{{ $service->number }}</option>
                            @endif
                        @endforeach
                    </select>

                    <label class="text" for="purpose">Цель поездки</label>
                    <select name="purpose" id="purpose" class="ui basic floating dropdown button">
                        <option value="tourism">{{ __('Туризм') }}</option>
                        <option value="visit">{{ __('Деловой визит') }}</option>
                        <option value="work">{{ __('Работа') }}</option>
                    </select>

                    <button type="submit" class="ui blue button">Сохранить</button>
                </form>
            </div>
        </section>
    </main>
@endsection
