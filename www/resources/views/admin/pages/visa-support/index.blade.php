@extends('admin.layouts.app')

@section('content')
    @include('admin.partials.sidebar')
    <article class="article">
        @include('admin.partials.header')
        <div class="header__sub">
            <div class="sub header">направление деятельности</div>
            <h1 class="ui header">ВИЗОВАЯ ПОДДЕРЖКА</h1>
            <h2 class="sub header"></h2>
            <div class="ui top attached tabular menu">
                <div class="item" data-tab="0">Русский</div>
                <div class="item" data-tab="1">Английский</div>
                <div class="item" data-tab="2">Оформление</div>
                <div class="item" data-tab="3">П-взаимод.</div>
                <div class="item" data-tab="4">Услуги</div>
                <div class="item" data-tab="5">МИА</div>
                <div class="item" data-tab="6">Страны</div>
            </div>
        </div>
    </article>
    <main class="page">
        <section class="srvm">
            <div class="ui bottom attached tab segment active" data-tab="0">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_ru->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание русское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_ru->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_ru->category, 'lang' => $page_ru->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__subheader">Метаданные русской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_ru->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_ru->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_ru->description_meta }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>

            <div class="ui bottom attached tab segment" data-tab="1">
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header">Содержание</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Название английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <div class="ui input">
                            <input type="text" name="name" value="{{ $page_eng->name }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Описание английское</h4>
                        <div class="sub header header__subheader">Пояснение</div>
                        <textarea class="ui segment textEditor"
                                  name="description">{{ $page_eng->description }}</textarea>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
                <form
                    action="{{ route('admin.page.store', ['page' => $page_eng->category, 'lang' => $page_eng->language]) }}"
                    method="POST">
                    @csrf
                    <h3 class="ui header header__subheader">Метаданные английской версии</h3>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Title</h4>
                        <div class="sub header header__subheader">не более 3-5 ключевых слов/словосочетаний</div>
                        <div class="ui input">
                            <input type="text" name="title_meta" value="{{ $page_eng->title_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleInputText">
                        <h4 class="ui header header__subtitle">Keywords</h4>
                        <div class="sub header header__subheader">70-80 символов</div>
                        <div class="ui input">
                            <input type="text" name="keywords_meta" value="{{ $page_eng->keywords_meta }}"/>
                        </div>
                    </div>
                    <div class="page-block-titleAreaText">
                        <h4 class="ui header header__subtitle">Description</h4>
                        <div class="sub header header__subheader">мета-описание страницы. Для SEO этот тег один из
                            ключевых.
                        </div>
                        <textarea class="ui segment textEditor"
                                  name="description_meta">{{ $page_eng->description_meta }}</textarea>
                        <button class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="2">
                <form
                    action="{{ route('admin.page.store.image', ['page' => $page_eng->category]) }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <h3 class="ui header">Оформление</h3>
                    <div class="page-block-titleBtnText">
                        <h4 class="ui header">Фоновая иллюстрация</h4>
                        <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div>
                        <img src="{{ asset($page_ru->background_image) }}" alt=""/>
                        <div class="ui action input">
                            <input type="text" placeholder="Загрузить файл" readonly=""/>
                            <input type="file" name="background_image"/>
                            <a class="ui blue button fileUpload">Выбрать</a>
                        </div>
                        <button type="submit" class="ui blue button">Сохранить</button>
                    </div>
                </form>
            </div>
            <div class="ui bottom attached tab segment" data-tab="3">
                <h3 class="ui header">Порядок взаимодействия</h3>
                <div class="page-block-select">
                    <select name="slide" class="ui basic floating dropdown button">
                        <i class="dropdown icon"></i>
                        <div class="menu tab">
                            @foreach($interaction_ru as $int)
                                <option value="{{ $int->slide }}" name="slide" class="item tablinks">
                                    Слайд {{ $int->slide }}</option>
                            @endforeach
                        </div>
                    </select>
                </div>

                <div class="tab">
                    @foreach($interaction_ru as $int)
                        <button class="tablinks ui blue button"
                                onclick="chooseSlide(event, 'ru({{ $int->slide }})', 'eng({{ $int->slide }})')">
                            Слайд {{ $int->slide }}</button>
                    @endforeach
                </div>

                @foreach($interaction_ru as $int)
                    <form
                        action="{{ route('admin.page.store.interaction', ['interaction' => $int->id]) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        <div id="ru({{ $int->slide }})" class="tabcontent" style="display: none">
                            <h3 class="ui header">Русскоязычная версия</h3>
                            <div class="page-block-titleInputText">
                                <h4 class="ui header header__subtitle">Заголовок</h4>
                                <div class="sub header header__subheader">максимум 21 символ, максимальная длина слова
                                    10
                                </div>
                                <div class="ui input">
                                    <input type="text" name="title" value="{{ $int->title }}" required/>
                                </div>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Клиент»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>

                                @foreach(\GuzzleHttp\json_decode($int->unit_client, true) as $client)
                                    <div class="ui input">
                                        <input type="text" name="client[]" value="{{ $client }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach

                                <button class="ui blue button">+</button>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Консультант»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>

                                @foreach(\GuzzleHttp\json_decode($int->unit_consultant, true) as $consultant)
                                    <div class="ui input">
                                        <input type="text" name="consultant[]" value="{{ $consultant }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach
                                <button class="ui blue button">+</button>
                                <h4 class="ui header">Иллюстрация русскоязычная</h4>
                                <div class="page-block-titleBtnText">
                                    <div class="sub header header__subheader">Готовый слайд точного размера 585х300
                                    </div>
                                    <img class="page__img" src="{{ asset($int->background_image) }}" alt=""/>
                                    <div class="ui action input">
                                        <input type="text" placeholder="Загрузить файл" readonly=""/>
                                        <input type="file" name="background_image"/>
                                        <a class="ui blue button fileUpload">Выбрать</a>
                                    </div>
                                    <button type="submit" class="ui blue button">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
                @foreach($interaction_eng as $int)
                    <form
                        action="{{ route('admin.page.store.interaction', ['interaction' => $int->id]) }}"
                        method="POST" enctype="multipart/form-data">
                        @csrf
                        <div id="eng({{ $int->slide }})" class="tabcontent" style="display: none">
                            <h3 class="ui header">Англоязычная версия</h3>
                            <div class="page-block-titleInputText">
                                <h4 class="ui header header__subtitle">Заголовок</h4>
                                <div class="sub header header__subheader">максимум 21 символ, максимальная длина слова
                                    10
                                </div>
                                <div class="ui input">
                                    <input type="text" name="title" value="{{ $int->title }}" required/>
                                </div>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Client»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>
                                @foreach(\GuzzleHttp\json_decode($int->unit_client, true) as $client)
                                    <div class="ui input">
                                        <input type="text" name="client[]" value="{{ $client }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach
                                <button class="ui blue button">+</button>
                            </div>
                            <div class="page-block-titleBtnAddText">
                                <h4 class="ui header header__subtitle page__icon_mark-positive">Блок «Consultant»</h4>
                                <div class="sub header header__subheader">Элемент списка, максимальная длина 60 символов
                                    с
                                    пробелами
                                </div>
                                @foreach(\GuzzleHttp\json_decode($int->unit_consultant, true) as $consultant)
                                    <div class="ui input">
                                        <input type="text" name="consultant[]" value="{{ $consultant }}"/>
                                        <button class="ui blue button">-</button>
                                    </div>
                                @endforeach
                                <button class="ui blue button">+</button>
                            </div>
                            <h4 class="ui header">Иллюстрация англоязычная</h4>
                            <div class="page-block-titleBtnText">
                                <div class="sub header header__subheader">Готовый слайд точного размера 585х300</div>
                                <img class="page__img" src="{{ asset($int->background_image) }}" alt=""/>
                                <div class="ui action input">
                                    <input type="text" placeholder="Загрузить файл" readonly=""/>
                                    <input type="file" name="background_image"/>
                                    <a class="ui blue button fileUpload">Выбрать</a>
                                </div>
                                <button type="submit" class="ui blue button">Сохранить</button>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
            <div class="ui bottom attached tab segment" data-tab="4">
                <h4 class="ui header">Услуги</h4>
                <a href="{{ route('admin.service-type.add', ['page' => $page_ru->category, 'category' => 'strany']) }}"
                   class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Управлять последовательностью иллюстрации
                    в
                    слайдах можно перетаскиванием строк в таблицах.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td data-label="Count">{{ $service->number }}</td>
                            <td data-label="id">{{ $service->id }}</td>
                            <td data-label="Name">{!! $service->name !!} </td>
                            <td data-label="Date">{{ $service->created_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.service.edit', ['serviceType' => 1, 'number' => $service->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <form action="{{ route('admin.service-type.delete', ['service' => $service->id]) }}"
                                      method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="page__icon page__icon_basket"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="ui bottom attached tab segment" data-tab="5">
                <h4 class="ui header">Привязка материалов информационного агентства (МИА)</h4>
                <div class="page-block-select page-block-select_2">
                    <div class="ui basic floating dropdown button">
                        <div class="text">Слайд 1</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item">Слайд 2</div>
                            <div class="item">Слайд 3</div>
                        </div>
                    </div>
                    <div class="ui basic floating dropdown button">
                        <div class="text">Слайд 1</div>
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <div class="item">Слайд 2</div>
                            <div class="item">Слайд 3</div>
                        </div>
                    </div>
                </div>
                <a href="{{ route('admin.service-type.add.mia', ['page' => 'vizovaya-podderzhka', 'category' => 'strany']) }}"
                   class="ui blue button">+ Добавить</a>
                <div class="ui positive message page__icon_info-positive">Сортировать МИА нужно непосредственно в
                    дочерних услугах. Здесь осуществляется только привязка. Управляйте последовательностью вывода
                    перетаскиванием строк.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mias as $mia)
                        <tr>
                            <td data-label="Count">{{ $mia->number }}</td>
                            <td data-label="id">{{ $mia->id }}</td>
                            <td data-label="Name">{!! $mia->name !!}</td>
                            <td data-label="Date">{{ $mia->created_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.mia.edit', ['serviceType' => 1,'number' => $mia->number]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_nolink"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="ui bottom attached tab segment" data-tab="6">
                <h4 class="ui header">Страны</h4>
                <div class="page-block-input page-block-input_2">
                    <form action="{{ route('admin.visa.country.store') }}" method="POST">
                        @csrf
                        <div class="ui input">
                            <input type="text" name="name_ru" placeholder="Русское название" required/>
                            <input type="text" name="name_eng" placeholder="Английское название" required/>
                        </div>

                        <label class="text" for="number">Номер услуги</label>
                        <select name="service_number" id="service_number" class="ui basic floating dropdown button">
                            @foreach($services as $service)
                                <option value="{{ $service->number }}">{{ $service->number }}</option>
                            @endforeach
                        </select>

                        <label class="text" for="purpose">Цель поездки</label>
                        <select name="purpose" id="purpose" class="ui basic floating dropdown button">
                                <option value="tourism">{{ __('Туризм') }}</option>
                                <option value="visit">{{ __('Деловой визит') }}</option>
                                <option value="work">{{ __('Работа') }}</option>
                        </select>

                        <button type="submit" class="ui blue button">Добавить</button>
                    </form>
                </div>
                <div class="ui positive message page__icon_info-positive">Сортировать МИА нужно непосредственно в
                    дочерних услугах. Здесь осуществляется только привязка. Управляйте последовательностью вывода
                    перетаскиванием строк.
                </div>
                <table class="ui celled table">
                    <thead>
                    <tr>
                        <th> №</th>
                        <th> ID</th>
                        <th>Название</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($countries as $country)
                        <tr>
                            <td data-label="Count">{{ $country->id }}</td>
                            <td data-label="id">{{ $country->id }}</td>
                            <td data-label="Name">{{ $country->name_ru }}</td>
                            <td data-label="Date">{{ $country->created_at }}</td>
                            <td data-label="Action">
                                <a href="{{ route('admin.visa.country.edit', ['country' => $country->id]) }}"
                                   class="page__icon page__icon_pen"></a>
                                <button class="page__icon page__icon_eye"></button>
                                <button class="page__icon page__icon_nolink"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </main>
@endsection
