<!DOCTYPE html>
<html lang="ru">
<head>
    @include('layouts.head')
    <title>{{ $page->title_meta }}</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mconsult-inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/page.css') }}">
</head>
<body>
@include('layouts.header.header-copy')
<section class="content">
    <div class="fixed-view">
        <picture>
            <source media="(min-width: 1440px)" srcset="{{ asset($service->background_image) }}">
            <source media="(min-width: 1280px)" srcset="{{ asset($service->background_image) }}">
            <source media="(min-width: 768px)" srcset="{{ asset($service->background_image) }}">
            <source media="(min-width: 564px)" srcset="{{ asset($service->background_image) }}">
            <source media="(min-width: 0px)" srcset="{{ asset($service->background_image) }}">
            <img class="" src="{{ asset($service->background_image) }}"
                 srcset="{{ asset($service->background_image) }} 2x"
                 alt="consalting">
        </picture>
    </div>
    <section class="hero">
        <div class="hero__inner hero__inner--consult-inner">
            <div class="title white">{{ $service->name }}</div>
            <div class="white"><p>
                    {!! $service->description !!}
                </p></div>

            @if($variations !== null)
                <div class="hero-services">
                    @foreach($variations as $variation)
                        <div class="hero-services__item hero-service">
                            <div class="hero-service__row">
                                <div class="hero-service__title">
                                    @lang('pages.migration.inner.term'):
                                </div>
                                <div class="hero-service__desc">
                                    {{ $variation->process_time }} @lang('pages.migration.inner.days')
                                </div>
                            </div>
                            <div class="hero-service__row">
                                <div class="hero-service__title">
                                    @lang('pages.migration.inner.price'):
                                </div>
                                <div class="hero-service__desc">
                                    {{ $variation->price }} @lang('pages.migration.inner.rubles')
                                </div>
                            </div>
                            <div class="hero-service__row">
                                <div class="hero-service__title">
                                    @lang('pages.migration.inner.duty'):
                                </div>
                                <div class="hero-service__desc">
                                    {{ $variation->government_duty }} @lang('pages.migration.inner.rubles')
                                </div>
                            </div>
                            <div class="hero-service__row">
                                <div class="hero-service__title">
                                    @lang('pages.migration.inner.quantity'):
                                </div>
                                <div class="hero-service__desc">
                                    <div class="counter">
                                        <button class="counter__minus"></button>
                                        <input value="1" type="text">
                                        <button class="counter__plus"></button>
                                    </div>
                                </div>
                            </div>
                            <a href="#!" class="hero-service__btn btn">@lang('pages.migration.inner.order')</a>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </section>
    <section class="services-grid">
        <div class="section-title section-title--red">
            <div class="title section-title__title">
                <h2>
                    <a href="#!">@lang('pages.migration.accompanying_services')</a>
                </h2>
            </div>
        </div>
        <div class="services-grid__inner main-grid main-grid_services">
            <div class="main-grid__row clearfix">
                @foreach($other_services as $other_service)
                    @if($other_service->id !== $service->id)
                        <a href="{{ route('mconsult-inner', ['locale' => $locale, 'number' => $other_service->number]) }}"
                           class="main-grid__item main-grid-item">
                            <img class="main-grid-item__img" src="{{ asset($other_service->background_image) }}"
                                 alt="grid-img">
                            <div class="main-grid-item__front">
                                <span class="title">{{ $other_service->name }}</span>
                            </div>
                            <div class="main-grid-item__back">
                                <span class="title">{{ $other_service->name }}</span>
                                <p>{!! $other_service->announcement !!}</p>
                                <div class="btn">
                                    @lang('pages.more')
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </section>

@include('layouts.footer.footer')

</body>
</html>





