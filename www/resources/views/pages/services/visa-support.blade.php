<!DOCTYPE html>
<html lang="{{ $locale }}">
<head>
    @include('layouts.head')
    <title>{{ $page->title_meta }}</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>
<body>
@include('layouts.header.header-copy')
<section class="content">
    <div class="fixed-view">
        <picture>
            <source media="(min-width: 1440px)" srcset="{{ asset($page->background_image) }}">
            <source media="(min-width: 1280px)" srcset="{{ asset($page->background_image) }}">
            <source media="(min-width: 768px)" srcset="{{ asset($page->background_image) }}">
            <source media="(min-width: 564px)" srcset="{{ asset($page->background_image) }}">
            <source media="(min-width: 0px)" srcset="{{ asset($page->background_image) }}">
            <img class="" src="{{ asset($page->background_image) }}"
                 srcset="{{ asset($page->background_image) }} 2x"
                 alt="consalting">
        </picture>
    </div>
    <section class="hero ">
        <div class="hero__inner hero__inner--consult">
            <h1 class="title white">{{ $page->name }}</h1>
            <div class="white">
                <p>
                    {!! $page->description !!}
                </p>
            </div>
            <form id="country_purpose_form" action="{{ route('visa') }}" method="GET">
                @csrf
                <select id="country_purpose_form__country" name="country">
                    @foreach($countries as $country)
                        <option value="{{ $country->service_number }}">@if($locale == 'ru'){{ $country->name_ru }} @else {{ $country->name_eng }} @endif</option>
                    @endforeach
                </select>
                <select id="country_purpose_form__purpose" name="purpose">
                    <option value="all">@lang('pages.service_search_form.purpose_options.all')</option>
                    <option value="visit">@lang('pages.service_search_form.purpose_options.business_visit')</option>
                    <option value="work">@lang('pages.service_search_form.purpose_options.work')</option>
                    <option value="tourism">@lang('pages.service_search_form.purpose_options.tourism')</option>
                </select>
                <button type="submit">@lang('pages.select')</button>
            </form>
        </div>
    </section>
    <section class="services-grid">
        <div class="services-grid__inner main-grid main-grid_services">
            <br>
            <br>
            <div class="main-grid__row clearfix">
                @foreach($services as $service)
                    <a href="{{ route('mconsult-inner', ['locale' => $locale, 'number' => $service->number]) }}" class="main-grid__item main-grid-item">
                        <img class="main-grid-item__img" src="{{ asset($service->background_image) }}"
                             alt="grid-img">
                        <div class="main-grid-item__front">
                            <span class="title">{!! $service->name !!}</span>
                        </div>
                        <div class="main-grid-item__back">
                            <span class="title">{!! $service->name !!}</span>
                            <p>{!! $service->description !!}</p>
                            <div class="btn">
                                @lang('pages.more')
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </section>
    <section class="steps-slider">
        <div class="steps-slider__main-title white">
            <h2>
                @lang('pages.interaction')
            </h2>
        </div>
        <div class="steps-slider__body">
            @foreach($interactions as $interaction)
                <div class="steps-slider__slide mobile-accordion-body active">
                    <div class="mobile-accordion-head mobile-accordion-head--step1 white">
                        {{ $interaction->title }}
                    </div>
                    <div class="steps-slider__inner">
                        <div class="steps-slider__img">
                            <img src="{{ asset($interaction->icon) }}" alt="step">
                        </div>
                        <div class="steps-slider__text">
                            @php
                                $clients = json_decode($interaction->unit_client, true);
                                $consultants = json_decode($interaction->unit_consultant, true);
                            @endphp
                            <div class="steps-slider__title">@lang('pages.client')</div>
                            <ul>
                                @foreach($clients as $client)
                                    <li>
                                        <span>{{ $client }}</span>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="steps-slider__title">@lang('pages.consultant')</div>
                            <ul>
                                @foreach($consultants as $consultant)
                                    <li>
                                        <span>{{ $consultant }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="steps-slider__main-title white">
            <h2>
                @lang('pages.offers')
            </h2>
        </div>
    </section>
    <section class="articles">
        <div class="articles__inner">
            <!-- <h2 class="articles__title red title"></h2> -->
            <div class="articles__slider articles-slider slider">
                @foreach($sales as $sale)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date">{{ $sale->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('mconsult.sale', ['number' => $sale->number]) }}">
                                        {!! $sale->name !!}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {!! $sale->description !!}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('mconsult.sale', ['number' => $sale->number]) }}">
                                    <img src="{{ asset($sale->background_image) }}" class="safari-fix"
                                         data-format="jpg"
                                         alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
</section>

@include('layouts.footer.footer')
</body>

</html>
