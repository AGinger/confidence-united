<!DOCTYPE html>
<html lang="ru">
<head>
    @include('layouts.head')
    <title>{{ $page->title_meta }}</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/mconsult-inner.css') }}">
    <link rel="stylesheet" href="{{ asset('css/page.css') }}">
</head>
<body>
@include('layouts.header.header-copy')
<section class="articles">
    <div class="articles-slide">
        <div class="articles-slide__left">
            <p class="articles-slide__date">{{ $sale->created_at }}</p>
            <div class="articles-slide__title">
                <p class="page__link"
                   href="http://old.366033-cb87966.tmweb.ru/eng/news/measures-to-observe-foreigners-stay-in-russia/">
                    {!! $sale->name !!}
                </p>
            </div>
            <p class="articles-slide__desc">
                {!! $sale->description !!}
            </p>
        </div>
        <div class="articles-slide__right">
            <p class="page__link"
               href="http://old.366033-cb87966.tmweb.ru/eng/news/measures-to-observe-foreigners-stay-in-russia/">
                <img src="{{ asset($sale->background_image) }}" class="safari-fix"
                     data-format="jpg"
                     alt="article-pic">
            </p>
        </div>
    </div>
</section>
@include('layouts.footer.footer')
</body>

</html>

