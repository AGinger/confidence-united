<!DOCTYPE html>
<html lang="{{ $locale }}">

<head>
    @include('layouts.head')
    <title>{{ $name }}</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/event.css') }}">
</head>

<body>
<header class="header sticky">
    <div class="header__inner">
        <div class="header__left">
            <a class="logo" href="/"></a>
        </div>
        <div class="header__mid">
            <nav>
                <ul class="header__nav">
                    <li>
                        <a href="{{route('inform-agency.news')}}">@lang('pages.news')</a>
                    </li>
                    <li>
                        <a href="{{route('inform-agency.publications')}}">@lang('pages.publications')</a>
                    </li>
                    <li>
                        <a href="{{route('inform-agency.events')}}">@lang('pages.events')</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header__right">

            <div class="header-options">
                <a href="#" class="header-option header-option--geo open-modal" data-modal="modal-geo">Москва</a>
                <a href="#" class="header-option header-option--search">@lang('pages.search')</a>
                @if($locale == 'ru')
                    <a class="header-option header-option--lang"
                       href="{{\App\Helper::changeLang('en')}}">@lang('pages.lang')</a>
                @else
                    <a class="header-option header-option--lang"
                       href="{{\App\Helper::changeLang('ru')}}">@lang('pages.lang')</a>
                @endif
            </div>

            <a class="menu-btn-mobile">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </a>
        </div>
    </div>
</header>


<section class="content">
    <section class="event">
        <div class="event__inner">
            <div class="event__title">
                <h1>{{ $page->name }}</h1>
            </div>
            <div class="event__date">{{ $page->created_at }}</div>
            <div class="event__img">
                <img src="{{ asset($page->background_image) }}" alt="event-pic">
            </div>
            <div class="event__body">
                <div class="event__content">
                    <p>
                        {{ $page->description }}
                    </p>
                </div>
                <div class="event__sidebar">
                    <div class="event__btns">
                        <a href="#!" class="event__btn btn open-modal" data-modal="modal-share">@lang('pages.share')</a>
                    </div>
                    <div class="event__info event-info">
                        <div class="event-info__author">
                            <div class="event-info__row">
                                <div class="event-info__title">@lang('pages.author'):</div>
                                <div class="event-info__desc"> Andrei Sprinchan, CEO</div>
                            </div>
                            <div class="event-info__row">
                                <div class="event-info__title">@lang('pages.photo'):</div>
                                <div class="event-info__desc">shutterstock</div>
                            </div>
                        </div>


                        <div class="event-info__row">
                            <div class="event-info__item event-info__item--views">324</div>
                        </div>
                        <div class="event-info__row">
                            <div class="event-info__item event-info__item_downloads">25</div>
                        </div>
                        <div class="event-info__row">
                            <a href="#!" class="event-info__item event-info__item--print">@lang('pages.print')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</section>

<div class="bottom-nav" style="bottom: 0px;">
    <a href="#" class="bell"></a>
    <a href="#" class="next"></a>
</div>
@php
    if ($locale == 'ru') {
        $lang = \App\Models\Page::LANGUAGE_RU;
    } else {
        $lang = \App\Models\Page::LANGUAGE_ENG;
    }
       $footer_info = \App\Models\MainPage::where('language', $lang)->first();
@endphp

<footer class="footer-sub">
    <div class="footer-sub__inner">
        <picture>
            <source media="(min-width: 1440px)"
                    srcset="{{ asset('images/1440/subscripbe-1440.webp, images/1440/subscripbe-1440.jpg') }}">
            <source media="(min-width: 1280px)"
                    srcset="{{ asset('images/1280/subscripbe-1280.webp') }}, {{ asset('images/1280/subscripbe-1280.jpg') }}">
            <source media="(min-width: 768px)"
                    srcset="{{ asset('images/768/subscripbe-768.webp, images/768/subscripbe-768.jpg') }}">
            <source media="(min-width: 564px)"
                    srcset="{{ asset('images/564/subscripbe-564.webp, images/564/subscripbe-564.jpg') }}">
            <source media="(min-width: 0px)"
                    srcset="{{ asset('images/400/subscripbe-400.webp') }}, {{ asset('images/400/subscripbe-400.jpg') }}">
            <img class="image-fallback" src="{{ asset('images/768/subscripbe-768.jpg') }}" alt="footer-bg">
        </picture>
        <div class="footer-sub__title title">
            <h2>@lang('pages.agency.footer.subscribe_for_information')</h2>
        </div>
        <div class="footer-sub__desc">
            <p class="red">@lang('pages.agency.footer.actual_events')</p>
        </div>
        <form action="" class="footer-sub__form">
            <div class="footer-sub__row">
                <input class="footer-sub__input" type="text" placeholder="@lang('pages.agency.footer.name')">
                <input class="footer-sub__input" type="email" placeholder="E-MAIL">
            </div>
            <button class="footer-sub__submit btn">@lang('pages.agency.footer.subscribe')</button>
        </form>
        <div class="soc">
            <a href="{{ $metaOptions['twitter_url_meta']->value }}" class="soc__link soc__link--tw"></a>
            <a href="{{ $metaOptions['facebook_url_meta']->value }}" class="soc__link soc__link--fb"></a>
            <a href="{{ $metaOptions['linkedin_url_meta']->value }}" class="soc__link soc__link--in"></a>
            <a href="{{ $metaOptions['vkontakte_url_meta']->value }}" class="soc__link soc__link--vk"></a>
        </div>
        <p class="footer-sub__extra">
            @lang('pages.agency.footer.information')
        </p>

    </div>
    <div class="footer__bot">
        @lang('pages.footer.rights')
    </div>
</footer>

<!-- MODALS -->
<div class="popup-form" id="modal-geo">
    <div class="inner">
        <div class="close close-modal" data-modal="modal-geo"></div>
        <div class="title big">@lang('pages.current_region')<br><span class="red">Moscow</span></div>
        <!--
        <div class="btn"><a href="#">Change region</a></div>
         -->
    </div>
</div>
<div class="popup-form" id="modal-register">
    <div class="inner">
        <div class="close close-modal" data-modal="modal-register"></div>
        <div class="title big">Round tables on current migration issues</div>
        <div class="descr">June 22 at 10:00</div>
        <form>
            <div class="row text"><input placeholder="Full name*"></div>
            <div class="row text left"><input placeholder="Phone*"></div>
            <div class="row text right"><input placeholder="E-mail*"></div>
            <div class="row text left"><input placeholder="Organization*"></div>
            <div class="row text right"><input placeholder="Position*"></div>
            <div class="row submit"><input type="submit" value="Register"></div>
            <div>* Required fields</div>
        </form>
    </div>
</div>


<div class="popup-form" id="modal-share">
    <div class="inner">
        <div class="close close-modal" data-modal="modal-share"></div>
        <div class="title big">Public discussion of migration policy</div>

        <div class="soc grey">
            <a href="{{ $metaOptions['twitter_url_meta']->value }}" class="soc__link soc__link--tw"></a>
            <a href="{{ $metaOptions['facebook_url_meta']->value }}" class="soc__link soc__link--fb"></a>
            <a href="{{ $metaOptions['linkedin_url_meta']->value }}" class="soc__link soc__link--in"></a>
            <a href="{{ $metaOptions['vkontakte_url_meta']->value }}" class="soc__link soc__link--vk"></a>
        </div>
    </div>
</div>
<!-- /MODALS -->
<!-- /MODALS -->
<div class="popup-form modal-reviews close-modal" id="modal-reviews" style="display: none;">
    <div class="inner ">
        <div class="close close-modal" data-modal="modal-reviews"></div>
        <div class="reviews-list">
            <div class="testimonials__title title">
                <h2>ОТЗЫВЫ</h2>
            </div>

            <div class="testimonials__slider slider">
                <div class="testimonials__slide">
                    <div class="testimonials__slide-inner testimonial">
                        <div class="testimonial__desc">
                            <p>
                                Разнообразный и богатый опыт рамки и место обучения кадров позволяет выполнять важные
                                задания по разработке форм развития. Равным образом укрепление и развитие структуры
                                представляет собой интересный эксперимент проверки систем массового участия. Равным
                                образом постоянный количественный рост и сфера нашей активности влечет за собой процесс
                                внедрения и модернизации системы обучения кадров, соответствует насущным потребностям.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Таким образом новая модель организационной деятельности играет важную роль в формировании
                            форм развития. Задача организации, в особенности же начало повседневной работы по
                            формированию позиции требуют определения и уточнения существенных финансовых и
                            административных условий. С другой стороны реализация намеченных плановых заданий требуют
                            определения и уточнения модели развития. Не следует, однако забывать, что постоянный
                            количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов)
                            участие в формировании существенных финансовых и административных условий.
                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Задача организации, в особенности же рамки и место обучения кадров позволяет оценить
                            значение позиций, занимаемых участниками в отношении поставленных задач. Задача организации,
                            в особенности же начало повседневной работы по формированию позиции в значительной степени
                            обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.
                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Значимость этих проблем настолько очевидна, что консультация с широким активом позволяет
                            выполнять важные задания по разработке систем массового участия. Таким образом дальнейшее
                            развитие различных форм деятельности влечет за собой процесс внедрения и модернизации
                            соответствующий условий активизации. Разнообразный и богатый опыт реализация намеченных
                            плановых заданий в значительной степени обуславливает создание форм развития.

                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм
                            деятельности играет важную роль в формировании систем массового участия.

                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм
                            деятельности играет важную роль в формировании систем массового участия.
                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм
                            деятельности играет важную роль в формировании систем массового участия.

                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм
                            деятельности играет важную роль в формировании систем массового участия.

                        </p>
                    </div>

                </div>
                <div class="testimonials__slide-inner testimonial">
                    <div class="testimonial__desc">
                        <p>
                            Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм
                            деятельности играет важную роль в формировании систем массового участия.
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('libs/slick.min.js') }}"></script>
<script src="{{ asset('libs/simplaLightbox/simpleLightbox.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
      rel="stylesheet">
</body>

</html>
