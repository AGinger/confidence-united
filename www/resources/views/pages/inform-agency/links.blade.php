@if($paginator->hasPages())
    <div class="numpages">
        @if($paginator->currentPage() != 1)
            <a class="nx" href="{{ $paginator->previousPageUrl() }}">&lt;</a>
        @endif
        @if ($paginator->onFirstPage())
            <a href="{{ $paginator->url(1) }}" class="modern-page-first active">1</a>
            <a href="{{ $paginator->nextPageUrl() }}" class="modern-page-first">2</a>
            @if($paginator->currentPage() != $paginator->lastPage())
                ...
                <a href="{{ $paginator->url($paginator->lastPage()) }}"
                   class="modern-page-first">{{ $paginator->lastPage() }}</a>
            @endif
        @else
            <a href="{{ $paginator->url(1) }}" class="modern-page-first">1</a>
            @if($paginator->currentPage() != $paginator->lastPage())
                @if($paginator->currentPage() != 2)
                    @if($paginator->currentPage() != 3)
                        ...
                    @endif
                    <a href="{{ $paginator->previousPageUrl() }}"
                       class="modern-page-first">{{ $paginator->currentPage() - 1 }}</a>
                @endif
                <a href="{{ $paginator->url($paginator->currentPage()) }}"
                   class="modern-page-first active">{{ $paginator->currentPage() }}</a>
                @if($paginator->currentPage() + 1 != $paginator->lastPage())
                    <a href="{{ $paginator->nextPageUrl() }}"
                       class="modern-page-first">{{ $paginator->currentPage() + 1 }}</a>
                    @if($paginator->currentPage() + 2 != $paginator->lastPage())
                        ...
                    @endif
                @endif
                <a href="{{ $paginator->url($paginator->lastPage()) }}"
                   class="modern-page-first">{{ $paginator->lastPage() }}</a>
            @else
                ...
                <a href="{{ $paginator->previousPageUrl() }}"
                   class="modern-page-first">{{ $paginator->currentPage() - 1 }}</a>
                <a href="{{ $paginator->nextPageUrl() }}"
                   class="modern-page-first active">{{ $paginator->currentPage() }}</a>
            @endif
        @endif
        @if($paginator->currentPage() != $paginator->lastPage())
            <a class="nx" href="{{ $paginator->nextPageUrl() }}">&gt;</a>
        @endif
    </div>
@endif
