@extends('layouts.inform-agency')

@section('content')
    <div id="news" class="anchors"></div>
    <section class="articles">
        <div class="articles__inner">
            <div class="articles__title red title">
                <h2>@lang('pages.news')</h2>
            </div>
            <div class="articles__slider articles-slider slider">
                @foreach($news as $item)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date"> {{ $item->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('inform-agency.item', ['type' => $item->type, 'agency' => $item->id]) }}">
                                        {!! $item->name !!}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {!! $item->description !!}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('inform-agency.item', ['type' => $item->type, 'agency' => $item->id]) }}">
                                    <img src="{{ asset($item->background_image) }}" class="safari-fix" data-format="jpg"
                                         alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <a href="{{ route('inform-agency.news', ['locale' => $locale]) }}" class="articles-slide__btn btn">@lang('pages.all') @lang('pages.news')</a>
        </div>
    </section>
    <div id="publication" class="anchors"></div>
    <section class="articles">
        <div class="articles__inner">
            <div class="articles__title red title">
                <h2>@lang('pages.publications')</h2>
            </div>
            <div class="articles__slider articles-slider slider">
                @foreach($publications as $publication)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date">{{ $publication->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('inform-agency.item', ['type' => $publication->type, 'agency' => $publication->id]) }}">
                                        {{ $publication->name }}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {{ $publication->description }}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $publication->type, 'agency' => $publication->id]) }}">
                                    <img src="{{ asset($publication->background_image) }}" class="safari-fix"
                                         data-format="jpg" alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a href="{{ route('inform-agency.publications', ['locale' => $locale]) }}" class="articles-slide__btn btn">@lang('pages.all') @lang('pages.publications')</a>
        </div>
    </section>
    <div id="event"  class="anchors"></div>
    <section class="articles">
        <div class="articles__inner">
            <div class="articles__title red title">
                <h2>@lang('pages.events')</h2>
            </div>
            <div class="articles__slider articles-slider slider">
                @foreach($events as $event)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date">{{ $event->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $event->type, 'agency' => $event->id]) }}">
                                        {{ $event->name }}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {{ $event->description }}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $event->type, 'agency' => $event->id]) }}">
                                    <img src="{{ asset($publication->background_image) }}" class="safari-fix"
                                         data-format="jpg" alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a href="{{ route('inform-agency.events', ['locale' => $locale]) }}" class="articles-slide__btn btn">@lang('pages.all') @lang('pages.events')</a>
        </div>
    </section>
</section>
@endsection



