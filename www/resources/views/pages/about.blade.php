<!DOCTYPE html>
<html lang="{{ $locale }}">
<head>
    @include('layouts.head')
    <title>{{ $page->title_meta }}</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">
</head>
<body>
@include('layouts.header.header-copy')
@include('partials.review-modal')

<section class="content">
    <div class="fixed-view">
        <picture>
            <source media="(min-width: 1440px)" srcset="{{ asset('images/1440/about-bg-1440.webp') }}">
            <source media="(min-width: 1280px)" srcset="{{ asset('images/1280/about-bg-1280.webp') }}">
            <source media="(min-width: 768px)" srcset="{{ asset('images/768/about-bg-768.webp') }}">
            <source media="(min-width: 564px)" srcset="{{ asset('images/564/about-bg-564.webp') }}">
            <source media="(min-width: 0px)" srcset="{{ asset('images/400/about-bg-400.webp') }}">
            <img class="" src="{{ asset('images/768/about-bg-768.jpg') }}"
                 srcset="{{ asset('images/768/about-bg-768.jpg') }} 1x, {{ asset('images/1440/about-bg-1440.jpg') }} 2x"
                 alt="about-bg">
        </picture>
    </div>
    <section class="hero">
        <div class="hero__inner hero__inner--about">
            <div class="white title">@lang('pages.about.we')</div>
            <div class="title red">{{ $page->name }}</div>
            <div class="title white">{!! $page->description !!}
            </div>
            @php
                $file = explode('/', $meta->presentation);
            @endphp
            <a href="{{ route('about.get-file', ['file' => $file[1]]) }}"
               class="btn">@lang('pages.about.presentation_button')</a>
        </div>
    </section>
    <section class="infograhics">
        <div class="infograhics__inner clearfix">
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/1.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/2.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item infograhics-item--wx2">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/3.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item infograhics-item--hx2">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/4.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/5.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/6.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/7.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item infograhics-item--hx2">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/8.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item infograhics-item--wx2">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/9.png') }}" alt="infographics">
                </div>
            </div>

            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/10.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/11.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item infograhics-item--hx2 infograhics-item--top-shift">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/12.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/13.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/14.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/15.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/16.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item infograhics-item--wx2">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/17.png') }}" alt="infographics">
                </div>
            </div>
            <div class="infograhics__item infograhics-item">
                <div class="infograhics-item__img">
                    <img src="{{ asset('images/company-infographics/18.png') }}" alt="infographics">
                </div>
            </div>
        </div>
    </section>

    <section class="testimonials">
        <div class="testimonials__inner">
            <div class="testimonials__title title">
                <h2>@lang('pages.about.reviews')</h2>
            </div>
            <div class="testimonials__dec">“</div>
            <div class="testimonials__slider slider">
                @foreach($reviews as $review)
                    <div class="testimonials__slide">
                        <div class="testimonials__slide-inner testimonial">
                            <div class="testimonial__desc">
                                {!! $review->description !!}
                            </div>
                            <div class="testimonial__author">
                                <p>
                                    {{ $review->author }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <a href="#" class="open-modal open-review-modal" data-modal="modal-review">@lang('pages.about.feedback')</a>
    </section>

    <section class="team">
        <picture>
            <source media="(min-width: 1440px)" srcset="{{ asset('images/1440/team-bg-1440.webp') }}">
            <source media="(min-width: 1280px)" srcset="{{ asset('images/1280/team-bg-1280.webp') }}">
            <source media="(min-width: 768px)" srcset="{{ asset('images/768/team-bg-768.webp') }}">
            <source media="(min-width: 564px)" srcset="{{ asset('images/564/team-bg-564.webp') }}">
            <source media="(min-width: 0px)" srcset="{{ asset('images/400/team-bg-400.webp') }}">
            <img class="image-fallback" src="{{ asset('images/768/team-bg-768.jpg') }}"
                 srcset="{{ asset('images/1440/team-bg-1440.jpg') }} 2x"
                 alt="team-bg">
        </picture>
        <div class="team__title title red">
            <h2>@lang('pages.about.team')</h2>
        </div>
        <div class="team__inner">
            <div class="page_desktop team__slider slider">
                <div class="team__slide">
                    <div class="team__slide-inner">

                        @foreach($team as $person)
                            <div class="team-member">
                                <div class="team-member__img">
                                    <img src="{{ asset($person->image_first) }}"
                                         alt="alt">
                                    <img src="{{ asset($person->image_second) }}"
                                         alt="alt">
                                </div>
                                @if($locale == 'ru')
                                    <div class="team-member__name">{{ $person->name_ru }}</div>
                                @else
                                    <div class="team-member__name">{{ $person->name_eng }}</div>
                                @endif
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <!-- Duplicate for mobile slider -->
            <div class="page_mobile team__slider slider">

                @foreach($team as $person)
                    <div class="team__slide">
                        <div class="team__slide-inner">
                            <div class="team-member">
                                <div class="team-member__img">
                                    <img src="{{ asset($person->image_first) }}"
                                         alt="alt">
                                    <img src="{{ asset($person->image_second) }}"
                                         alt="alt">
                                </div>
                                <div class="team-member__name">{{ $person->name_ru }}</div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
    <input id="video" value="{{ $meta->video_link }}" style="display: none"/>
    <div class="video-about">
        <div class="video-about__inner">
            <div class="youtube-player" data-id="BzknewK_UaE"></div>
        </div>
    </div>
    <div class="serts">
        <div class="serts__title title">
            <h2>@lang('pages.about.certificates')</h2>
        </div>
        <div class="serts__inner">
            <div class="serts__slider serts-slider">
                <div class="serts__slide">
                    <div class="serts-slider__inner imageGallery">
                        @foreach($certificates as $certificate)
                            <a href="({{ asset($certificate->name) }})">
                                <img alt="Gallery image"
                                     src="{{ asset($certificate->image) }}">
                            </a>
                        @endforeach
                    </div>
                </div>

                <div class="serts__slide">
                    <div class="serts-slider__inner imageGallery">
                        @foreach($certificates as $certificate)
                            <a href="({{ asset($certificate->name) }})">
                                <img alt="Gallery image"
                                     src="{{ asset($certificate->image) }}">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('layouts.footer.footer')
<script src="{{ asset('js/sticky.js') }}"></script>
</body>

</html>
