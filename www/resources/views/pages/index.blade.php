<!DOCTYPE html>
<html lang="{{ $locale }}">
<head>
    @include('layouts.head')
    <title>{{ $page->title_meta }}</title>

    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">

</head>
<body>
@include('layouts.header.header')
<section class="content">
    <div class="fixed-view contrast-view">
        <video class="page-video"
               autoplay="true"
               muted="muted"
               loop="loop"
               id="hero-video"
               poster="{{ asset('images/400/video-prompt-400.jpg') }}">
        </video>
    </div>
    <section class="hero hero--main" data-slide="slideTo">
        <div class="hero__inner hero__inner--main">
            <div class="white">@lang('pages.main.hi')</div>
            <div class="title white">@lang('pages.main.confidence')</div>
            <div class="red">@lang('pages.main.company')</div>
        </div>
    </section>
    <section class="services-grid " data-slide="slideTo" id="services">
        <div class="section-title">
            <div class="title section-title__title">
                <h2>
                    <a href="#">@lang('pages.main.services')</a>
                </h2>
            </div>
            <div class="section-title__desc">@lang('pages.main.services_text')</div>
        </div>
        <div class="services-grid__inner main-grid main-grid--index">
            <div class="main-grid__row clearfix">
                <a href="{{ route('mconsult') }}" class="main-grid__item main-grid-item main-grid-item--w64">
                    <img class="main-grid-item__img safari-fix" data-format="jpg"
                         src="{{ asset($mconsult->background_image) }}"
                         alt="grid-img">
                    
                    <div class="main-grid-item__front">
                        <span class="title">@lang('pages.migration.name')</span>
                    </div>

                    <!-- Migration Consulting Block -->
                    <div class="main-grid-item__back">
                        <span class="title">@lang('pages.migration.name')</span>
                        <p>{{ $mconsult->description }}</p>
                        <div class="btn">
                            @lang('pages.more')
                        </div>
                    </div>
                </a>
                <a href="{{route('visa')}}" class="main-grid__item main-grid-item">

                    <img class="main-grid-item__img safari-fix" src="{{ asset($visa->background_image) }}"
                         alt="grid-img"
                         data-format="jpg">

                    <div class="main-grid-item__front">
                        <span class="title">@lang('pages.visa.name')</span>
                    </div>
                    <div class="main-grid-item__back">
                        <span class="title">@lang('pages.visa.name')</span>
                        <p>{{ $visa->description }}</p>
                        <div class="btn">
                            @lang('pages.more')
                        </div>
                    </div>
                </a>
            </div>
            <div class="main-grid__row clearfix">
                <a href="{{ route('relocation') }}" class="main-grid__item main-grid-item main-grid-item--x2height">

                    <img class="main-grid-item__img safari-fix" data-format="jpg"
                         src="{{ asset($relocation->background_image) }}" alt="grid-img">

                    <div class="main-grid-item__front">
                        <span class="title">@lang('pages.relocation.name')</span>
                    </div>
                    <div class="main-grid-item__back">
                        <span class="title">@lang('pages.relocation.name')</span>
                        <p>{{ $relocation->description }}</p>
                        <div class="btn">
                            @lang('pages.more')
                        </div>
                    </div>
                </a>
                <a class="main-grid__item main-grid-item main-grid-item--w28 main-grid-item--center">
                    <div class="main-grid-item__front">
                        <span class="title">@lang('pages.main.services_center_text')</span>
                    </div>

                </a>
                <a href="{{ route('accreditation') }}" class="main-grid__item main-grid-item">

                    <img class="main-grid-item__img safari-fix" data-format="jpg"
                         src="{{ asset($accreditation->background_image)}}" alt="grid-img">
                    <div class="main-grid-item__front">
                        <span class="title">@lang('pages.accreditation.name')</span>
                    </div>
                    <div class="main-grid-item__back">
                        <span class="title">@lang('pages.accreditation.name')</span>
                        <p>{{ $accreditation->description }}</p>
                        <div class="btn">
                            @lang('pages.more')
                        </div>
                    </div>
                </a>
                <a href="{{ route('travel') }}" class="main-grid__item main-grid-item main-grid-item--w64">

                    <img class="main-grid-item__img safari-fix" data-format="jpg"
                         src="{{ asset($travel->background_image) }}"
                         alt="grid-img">
                    <div class="main-grid-item__front">
                        <span class="title">@lang('pages.travel.name')</span>
                    </div>
                    <div class="main-grid-item__back">
                        <span class="title">@lang('pages.travel.name')</span>
                        <p>{{ $travel->description }}</p>
                        <div class="btn">
                            @lang('pages.more')
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <section class="customers" data-slide="slideTo" id="customers">
        <div class="section-title section-title--red">
            <div class="title section-title__title">
                <h2>
                    <a href="#">@lang('pages.main.customers')</a>
                </h2>
            </div>
            <div class="section-title__desc">@lang('pages.main.customers_text')</div>
        </div>
        <div class="customers__inner">
            <div class="customers__col">
                <div class="customers__title title">@lang('pages.main.brands_rus')</div>
                <div class="customers__slider customers-slider slider">
                    <div class="slider__slide">
                        <div class="slider__slide-wrap">
                            @foreach($clients_ru_first as $client)
                                <img src="{{ asset($client->image) }}" alt="brand">
                            @endforeach
                        </div>
                    </div>
                    <div class="slider__slide">
                        <div class="slider__slide-wrap">
                            @foreach($clients_ru_second as $client)
                                <img src="{{ asset($client->image) }}" alt="brand">
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="customers__col">
                <div class="customers__title title">@lang('pages.main.brands_int')</div>
                <div class="customers__slider customers-slider slider">
                    <div class="slider__slide">
                        <div class="slider__slide-wrap">
                            @foreach($clients_int_first as $client)
                                <img src="{{ asset($client->image) }}" alt="brand">
                            @endforeach
                        </div>
                    </div>
                    <div class="slider__slide">
                        <div class="slider__slide-wrap">
                            @foreach($clients_int_second as $client)
                                <img src="{{ asset($client->image) }}" alt="brand">
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

   <!-- Informagency block -->
    <div class="section-info-agency section-title section-title--transparent" data-slide="slideTo" id="inform-agency">
        <div class="title section-title__title">
            <h2>
                <a href="{{ route('inform-agency.index', ['locale' => $locale]) }}">@lang('pages.agency.name')</a>
            </h2>
        </div>
        <div class="section-title__desc red">@lang('pages.main.agency_text')
        </div>
        <a class="page__btn btn" href="{{ route('inform-agency.index', ['locale' => $locale]) }}">
            @lang('pages.more')
        </a>
    </div>

    <!-- Adding label for scrolling -->
    <section class="articles"  data-slide="slideTo">
        <div class="articles__inner">
            <div class="articles__title red title">
                <h2> @lang('pages.news')</h2>
            </div>
            <div class="articles__slider articles-slider slider">
                @foreach($news as $item)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date"> {{ $item->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $item->type, 'agency' => $item->id]) }}">
                                        {!! $item->name !!}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {!! $item->description !!}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $item->type, 'agency' => $item->id]) }}">
                                    <img src="{{ asset($item->background_image) }}" class="safari-fix" data-format="jpg"
                                         alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a href="{{ route('inform-agency.news', ['locale' => $locale]) }}"
               class="articles-slide__btn btn"> @lang('pages.all') @lang('pages.news')</a>
        </div>
    </section>
    <section class="articles" data-slide="slideTo" >
        <div class="articles__inner">
            <div class="articles__title red title">
                <h2>@lang('pages.publications')</h2>
            </div>
            <div class="articles__slider articles-slider slider">
                @foreach($publications as $publication)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date">{{ $publication->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $publication->type, 'agency' => $publication->id]) }}">
                                        {!! $publication->name !!}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {!! $publication->description !!}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $publication->type, 'agency' => $publication->id]) }}">
                                    <img src="{{ asset($publication->background_image) }}" class="safari-fix"
                                         data-format="jpg" alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a href="{{ route('inform-agency.publications', ['locale' => $locale]) }}"
               class="articles-slide__btn btn">@lang('pages.all') @lang('pages.publications')</a>
        </div>
    </section>
    <section class="articles" data-slide="slideTo" >
        <div class="articles__inner">
            <div class="articles__title red title">
                <h2>@lang('pages.events')</h2>
            </div>
            <div class="articles__slider articles-slider slider">
                @foreach($events as $event)
                    <div class="articles__slide slide">
                        <div class="articles-slide">
                            <div class="articles-slide__left">
                                <p class="articles-slide__date">{{ $event->created_at }}</p>
                                <div class="articles-slide__title">
                                    <a class="page__link"
                                       href="{{ route('inform-agency.item', ['locale' => $locale,  'type' => $event->type, 'agency' => $event->id]) }}">
                                        {!! $event->name !!}
                                    </a>
                                </div>
                                <p class="articles-slide__desc">
                                    {!! $event->description !!}
                                </p>
                            </div>
                            <div class="articles-slide__right">
                                <a class="page__link"
                                   href="{{ route('inform-agency.item', ['locale' => $locale, 'type' => $event->type, $event->id]) }}">
                                    <img src="{{ asset($event->background_image) }}" class="safari-fix"
                                         data-format="jpg"
                                         alt="article-pic">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <a href="{{ route('inform-agency.events', ['locale' => $locale]) }}"
               class="articles-slide__btn btn"> @lang('pages.all')  @lang('pages.events')</a>
        </div>
    </section>
</section>

@include('layouts.footer.footer')
</body>

</html>
