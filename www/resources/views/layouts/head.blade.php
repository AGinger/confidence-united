@include('layouts.version')

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<base href="{{ url('/') }}">
@if(isset($page))
    <meta name="description" content="{{ $page->description_meta }}">
    <meta name="author" content="{{ $page->title_meta }}">
    <meta name="keywords" content="{{ $page->keywords_meta }}">

    <meta property="og:locale" content="{{ $locale }}" />
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="Конфиденс Групп" />
    <meta property="og:title" content="{{ $page->title_meta }}" />
    <meta property="og:description" content="{{ $page->description_meta }}" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:image" content="images/icons/confidence-og.png" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="{{ $metaOptions['twitter_account_meta']->value }}">
    <meta name="twitter:title" content="{{ $page->title_meta }}">
    <meta name="twitter:description" content="{{ $page->description_meta }}">
    <meta name="twitter:image" content="images/icons/confidence-og.png">
@endif

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Corporation",
    "name": "Конфиденс Групп",
    "url": "{{ url('/') }}",
    "logo": "images/icons/speeddial-160px.png",
    "alternateName": "Конфиденс",
    "sameAs": [
        "{{ $metaOptions['twitter_url_meta']->value }}",
        "{{ $metaOptions['facebook_url_meta']->value }}",
        "{{ $metaOptions['instagram_url_meta']->value }}",
        "{{ $metaOptions['linkedin_url_meta']->value }}",
    ],
    "contactPoint": [
        {
            "@type": "ContactPoint",
            "telephone": "{{ $metaOptions['phone_number_meta']->value }}",
            "contactType": "technical support",
            "email": "{{ $metaOptions['email_technical_support_meta']->value }}",
            "availableLanguage": [
                "en",
                "ru"
            ]
        }
    ]
}
</script>
<meta http-equiv="content-language" content="{{ $locale }}">
<link rel="icon" type="image/x-icon" href="images/icons/favicon.ico" />
<link rel="icon" href="images/icons/icon.svg" sizes="any" type="image/svg+xml">
<link rel="icon" href="images/icons/favicon-16.png" sizes="16x16" type="image/png">
<link rel="icon" href="images/icons/favicon-32.png" sizes="32x32" type="image/png">
<link rel="icon" type="image/png" href="images/icons/speeddial-160px.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/icons/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/icons/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="/ !images/icons/apple-touch-icon-57x57-precomposed.png|thumbnail!!images/icons/apple-touch-icon-72x72-precomposed.png|thumbnail!  !images/icons/speeddial-160px.png|thumbnail!  !images/icons/apple-touch-icon-114x114-precomposed.png|thumbnail!  [^favicon.ico] images/icons/apple-touch-icon-57x57-precomposed.png">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="">

