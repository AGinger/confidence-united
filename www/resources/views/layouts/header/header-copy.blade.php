<header class="header">
    <div class="header__inner">
        <div class="header__left">
            <a class="logo" href="/"></a>
        </div>
        <div class="header__mid">
            <nav>
                <ul class="header__nav">
                    <li>
                        <a href="{{route('mconsult')}}">@lang('pages.migration.name')</a>
                    </li>
                    <li>
                        <a href="{{route('visa')}}">@lang('pages.visa.name')</a>
                    </li>
                    <li>
                        <a href="{{route('relocation')}}">@lang('pages.relocation.name')</a>
                    </li>
                    <li>
                        <a href="{{route('accreditation')}}">@lang('pages.accreditation.name')</a>
                    </li>
                    <li>
                        <a href="{{route('travel')}}">@lang('pages.travel.name')</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header__right">

            <div class="header-options">
                <a href="#" class="header-option header-option--geo open-modal" data-modal="modal-geo">Москва</a>
                <a class="header-option header-option--search" href="#!">@lang('pages.search')</a>
                @if($locale == 'ru')
                    <a class="header-option header-option--lang"
                       href="{{\App\Helper::changeLang('en')}}">@lang('pages.lang')</a>
                @else
                    <a class="header-option header-option--lang"
                       href="{{\App\Helper::changeLang('ru')}}">@lang('pages.lang')</a>
                @endif
            </div>

            <a class="menu-btn-mobile">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </a>
        </div>
    </div>
</header>


