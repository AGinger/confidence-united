<header class="header orange-forever">
    <div class="header__inner">
        <div class="header__left">
            <a class="logo" href="/"></a>
        </div>
        <div class="header__mid">
            <nav>
                <ul class="header__nav">
                    <li>
                        <a href="{{route('about')}}">@lang('pages.about.name')</a>
                    </li>
                    <li>
                        <a href="{{route('about')}}">@lang('pages.services')</a>
                    </li>
                    <li>
                        <a href="{{route('about')}}">@lang('pages.customers')</a>
                    </li>
                    <li>
                        <a href="{{route('inform-agency')}}">@lang('pages.agency)</a>
                    </li>
                    <li>
                        <a href="{{route('about')}}">@lang('pages.contacts')</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header__right">

            <div class="header-options">
                <a class="header-option header-option--geo" href="#!">Москва</a>
                <a class="header-option header-option--search" href="#!">Search</a>
                <a class="header-option header-option--lang" href="#!">Ru</a>
            </div>

            <a class="menu-btn-mobile">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </a>
        </div>
    </div>
</header>


