<!DOCTYPE html>
<html lang="{{ $locale }}">
<head>
    @include('layouts.head')
    <title>@lang('pages.agency.name')</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/inform-agency.css') }}">
</head>
<body>
<header class="header">
    <div class="header__inner">
        <div class="header__left">
            <a class="logo" href="/"></a>
        </div>
        <div class="header__mid">
            <nav>
                <ul class="header__nav">
                    <li>
                        <a href="{{ route('inform-agency.news', ['locale' => $locale]) }}#news">@lang('pages.news')</a>
                    </li>
                    <li>
                        <a href="{{ route('inform-agency.publications', ['locale' => $locale]) }}#publication">@lang('pages.publications')</a>
                    </li>
                    <li>
                        <a href="{{ route('inform-agency.events', ['locale' => $locale]) }}#event">@lang('pages.events')</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header__right">

            <div class="header-options">
                <a href="#" class="header-option header-option--geo open-modal" data-modal="modal-geo">Москва</a>
                <a href="#" class="header-option header-option--search">@lang('pages.search')</a>
                @if($locale == 'ru')
                    <a class="header-option header-option--lang"
                       href="{{\App\Helper::changeLang('en')}}">@lang('pages.lang')</a>
                @else
                    <a class="header-option header-option--lang"
                       href="{{\App\Helper::changeLang('ru')}}">@lang('pages.lang')</a>
                @endif
            </div>

            <a class="menu-btn-mobile">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </a>
        </div>
    </div>
</header>

<section class="content">
    <section class="hero-search">
        <picture>
            <source media="(min-width: 1440px)" srcset="{{ asset('images/1440/inform-agency-inner-1440.webp') }}">
            <source media="(min-width: 1280px)" srcset="{{ asset('images/1280/inform-agency-inner-1280.webp') }}">
            <source media="(min-width: 768px)" srcset="{{ asset('images/768/inform-agency-inner-768.webp') }}">
            <source media="(min-width: 564px)" srcset="{{ asset('images/564/inform-agency-inner-564.webp') }}">
            <source media="(min-width: 0px)" srcset="{{ asset('images/400/inform-agency-inner-400.webp') }}">
            <img class="image-fallback" src="{{ asset('images/768/inform-agency-inner-768.jpg') }}"
                 srcset="{{ asset('images/1440/inform-agency-inner-1440.jpg') }} 2x" alt="hero-search">
        </picture>
        <div class="hero-search__inner">
            <div class="hero-search__title title">
                <h1>
                    @lang('pages.agency.name')
                </h1>
            </div>
            <form class="search-field">
                <input type="text" class="search-field__input" placeholder="@lang('pages.agency.search_form.interested')">
                <button class="search-field__reset" type="reset"></button>
                <button class="search-field__submit title" type="submit">@lang('pages.agency.search_form.search')</button>
            </form>
        </div>
    </section>
    @yield('content')
</section>

<div class="bottom-nav" style="bottom: 0px;">
    <a href="#" class="bell"></a>
    <a href="#" class="next"></a>
</div>

@php
    if ($locale == 'ru') {
        $lang = \App\Models\Page::LANGUAGE_RU;
    } else {
        $lang = \App\Models\Page::LANGUAGE_ENG;
    }
       $footer_info = \App\Models\MainPage::where('language', $lang)->first();
@endphp

<footer class="footer-sub">
    <div class="footer-sub__inner">
        <picture>
            <source media="(min-width: 1440px)"
                    srcset="{{ asset('images/1440/subscripbe-1440.webp, images/1440/subscripbe-1440.jpg') }}">
            <source media="(min-width: 1280px)"
                    srcset="{{ asset('images/1280/subscripbe-1280.webp') }}, {{ asset('images/1280/subscripbe-1280.jpg') }}">
            <source media="(min-width: 768px)"
                    srcset="{{ asset('images/768/subscripbe-768.webp, images/768/subscripbe-768.jpg') }}">
            <source media="(min-width: 564px)"
                    srcset="{{ asset('images/564/subscripbe-564.webp, images/564/subscripbe-564.jpg') }}">
            <source media="(min-width: 0px)"
                    srcset="{{ asset('images/400/subscripbe-400.webp') }}, {{ asset('images/400/subscripbe-400.jpg') }}">
            <img class="image-fallback" src="{{ asset('images/768/subscripbe-768.jpg') }}" alt="footer-bg">
        </picture>
        <div class="footer-sub__title title">
            <h2>@lang('pages.agency.footer.subscribe_for_information')</h2>
        </div>
        <div class="footer-sub__desc">
            <p class="red">@lang('pages.agency.footer.actual_events')</p>
        </div>
        <form action="" class="footer-sub__form">
            <div class="footer-sub__row">
                <input class="footer-sub__input" type="text" placeholder="@lang('pages.agency.footer.name')">
                <input class="footer-sub__input" type="email" placeholder="E-MAIL">
            </div>
            <button class="footer-sub__submit btn">@lang('pages.agency.footer.subscribe')</button>
        </form>
        <div class="soc">
            <a href="{{ $metaOptions['twitter_url_meta']->value }}" class="soc__link soc__link--tw"></a>
            <a href="{{ $metaOptions['facebook_url_meta']->value }}" class="soc__link soc__link--fb"></a>
            <a href="{{ $metaOptions['linkedin_url_meta']->value }}" class="soc__link soc__link--in"></a>
            <a href="{{ $metaOptions['vkontakte_url_meta']->value }}" class="soc__link soc__link--vk"></a>
        </div>
        <p class="footer-sub__extra">
            @lang('pages.agency.footer.information')
        </p>

    </div>
    <div class="footer__bot">
        @lang('pages.footer.rights')
    </div>
</footer>

<!-- MODALS -->
<div class="popup-form" id="modal-geo">
    <div class="inner">
        <div class="close close-modal" data-modal="modal-geo"></div>
        <div class="title big">@lang('pages.current_region')<br><span class="red">Moscow</span></div>
        <!--
        <div class="btn"><a href="#">Change region</a></div>
         -->
    </div>
</div>
<!-- /MODALS -->
<script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('libs/slick.min.js') }}"></script>
<script src="{{ asset('libs/simplaLightbox/simpleLightbox.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/scroll-anchors.js') }}"></script>
<script src="{{ asset('js/animation.js') }}"></script>
<script src="{{ asset('js/sticky.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
      rel="stylesheet">
</body>
</html>
