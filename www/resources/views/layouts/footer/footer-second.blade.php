@php
    if ($locale == 'ru') {
        $lang = \App\Models\Page::LANGUAGE_RU;
    } else {
        $lang = \App\Models\Page::LANGUAGE_ENG;
    }
       $footer_info = \App\Models\MainPage::where('language', $lang)->first();
@endphp

<footer class="footer-sub">
    <div class="footer-sub__inner">
        <picture>
            <source media="(min-width: 1440px)"
                    srcset="{{ asset('images/1440/subscripbe-1440.webp, images/1440/subscripbe-1440.jpg') }}">
            <source media="(min-width: 1280px)"
                    srcset="{{ asset('images/1280/subscripbe-1280.webp') }}, {{ asset('images/1280/subscripbe-1280.jpg') }}">
            <source media="(min-width: 768px)"
                    srcset="{{ asset('images/768/subscripbe-768.webp, images/768/subscripbe-768.jpg') }}">
            <source media="(min-width: 564px)"
                    srcset="{{ asset('images/564/subscripbe-564.webp, images/564/subscripbe-564.jpg') }}">
            <source media="(min-width: 0px)"
                    srcset="{{ asset('images/400/subscripbe-400.webp') }}, {{ asset('images/400/subscripbe-400.jpg') }}">
            <img class="image-fallback" src="{{ asset('images/768/subscripbe-768.jpg') }}" alt="footer-bg">
        </picture>
        <div class="footer-sub__title title">
            <h2>@lang('pages.agency.footer.subscribe_for_information')</h2>
        </div>
        <div class="footer-sub__desc">
            <p class="red">@lang('pages.agency.footer.actual_events')</p>
        </div>
        <form action="" class="footer-sub__form">
            <div class="footer-sub__row">
                <input class="footer-sub__input" type="text" placeholder="@lang('pages.agency.footer.name')">
                <input class="footer-sub__input" type="email" placeholder="E-MAIL">
            </div>
            <button class="footer-sub__submit btn">@lang('pages.agency.footer.subscribe')</button>
        </form>
        <div class="soc">
            <a href="{{ $metaOptions['twitter_url_meta']->value }}" class="soc__link soc__link--tw"></a>
            <a href="{{ $metaOptions['facebook_url_meta']->value }}" class="soc__link soc__link--fb"></a>
            <a href="{{ $metaOptions['linkedin_url_meta']->value }}" class="soc__link soc__link--in"></a>
            <a href="{{ $metaOptions['vkontakte_url_meta']->value }}" class="soc__link soc__link--vk"></a>
        </div>
        <p class="footer-sub__extra">
            @lang('pages.agency.footer.information')
        </p>

    </div>
    <div class="footer__bot">
        @lang('pages.footer.rights')
    </div>
</footer>

<script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('libs/slick.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
