<footer class="footer">
    @php
        if ($locale == 'ru') {
            $lang = \App\Models\Page::LANGUAGE_RU;
        } else {
            $lang = \App\Models\Page::LANGUAGE_ENG;
        }
           $footer_info = \App\Models\MainPage::where('language', $lang)->first();
    @endphp
    <div class="footer__inner">
        <div class="soc">
            <a href="{{ $footer_info->social_networks }}"
               class="soc__link soc__link--tw"></a>
            <a href="{{ $footer_info->social_networks_second }}"
               class="soc__link soc__link--fb"></a>
            <a href="{{ $footer_info->social_networks_third }}"
               class="soc__link soc__link--in"></a>
            <a href="{{ $footer_info->social_networks_fourth }}"
               class="soc__link soc__link--vk"></a>
        </div>
        <div class="footer__contacts">
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.contacts')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_contacts }}
                </p>
            </div>
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.office')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_office }}

                </p>
            </div>
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.hours')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_business_hours }}
                </p>
            </div>
        </div>
    </div>
    <div class="footer__bot">
        @lang('pages.footer.rights')
    </div>
</footer>

<script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('libs/slick.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
