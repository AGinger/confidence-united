@php
    if ($locale == 'ru') {
        $lang = \App\Models\Page::LANGUAGE_RU;
    } else {
        $lang = \App\Models\Page::LANGUAGE_ENG;
    }
       $footer_info = \App\Models\MainPage::where('language', $lang)->first();
@endphp

<footer class="footer footer--with-bg">
    <div class="footer__bg">
        <picture>
            <source media="(min-width: 1440px)"
                    srcset="{{ asset('images/1440/subscripbe-1440.webp, images/1440/subscripbe-1440.jpg') }}">
            <source media="(min-width: 1280px)"
                    srcset="{{ asset('images/1280/subscripbe-1280.webp') }}, {{ asset('images/1280/subscripbe-1280.jpg') }}">
            <source media="(min-width: 768px)"
                    srcset="{{ asset('images/768/subscripbe-768.webp, images/768/subscripbe-768.jpg') }}">
            <source media="(min-width: 564px)"
                    srcset="{{ asset('images/564/subscripbe-564.webp, images/564/subscripbe-564.jpg') }}">
            <source media="(min-width: 0px)"
                    srcset="{{ asset('images/400/subscripbe-400.webp') }}, {{ asset('images/400/subscripbe-400.jpg') }}">
            <img class="image-fallback" src="{{ asset('images/768/subscripbe-768.jpg') }}" alt="footer-bg">
        </picture>
    </div>
    <div class="footer__inner">
        <div class="soc">
            <a href="{{ $footer_info->social_networks }}"
               class="soc__link soc__link--tw"></a>
            <a href="{{ $footer_info->social_networks_second }}"
               class="soc__link soc__link--fb"></a>
            <a href="{{ $footer_info->social_networks_third }}"
               class="soc__link soc__link--in"></a>
            <a href="{{ $footer_info->social_networks_fourth }}"
               class="soc__link soc__link--vk"></a>
        </div>
        <div class="footer__contacts">
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.contacts')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_contacts }}
                </p>
            </div>
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.office')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_office }}

                </p>
            </div>
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.hours')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_business_hours }}
                </p>
            </div>
        </div>
    </div>
    <div class="footer__bot">
        @lang('pages.footer.rights')
    </div>
</footer>

<script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('libs/slick.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
