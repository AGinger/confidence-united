<div class="bottom-nav" style="bottom: 0px;">
    <a href="#" class="bell"></a>
    <a href="#" class="next"></a>
</div>

@php
    if ($locale == 'ru') {
        $lang = \App\Models\Page::LANGUAGE_RU;
    } else {
        $lang = \App\Models\Page::LANGUAGE_ENG;
    }
       $footer_info = \App\Models\MainPage::where('language', $lang)->first();
@endphp

<footer class="footer">
    <div class="footer__inner">
        <div class="soc" data-slide="slideTo" id="contacts">
            <a href="{{ $metaOptions['twitter_url_meta']->value }}" class="soc__link soc__link--tw"></a>
            <a href="{{ $metaOptions['facebook_url_meta']->value }}" class="soc__link soc__link--fb"></a>
            <a href="{{ $metaOptions['linkedin_url_meta']->value }}" class="soc__link soc__link--in"></a>
            <a href="{{ $metaOptions['vkontakte_url_meta']->value }}" class="soc__link soc__link--vk"></a>
        </div>
        <div class="footer__contacts">
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.contacts')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_contacts }}
                </p>
            </div>
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.office')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_office }}

                </p>
            </div>
            <div class="footer__col">
                <h3 class="footer__title title">@lang('pages.footer.hours')</h3>
                <p class="footer__info">
                    {{ $footer_info->footer_business_hours }}
                </p>
            </div>
        </div>
    </div>
    <div class="footer__bot">
        @lang('pages.footer.rights')
    </div>
</footer>

<!-- MODALS -->
<div class="popup-form" id="modal-geo">
    <div class="inner">
        <div class="close close-modal" data-modal="modal-geo"></div>
        <div class="title big">@lang('pages.current_region')<br><span class="red">Moscow</span></div>
        <!--
        <div class="btn"><a href="#">Change region</a></div>
         -->
    </div>
</div>
<!-- /MODALS -->

<!-- You need an exact match with the front repo templates -->
<script src="{{ asset('libs/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('libs/slick.min.js') }}"></script>
<script src="{{ asset('libs/simplaLightbox/simpleLightbox.min.js') }}"></script>
<script src="{{ asset('js/scrollTo.min.js') }}"></script> <!-- Scrolling script -->
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/animation.js') }}"></script>
<script src="{{ asset('js/sticky.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>

<!-- Connecting fonts -->
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
      rel="stylesheet">
