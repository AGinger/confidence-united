<div class="popup-form" id="modal-review">
    <div class="inner">
        <div class="close-review close-modal" data-modal="modal-review"></div>
        <form action="{{ route('review.create') }}" method="POST">
            @csrf
            <div class="title_big title-review">
                <label for="review">Имя:</label>
            </div>
            <input class="modal-review" name="author" type="text"/>
            <input name="language" value="0" type="text" style="display: none"/>
            <div class="title_big title-review">
                <label for="review">Отзыв:</label>
            </div>
            <textarea class="modal-review" id="review" name="description" rows="3" cols="40"></textarea>
            <button type="submit" class="btn btn-review">Оставить отзыв</button>
        </form>
    </div>
</div>
