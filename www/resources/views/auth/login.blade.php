<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <base href="../"/>
    <title>Auth</title>
    <link rel="stylesheet" href="{{ asset('libs/semantic/dist/semantic.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          type="text/css"/>
    <link rel="stylesheet" href="{{ asset('libs/text-editor/richtext.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}" type="text/css"/>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <meta name="release" content="GUI - 0.5UI"/>
</head>
<body>
<section class="auth">
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <div class="auth__logo"><img src="{{ asset('images/logo-main.svg') }}" alt="logo"/></div>
            <form method="POST" class="ui large form" action="{{ route('login') }}">
                @csrf
                <div class="ui stacked segment">
                    <h2 class="ui header">Панель управления</h2>
                    <div class="field">
                        <input type="text" type="email" name="email" placeholder="Почта"/>
                    </div>
                    <div class="field">
                        <input type="password" name="password" placeholder="Пароль"/>
                        <button class="ui blue button" type="submit">Войти</button>
                    </div>
                </div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>
    <div class="page_version">version GUI - 0.5UI</div>
</section>
<script src="{{ asset('libs/semantic/dist/semantic.min.js') }}"></script>
<script src="{{ asset('libs/text-editor/jquery.richtext.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
