<?php

return array(
    'lang' => 'Ru',
    'services' => 'SERVICES',
    'customers' => 'CUSTOMERS',
    'contacts' => 'CONTACTS',
    'more' => 'READ MORE',
    'search' => 'SEARCH',
    'news' => 'NEWS',
    'publications' => 'PUBLICATIONS',
    'events' => 'EVENTS',
    'close' => 'Close',
    'all' => 'ALL ',
    'offers' => 'Special offers and promotions',
    'package_services' => 'Package services',
    'separate_services' => 'Separate services',
    'choose' => 'CHOOSE',
    'chosen' => 'SELECTED',
    'interaction' => 'INTERACTION ORDER',
    'client' => 'Client',
    'consultant' => 'Consultant',
    'select' => 'SELECT',
    'share' => 'SHARE',
    'author' => 'Author',
    'photo' => 'Photo',
    'print' => 'Print version',
    'current_region' => 'Your current region: ',
    'footer' =>
        array(
            'rights' => '© 2007-2020 Confidence Group. All rights reserved',
            'contacts' => 'Contacts',
            'office' => 'Moscow office',
            'hours' => 'Working hours',
        ),
    'main' =>
        array(
            'name' => 'Confidence Group',
            'hi' => 'Hello, we are',
            'confidence' => 'Confidence Group',
            'company' => 'Relocation management company',
            'services' => 'Services',
            'services_text' => 'SINCE 2007 EXPERTS IN IMMIGRATION, RELOCATION, VISA CONSULTING
             AS WELL AS IN REGISTRATION/ACCREDITATION OF BUSINESS IN RUSSIA AND INDIVIDUAL TRAVEL',
            'services_center_text' => 'STEP INTO OUR WORLD AND YOU\'LL NEVER STEP OUT',
            'customers' => 'Customers',
            'customers_text' => 'SINCE 2007, CONFIDENCE GROUP HAS BEEN WORKING WITH
            EVERYONE FROM START-UPS TO GLOBAL BRANDS.',
            'brands_rus' => 'Russian brands',
            'brands_int' => 'International brands',
            'agency_text' => 'EVERY MONDAY, WEDNESDAY AND FRIDAY READ FRESH NEWS AND PROFESSIONAL
            ARTICLES. REGISTER FOR ACTUAL EVENTS',
        ),
    'migration' =>
        array(
            'name' => 'Migration consulting',
            'inner' =>
                array(
                    'term' => 'Term of registration',
                    'days' => 'working days',
                    'price' => 'Price',
                    'duty' => 'Government duty',
                    'rubles' => 'rubles',
                    'quantity' => 'Quantity',
                    'order' => 'Order'
                ),
            'accompanying_services' => 'Accompanying services'
        ),
    'about' =>
        array(
            'name' => 'About us',
            'we' => 'We are',
            'presentation_button' => 'DOWNLOAD PRESENTATION',
            'reviews' => 'Reviews',
            'feedback' => 'Give feedback',
            'team' => 'Our team',
            'certificates' => 'Certificates',
        ),
    'visa' =>
        array(
            'name' => 'Visa support',
        ),
    'relocation' =>
        array(
            'name' => 'Relocation services',
            'object' => 'OBJECT TYPE',
            'rooms' => 'ROOMS',
            'budget' => 'BUDGET',
            'only_photo' => 'WITH PHOTO ONLY'
        ),
    'accreditation' =>
        array(
            'name' => 'Registration and accreditation',
        ),
    'travel' =>
        array(
            'name' => 'Travel and recreation',
            'hotels' => 'FIND HOTELS',
            'tours' => 'Tours',
        ),
    'service_search_form' => array(
        'purpose_options' => array(
            'all' => 'All',
            'business_visit' => 'Business visit',
            'work' => 'Work',
            'tourism' => 'Tourism'
        ),
    ),
    'agency' => array(
        'name' => 'Information agency',
        'search_form' => array(
            'search' => 'SEARCH',
            'interested' => 'I\'m interested:',
        ),
        'footer' =>
            array(
                'subscribe_for_information' => 'Subscribe to information newsletter',
                'subscribe' => 'Subscribe',
                'actual_events' => 'Every monday, wednesday, and friday read fresh news and personal articles.
                Register for actual events',
                'name' => 'Name',
                'information' => '«Confidence Group Information Agency (+0) was registered with Roskomnadzor on September 21, 2016. room
             mass media certificates EL No. FS77-67213. Founder - Confidence Group LLC Chief Editor - AV Sprinchan
             Editorial office contacts: 105082, Moscow, Spartakovskaya sq., 14, building 2 (entrance from the side of Baumanskaya street), 3rd floor,
             office 2306, Bristol mansion, CENTRAL STREET business center, 8 495 748 7762, 8 800 222 7762,
             info@confidencegroup.ru',
            ),
    ),
);
