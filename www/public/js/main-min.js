$(document).ready(function() {

    console.log('yo');

    $('.menu-btn-mobile').click(function() {
        $('.menu-btn-mobile').toggleClass('active');
        $('.header__nav').toggleClass('active');
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $('.header').addClass("sticky");
        } else {
            $('.header').removeClass("sticky");
        }
    });


    (function() {
        $('.counter__plus').click(function() {
            $(this).prev().get(0).value++;
        })
        $('.counter__minus').click(function() {
            $(this).next().get(0).value--;
            if ($(this).next().get(0).value < 0) {
                $(this).next().val(0)
            }
        })
    })();

    $('.imageGallery a').simpleLightbox();

    $('.customers-slider').slick({
        arrows: false,
        dots: true
    });

    $('.articles-slider').slick({
        // arrows: false,
        dots: true
    });

    $('.services-slider__slider').slick({
        slidesToShow: 3,
        arrows: true,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('.testimonials__slider').slick({
        dots: true
    });

    $('.team__slider').slick({});


    $('.serts-slider').slick({
        dots: true
    });

    // responsive video on index page
    var mainVideo = $('#hero-video');

    if ($(window).width() > 1440) {
        mainVideo.append('<source src="images/video/confidence-1920.mp4" type="video/mp4" >');
    } else if ($(window).width() > 640) {
        mainVideo.append('<source src="images/video/confidence-640.mp4" type="video/mp4" >');
    } else {
        mainVideo.append('<source src="images/video/confidence-320.mp4" type="video/mp4" >');

    }
    mainVideo.mediaelementplayer();

});