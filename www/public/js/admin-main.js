// A $( document ).ready() block.
$(document).ready(function () {
    console.log("ready!");
    $('.menu .item').tab();
});

$('#AddClient').click(function () {
    $('.client_row').prepend(' <div class="ui input ui_client"><input type="text" name="client[]"/></div>');
});

$('#RemoveClient').click(function () {
   $('div.ui_client').last().remove();
});

$('#AddConsult').click(function () {
    $('.consult_row').prepend(' <div class="ui input ui_consult"><input type="text" name="client[]"/></div>');
});

$('#RemoveConsult').click(function () {
    $('div.ui_consult').last().remove();
});

$('#standard_calendar').calendar();

// textEditor init
$('.textEditor').each(function () {
    $(this).richText({
        bold: true,
        italic: true,
        underline: true,
        leftAlign: false,
        centerAlign: false,
        rightAlign: false,
        justify: false,
        ol: true,
        ul: true,
        heading: true,
        fonts: false,
        fontList: ["Arial",
          "Arial Black",
          "Comic Sans MS",
          "Courier New",
          "Geneva",
          "Georgia",
          "Helvetica",
          "Impact",
          "Lucida Console",
          "Tahoma",
          "Times New Roman",
          "Verdana"
        ],
        fontColor: false,
        fontSize: false,
        imageUpload: false,
        fileUpload: false,
        videoEmbed: false,
        urls: true,
        table: true,
        removeStyles: false,
        code: false,
        colors: [],
        fileHTML: '',
        imageHTML: '',
        translations: {
          'title': 'Title',
          'white': 'White',
          'black': 'Black',
          'brown': 'Brown',
          'beige': 'Beige',
          'darkBlue': 'Dark Blue',
          'blue': 'Blue',
          'lightBlue': 'Light Blue',
          'darkRed': 'Dark Red',
          'red': 'Red',
          'darkGreen': 'Dark Green',
          'green': 'Green',
          'purple': 'Purple',
          'darkTurquois': 'Dark Turquois',
          'turquois': 'Turquois',
          'darkOrange': 'Dark Orange',
          'orange': 'Orange',
          'yellow': 'Yellow',
          'imageURL': 'Image URL',
          'fileURL': 'File URL',
          'linkText': 'Link text',
          'url': 'URL',
          'size': 'Size',
          'responsive': '<a href="https://www.jqueryscript.net/tags.php?/Responsive/">Responsive</a>',
          'text': 'Text',
          'openIn': 'Open in',
          'sameTab': 'Same tab',
          'newTab': 'New tab',
          'align': 'Align',
          'left': 'Left',
          'justify': 'Justify',
          'center': 'Center',
          'right': 'Right',
          'rows': 'Rows',
          'columns': 'Columns',
          'add': 'Add',
          'pleaseEnterURL': 'Please enter an URL',
          'videoURLnotSupported': 'Video URL not supported',
          'pleaseSelectImage': 'Please select an image',
          'pleaseSelectFile': 'Please select a file',
          'bold': 'Bold',
          'italic': 'Italic',
          'underline': 'Underline',
          'alignLeft': 'Align left',
          'alignCenter': 'Align centered',
          'alignRight': 'Align right',
          'addOrderedList': 'Add ordered list',
          'addUnorderedList': 'Add unordered list',
          'addHeading': 'Add Heading/title',
          'addFont': 'Add font',
          'addFontColor': 'Add font color',
          'addFontSize': 'Add font size',
          'addImage': 'Add image',
          'addVideo': 'Add video',
          'addFile': 'Add file',
          'addURL': 'Add URL',
          'addTable': 'Add table',
          'removeStyles': 'Remove styles',
          'code': 'Show HTML code',
          'undo': 'Undo',
          'redo': 'Redo',
          'close': 'Close'
        },
        youtubeCookies: false,
        useSingleQuotes: false,
        height: 0,
        heightPercentage: 0,
        id: "",
        class: "",
        useParagraph: false,
        maxlength: 0,
    });
});

// custom file upload
$("input:text").click(function () {
    $(this).parent().find("input:file").click();
});
$(".fileUpload").click(function () {
    $(this).parent().find("input:file").click();
});

$('input:file', '.ui.action.input')
    .on('change', function (e) {
        var name = e.target.files[0].name;
        $('input:text', $(e.target).parent()).val(name);
    });

function chooseSlide(evt, slide_ru, slide_eng) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(slide_ru).style.display = "block";
    document.getElementById(slide_eng).style.display = "block";
    evt.currentTarget.className += " active";
}

// textEditor all setting
// $('.content').richText({
//     // text formatting
//     bold: true,
//     italic: true,
//     underline: true,

//     // text alignment
//     leftAlign: true,
//     centerAlign: true,
//     rightAlign: true,
//     justify: true,

//     // lists
//     ol: true,
//     ul: true,

//     // title
//     heading: true,

//     // fonts
//     fonts: true,
//     fontList: ["Arial",
//       "Arial Black",
//       "Comic Sans MS",
//       "Courier New",
//       "Geneva",
//       "Georgia",
//       "Helvetica",
//       "Impact",
//       "Lucida Console",
//       "Tahoma",
//       "Times New Roman",
//       "Verdana"
//     ],
//     fontColor: true,
//     fontSize: true,

//     // uploads
//     imageUpload: true,
//     fileUpload: true,

//     // // media
//     // <a href="https://www.jqueryscript.net/tags.php?/video/">video</a>Embed: true,

//     // link
//     urls: true,

//     // tables
//     table: true,

//     // code
//     removeStyles: true,
//     code: true,

//     // colors
//     colors: [],

//     // dropdowns
//     fileHTML: '',
//     imageHTML: '',

//     // translations
//     translations: {
//       'title': 'Title',
//       'white': 'White',
//       'black': 'Black',
//       'brown': 'Brown',
//       'beige': 'Beige',
//       'darkBlue': 'Dark Blue',
//       'blue': 'Blue',
//       'lightBlue': 'Light Blue',
//       'darkRed': 'Dark Red',
//       'red': 'Red',
//       'darkGreen': 'Dark Green',
//       'green': 'Green',
//       'purple': 'Purple',
//       'darkTurquois': 'Dark Turquois',
//       'turquois': 'Turquois',
//       'darkOrange': 'Dark Orange',
//       'orange': 'Orange',
//       'yellow': 'Yellow',
//       'imageURL': 'Image URL',
//       'fileURL': 'File URL',
//       'linkText': 'Link text',
//       'url': 'URL',
//       'size': 'Size',
//       'responsive': '<a href="https://www.jqueryscript.net/tags.php?/Responsive/">Responsive</a>',
//       'text': 'Text',
//       'openIn': 'Open in',
//       'sameTab': 'Same tab',
//       'newTab': 'New tab',
//       'align': 'Align',
//       'left': 'Left',
//       'justify': 'Justify',
//       'center': 'Center',
//       'right': 'Right',
//       'rows': 'Rows',
//       'columns': 'Columns',
//       'add': 'Add',
//       'pleaseEnterURL': 'Please enter an URL',
//       'videoURLnotSupported': 'Video URL not supported',
//       'pleaseSelectImage': 'Please select an image',
//       'pleaseSelectFile': 'Please select a file',
//       'bold': 'Bold',
//       'italic': 'Italic',
//       'underline': 'Underline',
//       'alignLeft': 'Align left',
//       'alignCenter': 'Align centered',
//       'alignRight': 'Align right',
//       'addOrderedList': 'Add ordered list',
//       'addUnorderedList': 'Add unordered list',
//       'addHeading': 'Add Heading/title',
//       'addFont': 'Add font',
//       'addFontColor': 'Add font color',
//       'addFontSize': 'Add font size',
//       'addImage': 'Add image',
//       'addVideo': 'Add video',
//       'addFile': 'Add file',
//       'addURL': 'Add URL',
//       'addTable': 'Add table',
//       'removeStyles': 'Remove styles',
//       'code': 'Show HTML code',
//       'undo': 'Undo',
//       'redo': 'Redo',
//       'close': 'Close'
//     },

//     // privacy
//     youtubeCookies: false,

//     // dev settings
//     useSingleQuotes: false,
//     height: 0,
//     heightPercentage: 0,
//     id: "",
//     class: "",
//     useParagraph: false,
//     maxlength: 0,

//     // callback function after init
//     callback: undefined
//   });
