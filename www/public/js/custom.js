// Модальные окна
	
$('.open-modal, .close-modal').click(function(e) {

    e.preventDefault();

    var modalId = $(this).data('modal');

    if ($('#' + modalId).css('display') == 'none') {
        
		$('#' + modalId).css('display', 'flex');
		
    } else {
        
		$('#' + modalId).css('display', 'none');
		
    }

});