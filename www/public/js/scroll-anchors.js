const anchors = document.querySelectorAll('a[href*="#"]');
for (let anchor of anchors) {
    let blockID = anchor.getAttribute('href').substr(1);
    if(anchor.getAttribute('href')[0] == "#"){
        anchor.addEventListener('click', function (e) {
            e.preventDefault()
            document.getElementById(blockID).scrollIntoView({
                    behavior: 'smooth',
                    block: 'start'
                  })
          
          })
    }
 
}