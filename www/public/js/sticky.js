// $(document).ready(function () {
//     $(window).scroll(function () {
//         if ($(this).scrollTop() > 1) {
//             $('.header').addClass("sticky");
//         } else {
//             $('.header').removeClass("sticky");
//         }
//     });
// });

// header fixed
const debounce = (fn) => {
    let frame;
    return (...params) => {

        if (frame) {
            cancelAnimationFrame(frame);
        }

        frame = requestAnimationFrame(() => {
            fn(...params);
        });
    }
};

const storeScroll = () => {
    document.documentElement.dataset.scroll = window.scrollY;
}

document.addEventListener('scroll', debounce(storeScroll), { passive: true });

storeScroll();