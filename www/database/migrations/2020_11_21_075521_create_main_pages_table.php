<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_pages', function (Blueprint $table) {
            $table->id();
            $table->text('services');
            $table->text('clients');
            $table->text('information_agency');
            $table->string('social_networks');
            $table->string('social_networks_second')->nullable();
            $table->string('social_networks_third')->nullable();
            $table->string('social_networks_fourth')->nullable();
            $table->text('footer_contacts');
            $table->text('footer_office');
            $table->text('footer_business_hours');
            $table->tinyInteger('language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_pages');
    }
}
