<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inform_agencies', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->integer('number');
            $table->text('name');
            $table->text('description');
            $table->string('title_meta');
            $table->string('keywords_meta');
            $table->text('description_meta');
            $table->string('background_image')->nullable();
            $table->tinyInteger('language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inform_agencies');
    }
}
