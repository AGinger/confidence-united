<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('page');
            $table->string('category');
            $table->integer('number');
            $table->text('name');
            $table->text('announcement');
            $table->text('description');
            $table->string('title_meta');
            $table->string('keywords_meta');
            $table->text('description_meta');
            $table->string('background_image')->nullable();
            $table->tinyInteger('language');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
