<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAboutPage();
        $this->createMainPage();
        $this->createSRVMPage();
        $this->createVisaSupportPage();
        $this->createRelocationPage();
        $this->createInformAgency();
        $this->createRegistryAccreditation();
        $this->createTravelRelax();
    }

    protected function createAboutPage()
    {
        $category = Page::PAGE_ABOUT;
        $name_ru = 'Конфиденс Групп';
        $description_ru = 'С 2007 года Конфиденс Групп является экспертом в области миграционного консалтинга, визовой
         поддержки для выезжающих зарубеж, подбора жилья и релокационных услуг, а также организации индивидуальных путешествий,
          а также регистрации и аккредитации бизнеса.';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Confidence group';
        $description_eng = 'Since 2007, Confidence Group has been an expert in the field of migration consulting, visa
         support for people traveling abroad, housing and relocation services, as well as individual travel arrangements,
          as well as business registration and accreditation.';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng);
    }

    protected function createMainPage()
    {
        $category = Page::PAGE_MAIN;
        $name_ru = 'Конфиденс Групп';
        $description_ru = 'С 2007 года Конфиденс Групп является экспертом в области миграционного консалтинга, визовой
         поддержки для выезжающих зарубеж, подбора жилья и релокационных услуг, а также организации индивидуальных путешествий,
          а также регистрации и аккредитации бизнеса.';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Confidence group';
        $description_eng = 'Since 2007, Confidence Group has been an expert in the field of migration consulting, visa
         support for people traveling abroad, housing and relocation services, as well as individual travel arrangements,
          as well as business registration and accreditation.';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng);
    }

    protected function createSRVMPage()
    {
        $category = Page::PAGE_SRVM;
        $background_image = 'images/img-srvm-1.png';
        $name_ru = 'Миграционный консалтинг';
        $description_ru = 'Комплексное сопровождение по всем вопросам, связанным с въездом, пребываением и осуществлением
         трудовой деятельности иностранных граждан на всей территории РФ';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Migration consulting';
        $description_eng = 'Comprehensive support on all issues related to entry, stay and implementation
          labor activity of foreign citizens throughout the Russian Federation';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru, $background_image);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng, $background_image);
    }

    protected function createVisaSupportPage()
    {
        $category = PAGE::PAGE_VISA_SUPPORT;
        $background_image = 'images/768/viza-768.jpg';
        $name_ru = 'Визовая поддержка';
        $description_ru = 'Консультационные Услуги по оформлению  деловых,
         туристических и частных виз для Российских и иностранных граждан,
         выезжающих за пределы РФ';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Visa support';
        $description_eng = 'CONSULTING SERVICES ON OUTBOUND BUSINESS,
         TOURISTIC, PRIVATE VISAS FOR RUSSIAN AND FOREIGN CITIZENS';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru, $background_image);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng, $background_image);
    }

    protected function createRelocationPage()
    {
        $category = PAGE::PAGE_RELOCATION;
        $background_image = 'images/768/relokaciya-768.webp';
        $name_ru = 'Релокационные услуги';
        $description_ru = 'Полный спектр услуг по подбору недвижимости в аренду, переезду, перевозки вещей,
         а также обустройству и адаптации иностранных и российских граждан на новом месте';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Relocation services';
        $description_eng = 'FULL SCOPE OF RELOCATION SERVICES, INCLUDING HOME  SEARCH, MOVING,
        TRANSPORTATION SERVICES AND ADAPTATION TOURS FOR FOREIGNERS COMING TO RUSSIA';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru, $background_image);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng, $background_image);
    }

    protected function createInformAgency()
    {
        $category = PAGE::PAGE_INFORM_AGENCY;
        $background_image = 'images/768/inform-agency-inner-768.jpg';
        $name_ru = 'Информационное агенство';
        $description_ru = 'Комплексное сопровождение по всем вопросам, связанным с въездом, пребываением и осуществлением трудовой
         деятельности иностранных граждан на всей территории РФ';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Information agency';
        $description_eng = 'Comprehensive support on all issues related to the entry, stay and employment of foreign citizens
         throughout the Russian Federation';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru, $background_image);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng, $background_image);
    }

    protected function createRegistryAccreditation()
    {
        $category = PAGE::PAGE_REGISTRY_ACCREDITATION;
        $background_image = 'images/768/registraciya-768.jpg';
        $name_ru = 'Регистрация и аккредитация';
        $description_ru = 'Консультационные услуги по аккредитации представительств и
        филиалов иностранных юридических лиц на территории РФ, а также регистрации Российских
        юридических лиц';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Registration and accreditation';
        $description_eng = 'CONSULTING SERVICES ON ACCREDITATION OF
         REP OFFICES AND BRANCHES ON THE TERRITORY OF RUSSIA, REGISTRATION OF RUSSIAN LEGAL ENTITIES';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru, $background_image);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng, $background_image);
    }

    protected function createTravelRelax()
    {
        $category = PAGE::PAGE_TRAVEL_RELAXATION;
        $background_image = 'images/768/otdyh-768.jpg';
        $name_ru = 'Путешествия и отдых';
        $description_ru = 'Индивидуальный подход в организации
        незабываемых путешествий, деловых и интенсивных поездок в
        России и за ее пределами';
        $keywords_meta_ru = 'ключ 1, ключ 2, ключ 3';
        $language_ru = Page::LANGUAGE_RU;

        $name_eng = 'Travel and recreation';
        $description_eng = 'INDIVIDUAL APPROACH TO THE ORGANIZATION OF
        LEASURE, BUSINESS AND INTENSIVE TRIPS IN RUSSIA AND ABROAD';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createPage($category, $name_ru, $description_ru, $keywords_meta_ru, $language_ru, $background_image);
        $this->createPage($category, $name_eng, $description_eng, $keywords_meta_eng, $language_eng, $background_image);
    }

    /**
     * @param $category
     * @param $name
     * @param $description
     * @param $keywords
     * @param $language
     * @param $background_image
     * @return Page
     */
    protected function createPage($category, $name, $description, $keywords, $language, $background_image = null)
    {
        $page = new Page();
        $page->category = $category;
        $page->name = $name;
        $page->description = $description;
        $page->title_meta = $name;
        $page->keywords_meta = $keywords;
        $page->description_meta = $description;
        $page->language = $language;
        $page->background_image = $background_image;
        $page->save();

        return $page;
    }
}
