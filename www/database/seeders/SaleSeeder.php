<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Sale;
use Illuminate\Database\Seeder;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = '12.07.2020';
        $name_ru = 'Скидка для членов Американской Торговой Палаты в РФ';
        $description_ru = 'Для членов Американской Торговой Палаты в РФ, Финско-Российской Торговой Палаты в РФ,
         Торгово-Промышленной Палаты РФ и других бизнес-сообществ РФ скидка на миграционные и визовые услуги составляет 30%
          от стандартной стоимости услуг Конфиденс Групп. Условия акции: не распространяются на действующие договоры с Конфиденс
           Групп; распространяются на услуги Конфиденс Групп в рамках договоров, заключенных в период с 01.06.2017 по ';

        $name_eng = 'Discount for members of American Chamber of Commerce in the Russian Federation';
        $description_eng = 'For members of the American Chamber of Commerce in the Russian Federation, the Finnish-Russian Chamber
         of Commerce in the Russian Federation, the Chamber of Commerce and Industry of the Russian Federation and other business
         communities of the Russian Federation, a discount on migration and visa services is 30% of the standard cost of Confidence
          Group services. Promotion terms: do not apply to existing agreements with Confidence Group; apply to the services of the
          Confidence Group ';

        $this->createSale(Page::PAGE_SRVM, 9098, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_SRVM, 9098, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);
        $this->createSale(Page::PAGE_SRVM, 9097, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_SRVM, 9097, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);

        $this->createSale(Page::PAGE_VISA_SUPPORT, 8098, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_VISA_SUPPORT, 8098, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);
        $this->createSale(Page::PAGE_VISA_SUPPORT, 8097, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_VISA_SUPPORT, 8097, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);

        $this->createSale(Page::PAGE_RELOCATION, 7098, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_RELOCATION, 7098, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);
        $this->createSale(Page::PAGE_RELOCATION, 7097, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_RELOCATION, 7097,  $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);

        $this->createSale(Page::PAGE_REGISTRY_ACCREDITATION, 6098, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_REGISTRY_ACCREDITATION, 6098, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);
        $this->createSale(Page::PAGE_REGISTRY_ACCREDITATION, 6097, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_REGISTRY_ACCREDITATION, 6097, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);

        $this->createSale(Page::PAGE_TRAVEL_RELAXATION, 5098, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_TRAVEL_RELAXATION, 5098, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);
        $this->createSale(Page::PAGE_TRAVEL_RELAXATION, 5097, $name_ru, $description_ru,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createSale(Page::PAGE_TRAVEL_RELAXATION, 5097, $name_eng, $description_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);

    }

    protected function createSale(
        $category,
        $number,
        $name,
        $description,
        $background_image,
        $language
    ) {
        $sale = new Sale();
        $sale->category = $category;
        $sale->number = $number;
        $sale->name = $name;
        $sale->description = $description;
        $sale->background_image = $background_image;
        $sale->language = $language;
        $sale->save();

        return $sale;
    }
}
