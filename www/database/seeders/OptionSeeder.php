<?php

namespace Database\Seeders;

use App\Models\Option;
use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOption(
            'Vkontakte  URL',
            'vkontakte_url_meta',
            'https://vk.com/confidence_group',
            'Ссылка на страницу в Vkontakte',
            'meta'
        );
        $this->createOption(
            'LinkedIn  URL',
            'linkedin_url_meta',
            'https://www.linkedin.com/confidence.group/ ',
            'Ссылка на страницу в LinkedIn',
            'meta'
        );
        $this->createOption(
            'Twitter URL',
            'twitter_url_meta',
            'https://twitter.com/confidence.group',
            'Ссылка на страницу в Twitter',
            'meta'
        );
        $this->createOption(
            'Twitter Account',
            'twitter_account_meta',
            '@Confidence.Group',
            'Название аккаунта в Twitter, пример: @Confidence.Group',
            'meta'
        );
        $this->createOption(
            'Facebook URL',
            'facebook_url_meta',
            'https://www.facebook.com/confidence.group',
            'Ссылка на страницу в Facebook',
            'meta'
        );
        $this->createOption(
            'Instagram URL',
            'instagram_url_meta',
            'https://www.instagram.com/confidence.group',
            'Ссылка на страницу Instagram',
            'meta'
        );
        $this->createOption(
            'Телефон',
            'phone_number_meta',
            '88002227762',
            'Номер телефона',
            'meta'
        );
        $this->createOption(
            'Email тех. поддержки',
            'email_technical_support_meta',
            'info@confidencegroup.ru',
            'Email технической поддержки',
            'meta'
        );
    }

    protected function createOption(
        $title,
        $key,
        $value,
        $description,
        $type
    ) {
        $option = new Option();
        $option->title = $title;
        $option->key = $key;
        $option->value = $value;
        $option->description = $description;
        $option->type = $type;
        $option->save();

        return $option;
    }
}
