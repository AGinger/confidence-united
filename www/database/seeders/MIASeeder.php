<?php

namespace Database\Seeders;

use App\Models\MIA;
use App\Models\Page;
use Illuminate\Database\Seeder;

class MIASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = 'Разрешение на работу сопровождающего члена семьи
         высококвалифицированного специалиста (ВКС)';
        $announcement = 'Оформляется членам семьи высококвалифицированных
         специалистов в упрощенном порядке на срок не превышающий срок действия
          разрешения на работу основных высококвалифицированных специалистов';
        $description = 'Оформляется членам семьи высококвалифицированных
         специалистов в упрощенном порядке на срок не превышающий срок действия
         разрешения на работу основных высококвалифицированных специалистов,
          Оформляется членам семьи высококвалифицированных специалистов в упрощенном
           порядке на срок не превышающий срок действия разрешения на работу основных
            высококвалифицированных специалистов.';
        $title_meta = 'Миграционный консалтинг';
        $keywords_meta = 'ключ 1, ключ 2, ключ 3';
        $description_meta = 'Комплексное сопровождение по всем вопросам, связанным с
        въездом, пребываением и осуществлением трудовой деятельности иностранных граждан
         на всей территории РФ';

        $this->createMIA('vks', 124412124, $name, $announcement, $description, $title_meta, $keywords_meta, $description_meta,
            Page::LANGUAGE_ENG);
        $this->createMIA('vks', 124412124, $name, $announcement, $description, $title_meta, $keywords_meta, $description_meta,
            Page::LANGUAGE_RU);

        $this->createMIA('strany', 124412123, $name, $announcement, $description, $title_meta, $keywords_meta, $description_meta,
            Page::LANGUAGE_ENG);
        $this->createMIA('strany', 124412123, $name, $announcement, $description, $title_meta, $keywords_meta, $description_meta,
            Page::LANGUAGE_RU);

        $this->createMIA('strany', 124412125, $name, $announcement, $description, $title_meta, $keywords_meta, $description_meta,
            Page::LANGUAGE_ENG);
        $this->createMIA('strany', 124412125, $name, $announcement, $description, $title_meta, $keywords_meta, $description_meta,
            Page::LANGUAGE_RU);
    }

    protected function createMIA(
        $category,
        $number,
        $name,
        $announcement,
        $description,
        $title_meta,
        $keywords_meta,
        $description_meta,
        $lang
    ) {
        $mia = new MIA();
        $mia->category = $category;
        $mia->number = $number;
        $mia->name = $name;
        $mia->announcement = $announcement;
        $mia->description = $description;
        $mia->title_meta = $title_meta;
        $mia->keywords_meta = $keywords_meta;
        $mia->description_meta = $description_meta;
        $mia->language = $lang;
        $mia->save();

        return $mia;
    }
}
