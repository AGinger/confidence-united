<?php

namespace Database\Seeders;

use App\Models\AboutMeta;
use App\Models\Page;
use Illuminate\Database\Seeder;

class AboutMetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $presentation = 'presentations/СG_presentation_main_V001_RUS.pdf';
        $announce_ru = 'Мы, компания CONFIDENCE GROUP в целях обеспечения поддержки
         наших Клиентов на пути к достижению их бизнес-задач, торжественно обещаем
         работать в полном соответствии с законом и международными стандартами качества,
         постоянно совершенствовать бизнес-процессы. Гарантируем проактивный подход,
         оперативность в предоставлении услуг и информации, несем полную ответственность перед Клиентами';
        $video_ru = 'https://www.youtube.com/embed/BzknewK_UaE?ecver=2';

        $announce_eng = 'We are CONFIDENCE GROUP IN ORDER TO SUPPORT OUR CLIENTS ON THE WAY TO ACHIEVE THEIR BUSINESS
         OBJETIVES, DECLARE TO WORK IN FULL COMPLIENCE WITH LAW AND INTERNATIONAL QUALITY STANDARDS, CONSTANTLY IMPROVE
          BUSINESS PROCESSES. HEREIN WE GUARANTEE THE PROACTIVE APPROACH, PROMPT ATTENTION WHILE PROVIDING SERVICES AND
          INFORMATION. WE BARE THE FULL RESPONSIBILITY TO OUR CLIENTS';
        $video_eng = $video_ru;

        $this->createMeta($announce_ru, $video_ru, $presentation, Page::LANGUAGE_RU);
        $this->createMeta($announce_eng, $video_eng, $presentation, Page::LANGUAGE_ENG);
    }

    protected function createMeta($announce, $video, $presentation, $language): AboutMeta
    {
        $meta = new AboutMeta();
        $meta->announce = $announce;
        $meta->video_link = $video;
        $meta->presentation = $presentation;
        $meta->language = $language;
        $meta->save();

        return $meta;
    }
}
