<?php

namespace Database\Seeders;

use App\Models\InformAgency;
use App\Models\MainClients;
use App\Models\MainPage;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
        $this->call(MainClientsSeeder::class);
        $this->call(MainPageSeeder::class);
        $this->call(AboutCertificateSeeder::class);
        $this->call(AboutReviewsSeeder::class);
        $this->call(AboutTeamSeeder::class);
        $this->call(AboutMetaSeeder::class);
        $this->call(InteractionSeeder::class);
        $this->call(MIASeeder::class);
        $this->call(PageSeeder::class);
        $this->call(SaleSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ServiceTypeSeeder::class);
        $this->call(ServiceVariationSeeder::class);
        $this->call(VisaSupSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(InformAgencySeeder::class);
        $this->call(OptionSeeder::class);
    }
}
