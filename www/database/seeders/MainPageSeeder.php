<?php

namespace Database\Seeders;

use App\Models\MainPage;
use App\Models\Page;
use Illuminate\Database\Seeder;

class MainPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $social_first = 'https://twitter.com/intent/tweet?url=http%3A%2F%2Fold.366033-cb87966.tmweb.ru%2F%3Flng%3Dchange%26amp%3Blng%3Dchange&text=Confidence+Group';
        $social_second = 'https://www.facebook.com/confidencegr';
        $social_third = 'https://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fold.366033-cb87966.tmweb.ru%2F%3Flng%3Dchange%26amp%3Blng%3Dchange&title=Confidence+Group&summary=&source=Confidence+Group';
        $social_fourth = 'https://vk.com/confidence_group';
        $services_ru = 'С 2007 года Конфиденс Групп является экспертом в области миграционного консалтинга,
         визовой поддержки для выезжающих зарубеж, подбора жилья и релокационных услуг, а также организации
         индивидуальных путешествий, а также регистрации и аккредитации бизнеса.';
        $footer_contacts_ru = 'Тел.: +7 495 748-77-62; 8 800 222-77-62 (бесплатные звонки в России); Факс:
        +7 495 748-77-62; info@confidencegroup.ru';
        $footer_office_ru = '105082, Спартаковская площадь, д. 14, стр. 2(вход со стороны ул. Бауманская), Особняк
         Bristol, бизнес-центр Central Street,3 этаж, офис 2306';
        $footer_business_hours_ru = 'Понедельник — пятница С 9:00 до 18:00';
        $language_ru = Page::LANGUAGE_RU;

        $services_eng = 'Since 2007, Confidence Group has been an expert in the field of migration consulting,
         visa support for people traveling abroad, housing and relocation services, as well as individual travel
         arrangements, as well as business registration and accreditation.';
        $footer_contacts_eng = 'Tel./fax: +7 495 748-77-62; 8 800 222-77-62 (toll free calls in Russia); info@confidencegroup.ru';
        $footer_office_eng = '105082, Spartakovskaya Square, 14, bld. 2(entrance from Baumanskaya Street), Bristol Mansion, Central
        Street Business Center,3rd floor, office 2306';
        $footer_business_hours_eng = 'Monday - Friday From 9 a.m. to 6 p.m.';
        $language_eng = Page::LANGUAGE_ENG;

        $this->createMainPage($social_first, $social_second, $social_third, $social_fourth, $services_ru,
            $footer_contacts_ru, $footer_office_ru, $footer_business_hours_ru, $language_ru);
        $this->createMainPage($social_first, $social_second, $social_third, $social_fourth, $services_eng,
            $footer_contacts_eng, $footer_office_eng, $footer_business_hours_eng, $language_eng);
    }

    private function createMainPage(
        $social_first,
        $social_second,
        $social_third,
        $social_fourth,
        $services,
        $footer_contacts,
        $footer_office,
        $footer_business_hours,
        $language
    ) {
        $main = new MainPage();
        $main->social_networks = $social_first;
        $main->social_networks_second = $social_second;
        $main->social_networks_third = $social_third;
        $main->social_networks_fourth = $social_fourth;
        $main->services = $services;
        $main->clients = $services;
        $main->information_agency = $services;
        $main->footer_contacts = $footer_contacts;
        $main->footer_office = $footer_office;
        $main->footer_business_hours = $footer_business_hours;
        $main->language = $language;
        $main->save();

        return $main;
    }
}
