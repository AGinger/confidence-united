<?php

namespace Database\Seeders;

use App\Models\MainClients;
use Illuminate\Database\Seeder;

class MainClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i <= 2; $i++)
        {
            $this->createMainClients(1, MainClients::TYPE_RUSSIAN,'Саунды', 'images/img-main.png');
            $this->createMainClients(1, MainClients::TYPE_RUSSIAN,'Nestle', 'images/nestle.png');
            $this->createMainClients(2, MainClients::TYPE_RUSSIAN,'Саунды', 'images/img-main.png');
            $this->createMainClients(2, MainClients::TYPE_RUSSIAN,'Nestle', 'images/nestle.png');

            $this->createMainClients(1, MainClients::TYPE_INTERNATIONAL,'Nestle', 'images/nestle.png');
            $this->createMainClients(1, MainClients::TYPE_INTERNATIONAL,'Саунды', 'images/img-main.png');
            $this->createMainClients(2, MainClients::TYPE_INTERNATIONAL,'Nestle', 'images/nestle.png');
            $this->createMainClients(2, MainClients::TYPE_INTERNATIONAL,'Саунды', 'images/img-main.png');

        }
    }

    protected function createMainClients($slide, $type, $name, $image)
    {
        $client = new MainClients();
        $client->slide = $slide;
        $client->type = $type;
        $client->name = $name;
        $client->image = $image;
        $client->save();

        return $client;
    }
}
