<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\ServiceType;
use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createServiceType(Page::PAGE_SRVM, 'vks', 'высококфалиф. специалисты', 'highly qualified specialists');
        $this->createServiceType(Page::PAGE_SRVM, 'vig-op', 'Визовые иностранные граждане, общий порядок',
            'Visa foreign citizens, general procedure');
        $this->createServiceType(Page::PAGE_SRVM, 'bvig-op', 'Безвизовые иностранные граждане, общий порядок',
            'Visa-free foreign citizens, general procedure');
        $this->createServiceType(Page::PAGE_SRVM, 'akkreditovannye-sotrudniki', 'Аккредитованные сотрудники',
            'Accredited employees');
        $this->createServiceType(Page::PAGE_SRVM, 'v-ramkah-soglashenij-s-franciej-i-yuzhnoj-koreej',
            'В рамках соглашений с Францией и Южной Кореей',
            'Under agreements with France and South Korea');
        $this->createServiceType(Page::PAGE_SRVM, 'razreshenie-na-vremennoe-prozhivanie',
            'Разрешение на временное проживание',
            'Temporary residence permit');
        $this->createServiceType(Page::PAGE_SRVM, 'vid-na-zhitelstvo', 'Вид на жительство', 'Residence');
        $this->createServiceType(Page::PAGE_SRVM, 'prochie', 'Прочие', 'Others');

        $this->createServiceType(Page::PAGE_RELOCATION, 'paketnye-uslugi', 'Пакетные услуги', 'Package services');
        $this->createServiceType(Page::PAGE_RELOCATION, 'otdelnye-servisy', 'Отдельные сервисы', 'Individual services');

        $this->createServiceType(Page::PAGE_REGISTRY_ACCREDITATION, 'akkreditaciya-predstavitelstvi-filialov-inostrannyh-kompanij',
            'Аккредитация представительств и филиалов иностранных компаний',
            'Accreditation of representative offices and branches of foreign companies');
        $this->createServiceType(Page::PAGE_REGISTRY_ACCREDITATION, 'registraciya-rossijskih-kompanij',
            'Регистрация российских компаний',
            'Registration of Russian companies');
        $this->createServiceType(Page::PAGE_REGISTRY_ACCREDITATION, 'prochie-uslugi', 'Прочие услуги',
            'Other services');

        $this->createServiceType(Page::PAGE_TRAVEL_RELAXATION, 'turi', 'Туры',
        'Tours');
        $this->createServiceType(Page::PAGE_TRAVEL_RELAXATION, 'otdelnye-servisy', 'Отдельные сервисы',
        'Individual services');
    }

    protected function createServiceType($page, $category, $name_ru, $name_eng)
    {
        $type = new ServiceType();
        $type->page = $page;
        $type->category = $category;
        $type->name_ru = $name_ru;
        $type->name_eng = $name_eng;
        $type->save();

        return $type;
    }
}
