<?php

namespace Database\Seeders;

use App\Models\AboutTeam;
use Illuminate\Database\Seeder;

class AboutTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createTeam('София Батура', 'Batura Sofiya', 'images/persons/11.jpg', 'images/persons/1.jpg');
        $this->createTeam('Иван Иванов', 'Ivan Ivanov', 'images/persons/3.jpg', 'images/persons/2.jpg');
        $this->createTeam('Сергей Сергеев', 'Sergey Sergeev', 'images/persons/4.jpg', 'images/persons/5.jpg');
    }

    protected function createTeam($name_ru, $name_eng, $image_first, $image_second)
    {
        $team = new AboutTeam();
        $team->name_ru = $name_ru;
        $team->name_eng = $name_eng;
        $team->image_first = $image_first;
        $team->image_second = $image_second;
        $team->save();

        return $team;
    }
}
