<?php

namespace Database\Seeders;

use App\Models\AboutReviews;
use App\Models\Page;
use Illuminate\Database\Seeder;

class AboutReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name_1 = 'Amec Foster Wheeler';
        $desc_1 = 'I would like to express
        my gratitude to your company for doing
        such a necessary and useful thing, and
         for supporting other organizations in
         the field of migration free of charge,
          as well as for the high professionalism
          of specialists, providing concrete answers
           to complex questions and well-chosen interesting
           material.';

        $name_2 = 'Кузнецова Л.В. Вице-президент Sun Capital
         Partners Consulting Limited';
        $desc_2 = 'The representative office of SAN Capital
         Partners thanks the employees of Confidence Group Service
          Company LLC for the assistance provided to our foreign employee
           in obtaining a residence permit. Our corporate manager, Olga
            Makarova, showed a high level of professionalism, advising us
             on all issues of interest, talking about all the nuances of
              the case, offering alternatives, organizing a meeting with
               the head of the department at the State Budgetary Institution
                IMC, as well as receiving a document from a foreign citizen.
                 In connection with the current situation in the State Budgetary
                  Institution of MMC, the issuance of a new residence permit to
                  our employee was delayed, and the validity of the current
                  document was drawing to a close. Olga Makarova was able to quickly
                  get involved in the work, she personally monitored on-site that the
                   meeting of our employee with the leader took place, provided
                   advisory and moral support. We also thank Sergey Melekhin for
                   accompanying our employee at the Migration Center. We look forward
                    to further fruitful cooperation.';

        $name_3 = 'Olga BARABLINA Administrative Specialist, Эйрбас';
        $desc_3 = 'Good afternoon! Thank you very much for the information
         provided. We also thank for Migration Bulletin No. 6, yesterday we
         received a magazine. You are great fellows!';

        $name_4 = 'Ольга Валентиновна Кучеренко Директор по персоналу';
        $desc_4 = 'Maria, good afternoon! Allow me to express my gratitude
         on behalf of KMEW to all those involved in the process of extending the
         Accreditation of the indicated Representative Office, as well as to you
          personally for your involvement in the process important to the Company
           and proper supervision of you. THANK!!!';

        $name_5 = 'Объединение современных технологий и инноваций в хирургии';
        $desc_5 = '“Combining modern technologies and innovations in surgery”:
         Dear Alina Eduardovna! We express our sincere gratitude to you and your
          team for the professional assistance in the implementation of our medical
           projects. I would like to note the high professionalism of Svetlana Ryseva.
           Thanks to her responsibility and clear organization, we managed to carry out
           a number of hasty educational trips for domestic doctors! We are deeply grateful
           to you for the fruitful cooperation in 2016.';

        $name_6 = 'Стройтрансгаз';
        $desc_6 = 'The information is presented in an accessible language, in detail, with many
        examples. The published journal is useful, as well as mailing with innovations in the
        legislative migration world. Carrying out these activities is useful and necessary. Thank.';


        $this->createReview($name_1, $desc_1, Page::LANGUAGE_RU);
        $this->createReview($name_2, $desc_2, Page::LANGUAGE_RU);
        $this->createReview($name_3, $desc_3, Page::LANGUAGE_RU);
        $this->createReview($name_4, $desc_4, Page::LANGUAGE_ENG);
        $this->createReview($name_5, $desc_5, Page::LANGUAGE_ENG);
        $this->createReview($name_6, $desc_6, Page::LANGUAGE_ENG);
    }

    protected function createReview($name, $description, $language)
    {
        $review = new AboutReviews();
        $review->author = $name;
        $review->description = $description;
        $review->language = $language;
        $review->save();

        return $review;
    }
}
