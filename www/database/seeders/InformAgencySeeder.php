<?php

namespace Database\Seeders;

use App\Models\InformAgency;
use App\Models\Page;
use Illuminate\Database\Seeder;

class InformAgencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $event_name_1 = 'ВЫСОКОКФАЛИФИЦИРОВАННЫЕ СПЕЦИАЛИСТЫ В РФ 1';
        $event_name_1_eng = 'HIGHLY QUALIFIED SPECIALISTS IN THE RUSSIAN FEDERATION 1';
        $event_name_2 = 'ВЫСОКОКФАЛИФИЦИРОВАННЫЕ СПЕЦИАЛИСТЫ В РФ 2';
        $event_name_2_eng = 'HIGHLY QUALIFIED SPECIALISTS IN THE RUSSIAN FEDERATION 2';

        $event_desc_1 = 'Конфиденс Групп рада сообщить вам о продолжении серии мероприятий по актуальным миграционным вопросам в 2017 году';
        $event_desc_1_eng = 'Confidence Group is pleased to inform you about the continuation of a series of events on topical migration issues in 2017';
        $event_desc_2 = 'Конфиденс Групп рада сообщить вам о продолжении серии мероприятий по актуальным миграционным вопросам в 2017 году';

        $event_image_1 = 'images/inform-agency-uploads/ia-item-3-860.jpg';
        $event_image_2 = 'images/inform-agency-uploads/ia-item-2-763.jpg';

        $news_desc_1 = 'Конфиденс Групп рада сообщить вам о продолжении серии мероприятий по актуальным миграционным вопросам в 2017 году';
        $news_desc_1_eng = 'Confidence Group is pleased to inform you about the continuation of a series of events on topical migration issues in 2017';
        $news_desc_2 = 'Конфиденс Групп рада сообщить вам о продолжении серии мероприятий по актуальным миграционным вопросам в 2017 году';

        $news_name_1 = 'ВЫСОКОКФАЛИФИЦИРОВАННЫЕ СПЕЦИАЛИСТЫ В РФ';
        $news_name_2 = 'Нарушения миграционного законодательства. Минимизация рисков при проверках организаций компетентными органами';
        $news_name_2_eng = 'Violations of migration legislation. Minimizing risks when auditing organizations by competent authorities';

        $news_image_1 = 'images/inform-agency-uploads/ia-item-4-625.jpg';
        $news_image_2 = 'images/inform-agency-uploads/ia-item-5-1301.jpg';

        $pub_name_1 = '16 января 2019 г. вступит в силу Федеральный закон от 19.07.2018 N 215-ФЗ';
        $pub_name_1_eng = 'On January 16, 2019 the Federal Law of 19.07.2018 N 215-FZ will come into force';
        $pub_name_2 = 'Белоруссия ввела безвизовый режим для 80 стран';
        $pub_name_2_eng = 'Belarus introduced visa-free regime for 80 countries';

        $pub_desc_1 = '9 января 2017 г. Президент Белоруссии Александр Лукашенко подписал указ об
       установлении безвизового порядка въезда и выезда на территорию республики для граждан 80 иностранных государств';
        $pub_desc_1_eng = 'On January 9, 2017, President of Belarus Alexander Lukashenko signed a decree on
        establishment of a visa-free procedure for entry and exit to the territory of the republic for citizens of 80 foreign states';
        $pub_desc_2 = 'В мае 2017 г. стало известно, что документы на оформление квоты о привлечении иностранных работников
         в Московской области принимаются до 30 мая 2017 г.';
        $pub_desc_2_eng = 'In May 2017, it became known that the documents for issuing a quota on attracting foreign workers
          in the Moscow region are accepted until May 30, 2017';

        $pub_image_1 = 'images/services-upload/service-item-1-1000.jpg';
        $pub_image_2 = 'images/services-upload/service-item-2-860.jpg';

        $keywords = 'ключ1, ключ2, ключ3';
        $keywords_eng = 'key1, key2, key3';

        for ($i = 0; $i <= 5; $i++) {
            $this->createAgency(InformAgency::TYPE_EVENTS, 1242, $event_name_1, $event_desc_1, $keywords,
                $event_image_1,
                Page::LANGUAGE_RU);
            $this->createAgency(InformAgency::TYPE_EVENTS, 1242, $event_name_1_eng, $event_desc_1_eng, $keywords_eng,
                $event_image_1,
                Page::LANGUAGE_ENG);
            $this->createAgency(InformAgency::TYPE_EVENTS, 1242, $event_name_2, $event_desc_2, $keywords,
                $event_image_2,
                Page::LANGUAGE_RU);
            $this->createAgency(InformAgency::TYPE_EVENTS, 1242, $event_name_2_eng, $event_desc_1_eng, $keywords_eng,
                $event_image_2,
                Page::LANGUAGE_ENG);

            $this->createAgency(InformAgency::TYPE_NEWS, 1144, $news_name_1, $news_desc_1, $keywords, $news_image_1,
                Page::LANGUAGE_RU);
            $this->createAgency(InformAgency::TYPE_NEWS, 1144, $event_name_1_eng, $news_desc_1_eng, $keywords_eng, $news_image_1,
                Page::LANGUAGE_ENG);
            $this->createAgency(InformAgency::TYPE_NEWS, 1144, $news_name_2, $news_desc_2, $keywords, $news_image_2,
                Page::LANGUAGE_RU);
            $this->createAgency(InformAgency::TYPE_NEWS, 1144, $news_name_2_eng, $news_desc_1_eng, $keywords_eng, $news_image_2,
                Page::LANGUAGE_ENG);

            $this->createAgency(InformAgency::TYPE_PUBLICATIONS, 1322, $pub_name_1, $pub_desc_1, $keywords,
                $pub_image_1,
                Page::LANGUAGE_RU);
            $this->createAgency(InformAgency::TYPE_PUBLICATIONS, 1322, $pub_name_1_eng, $pub_desc_1_eng, $keywords_eng,
                $pub_image_1, Page::LANGUAGE_ENG);
            $this->createAgency(InformAgency::TYPE_PUBLICATIONS, 1322, $pub_name_2, $pub_desc_2, $keywords,
                $pub_image_2,
                Page::LANGUAGE_RU);
            $this->createAgency(InformAgency::TYPE_PUBLICATIONS, 1322, $pub_name_2_eng, $pub_desc_2_eng, $keywords_eng,
                $pub_image_2, Page::LANGUAGE_ENG);
        }
    }

    protected function createAgency($type, $number, $name, $description, $keywords_meta, $image, $lang)
    {
        $agency = new InformAgency();
        $agency->type = $type;
        $agency->number = $number;
        $agency->name = $name;
        $agency->description = $description;
        $agency->title_meta = $name;
        $agency->keywords_meta = $keywords_meta;
        $agency->description_meta = $description;
        $agency->background_image = $image;
        $agency->language = $lang;
        $agency->save();

        return $agency;
    }
}
