<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Services;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = 'Разрешение на работу сопровождающего члена семьи
         высококвалифицированного специалиста (ВКС)';
        $name_eng = 'Work permit of an accompanying family member
          highly qualified specialist (HQS)';
        $announcement = 'Оформляется членам семьи высококвалифицированных
         специалистов в упрощенном порядке на срок не превышающий срок действия
          разрешения на работу основных высококвалифицированных специалистов';
        $announcement_eng = 'Issued to family members of highly qualified
          specialists in a simplified manner for a period not exceeding the validity period
           work permits for the main highly qualified specialists';
        $description = 'Оформляется членам семьи высококвалифицированных
         специалистов в упрощенном порядке на срок не превышающий срок действия
         разрешения на работу основных высококвалифицированных специалистов,
          Оформляется членам семьи высококвалифицированных специалистов в упрощенном
           порядке на срок не превышающий срок действия разрешения на работу основных
            высококвалифицированных специалистов.';
        $description_eng = 'Issued to family members of highly qualified
          specialists in a simplified manner for a period not exceeding the validity period
          work permits for the main highly qualified specialists,
           It is issued to family members of highly qualified specialists in a simplified
            order for a period not exceeding the period of validity of the work permit of the main
             highly qualified specialists.';
        $title_meta = 'Миграционный консалтинг';
        $title_meta_eng = 'Migration consulting';
        $keywords_meta = 'ключ 1, ключ 2, ключ 3';
        $keywords_meta_eng = 'key 1, key 2, key 3';
        $description_meta = 'Комплексное сопровождение по всем вопросам, связанным с
        въездом, пребываением и осуществлением трудовой деятельности иностранных граждан
         на всей территории РФ';
        $description_meta_eng = 'Comprehensive support on all issues related to
         entry, stay and employment of foreign citizens
          throughout the Russian Federation';

        $this->createService('migracionnyj-konsalting', 'vks', 27056, $name, $announcement, $description, $title_meta,
            $keywords_meta,
            $description_meta,
            'images/img-srvm-1.png', Page::LANGUAGE_RU);
        $this->createService('migracionnyj-konsalting', 'vks', 27056, $name_eng, $announcement_eng, $description_eng, $title_meta_eng,
            $keywords_meta_eng,
            $description_meta_eng,
            'images/img-srvm-1.png', Page::LANGUAGE_ENG);

        $this->createService('migracionnyj-konsalting', 'vig-op', 27014, $name, $announcement, $description, $title_meta,
            $keywords_meta,
            $description_meta,
            'images/img-srvm-2.png', Page::LANGUAGE_RU);
        $this->createService('migracionnyj-konsalting', 'vig-op', 27014, $name_eng, $announcement_eng, $description_eng, $title_meta_eng,
            $keywords_meta_eng,
            $description_meta_eng,
            'images/img-srvm-2.png', Page::LANGUAGE_ENG);

        $this->createService('migracionnyj-konsalting', 'bvig-op', 27036, $name, $announcement, $description, $title_meta,
            $keywords_meta,
            $description_meta,
            'images/migracionnyi-consalting-inner.jpg', Page::LANGUAGE_RU);
        $this->createService('migracionnyj-konsalting', 'bvig-op', 27036, $name_eng, $announcement_eng, $description_eng, $title_meta_eng,
            $keywords_meta_eng,
            $description_meta_eng,
            'images/migracionnyi-consalting-inner.jpg', Page::LANGUAGE_ENG);

        $this->createService('relokacionnye-uslugi', 'paketnye-uslugi', 27016, $name, $announcement, $description,
            $title_meta, $keywords_meta,
            $description_meta,
            'images/img-srvm-1.png', Page::LANGUAGE_RU);
        $this->createService('relokacionnye-uslugi', 'paketnye-uslugi', 27016, $name_eng, $announcement_eng, $description_eng,
            $title_meta_eng, $keywords_meta_eng,
            $description_meta_eng,
            'images/img-srvm-1.png', Page::LANGUAGE_ENG);

        $this->createService('relokacionnye-uslugi', 'otdelnye-servisy', 27017, $name, $announcement, $description,
            $title_meta, $keywords_meta,
            $description_meta,
            'images/img-srvm-1.png', Page::LANGUAGE_RU);
        $this->createService('relokacionnye-uslugi', 'otdelnye-servisy', 27017, $name_eng, $announcement_eng, $description_eng,
            $title_meta_eng, $keywords_meta_eng,
            $description_meta_eng,
            'images/img-srvm-1.png', Page::LANGUAGE_ENG);

        $this->createService('registraciya-i-akkreditaciya', 'registraciya-rossijskih-kompanij', 27052, $name,
            $announcement, $description, $title_meta, $keywords_meta,
            $description_meta,
            'images/img-srvm-1.png', Page::LANGUAGE_RU);
        $this->createService('registraciya-i-akkreditaciya', 'registraciya-rossijskih-kompanij', 27052, $name_eng,
            $announcement_eng, $description_eng, $title_meta_eng, $keywords_meta_eng,
            $description_meta_eng,
            'images/img-srvm-1.png', Page::LANGUAGE_ENG);

        $this->createService('puteshestviya-i-otdyh', 'otdelnye-servisy', 27082, $name, $announcement,
            $description, $title_meta, $keywords_meta,
            $description_meta,
            'images/img-srvm-1.png', Page::LANGUAGE_RU);

        $this->createService('puteshestviya-i-otdyh', 'otdelnye-servisy', 27082, $name_eng, $announcement_eng,
            $description_eng, $title_meta_eng, $keywords_meta_eng,
            $description_meta_eng,
            'images/img-srvm-1.png', Page::LANGUAGE_ENG);

        $this->createService(Page::PAGE_VISA_SUPPORT, 'strany', 27099, $name, $announcement, $description, $title_meta,
            $keywords_meta,
            $description_meta,
            'images/inform-agency-uploads/ia-item-4-625.jpg', Page::LANGUAGE_RU);
        $this->createService(Page::PAGE_VISA_SUPPORT, 'strany', 27099, $name_eng, $announcement_eng, $description_eng, $title_meta_eng,
            $keywords_meta_eng,
            $description_meta_eng,
            'images/inform-agency-uploads/ia-item-4-625.jpg', Page::LANGUAGE_ENG);

        $this->createService(Page::PAGE_VISA_SUPPORT, 'strany', 27095, $name, $announcement, $description, $title_meta,
            $keywords_meta,
            $description_meta,
            'images/inform-agency-uploads/ia-item-5-1301.jpg', Page::LANGUAGE_RU);
        $this->createService(Page::PAGE_VISA_SUPPORT, 'strany', 27095, $name_eng, $announcement_eng, $description_eng, $title_meta_eng,
            $keywords_meta_eng,
            $description_meta_eng,
            'images/inform-agency-uploads/ia-item-5-1301.jpg', Page::LANGUAGE_ENG);
    }

    protected function createService(
        $page,
        $category,
        $number,
        $name,
        $announcement,
        $description,
        $title_meta,
        $keywords_meta,
        $description_meta,
        $image,
        $lang
    ) {
        $service = new Services();
        $service->page = $page;
        $service->category = $category;
        $service->number = $number;
        $service->name = $name;
        $service->announcement = $announcement;
        $service->description = $description;
        $service->title_meta = $title_meta;
        $service->keywords_meta = $keywords_meta;
        $service->description_meta = $description_meta;
        $service->background_image = $image;
        $service->language = $lang;
        $service->save();
    }
}
