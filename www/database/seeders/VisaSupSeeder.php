<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\VisaSupportCountries;
use Illuminate\Database\Seeder;

class VisaSupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCountry('Russia', Page::LANGUAGE_ENG);
        $this->createCountry('Россия', Page::LANGUAGE_RU);
    }

    protected function createCountry($name, $lang)
    {
        $country = new VisaSupportCountries();
        $country->name = $name;
        $country->language = $lang;
        $country->save();

        return $country;
    }
}
