<?php

namespace Database\Seeders;

use App\Models\ServiceVariations;
use Illuminate\Database\Seeder;

class ServiceVariationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createServiceVariation(27056,5, 500, 500);
        $this->createServiceVariation(27056, 10, 1500, 2500);
        $this->createServiceVariation(27052,15, 2500, 3500);
    }

    /*
     * @param $process_time
     * @param $duty
     * @param $price
     */
    protected function createServiceVariation($service_number, $process_time, $duty, $price)
    {
        $var = new ServiceVariations();
        $var->service_number = $service_number;
        $var->process_time = $process_time;
        $var->government_duty = $duty;
        $var->price = $price;
        $var->save();

        return $var;
    }
}
