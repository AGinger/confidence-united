<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Countries;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCountry('Россия', 'Russia', 27099, Countries::PURPOSE_TOURISM);
        $this->createCountry('Южная Корея', 'South Korea', 27095, Countries::PURPOSE_WORK);
    }

    protected function createCountry($name_ru, $name_eng, $service_number, $purpose)
    {
        $country = new Countries();
        $country->name_ru = $name_ru;
        $country->name_eng = $name_eng;
        $country->service_number = $service_number;
        $country->purpose = $purpose;
        $country->save();

        return $country;
    }
}
