<?php

namespace Database\Seeders;

use App\Models\Interactions;
use App\Models\Page;
use Illuminate\Database\Seeder;

class InteractionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // Слайдер схемы взаимодействия
    public function run()
    {
        $title_ru = 'подготовка документов';
        $client_ru = json_encode(['подача документов']);
        $consultant_ru = json_encode(['согласование с руководством', 'сопровождение при личных визитах']);

        $title_eng = 'preparation of documents';
        $client_eng = json_encode(['preparation of documents']);
        $consultant_eng = json_encode(['coordination with managment', 'personal visit support']);

        for ($i = 1; $i < 7; $i++) {
            $this->createInteraction(Page::PAGE_SRVM, $i, $title_ru.$i, $client_ru, $consultant_ru,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png', PAGE::LANGUAGE_RU);
            $this->createInteraction(Page::PAGE_SRVM, $i, $title_eng.$i, $client_eng, $consultant_eng,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png', PAGE::LANGUAGE_ENG);

            $this->createInteraction(Page::PAGE_RELOCATION, $i, $title_ru.$i, $client_ru, $consultant_ru,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_RU);
            $this->createInteraction(Page::PAGE_RELOCATION, $i, $title_eng.$i, $client_eng, $consultant_eng,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_ENG);

            $this->createInteraction(Page::PAGE_VISA_SUPPORT, $i, $title_ru.$i, $client_ru, $consultant_ru,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_RU);
            $this->createInteraction(Page::PAGE_VISA_SUPPORT, $i, $title_eng.$i, $client_eng, $consultant_eng,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_ENG);

            $this->createInteraction(Page::PAGE_REGISTRY_ACCREDITATION, $i, $title_ru.$i, $client_ru, $consultant_ru,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_RU);
            $this->createInteraction(Page::PAGE_REGISTRY_ACCREDITATION, $i, $title_eng.$i, $client_eng, $consultant_eng,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_ENG);

            $this->createInteraction(Page::PAGE_TRAVEL_RELAXATION, $i, $title_ru.$i, $client_ru, $consultant_ru,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_RU);
            $this->createInteraction(Page::PAGE_TRAVEL_RELAXATION, $i, $title_eng.$i, $client_eng, $consultant_eng,
                'images/steps-slider/'.$i.'.svg', 'images/img-srvm-2.png',
                PAGE::LANGUAGE_ENG);
        }
    }

    protected function createInteraction(
        $category,
        $slide,
        $title,
        $client,
        $consultant,
        $icon,
        $background_image,
        $language
    ) {
        $interact = new Interactions();
        $interact->category = $category;
        $interact->slide = $slide;
        $interact->title = $title;
        $interact->unit_client = $client;
        $interact->unit_consultant = $consultant;
        $interact->icon = $icon;
        $interact->background_image = $background_image;
        $interact->language = $language;
        $interact->save();

        return $interact;
    }
}
