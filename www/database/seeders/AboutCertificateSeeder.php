<?php

namespace Database\Seeders;

use App\Models\AboutCertificate;
use Illuminate\Database\Seeder;

class AboutCertificateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCertificate('SVKK', 'images/img-about-2.png');
        $this->createCertificate('Свитедельство', 'images/img-about-2.png');
    }

    protected function createCertificate($name, $image)
    {
        $certificate = new AboutCertificate();
        $certificate->name = $name;
        $certificate->image = $image;
        $certificate->save();

        return $certificate;
    }
}
