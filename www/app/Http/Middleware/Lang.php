<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class Lang {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     *
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null ) {
        $userLocale = session( 'locale', app()->getLocale() );
        $locale     = $request->segment( 1 );

        if ( empty( $locale ) ) {
            return redirect()->to( '/' . $userLocale );
        }

        if ( in_array( $locale, [ 'en', 'ru' ] )!==false ) {
            app()->setLocale( $locale );
            if ( $userLocale != $locale ) {
                session( [ 'locale' => $locale ] );

            }

            URL::defaults( [ 'locale' => $locale ] );
            if(Auth::check() && Auth::user()->locale!=$locale) {
                Auth::user()->update(['locale' => $locale]);
            }
        } else {
            return redirect()->to($userLocale .'/'. $request->path());
        }

        return $next( $request );
    }
}
