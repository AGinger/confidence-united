<?php

namespace App\Http\Requests\Admin\Services;

use Illuminate\Foundation\Http\FormRequest;

class SaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required',
            'number' => 'nullable|integer',
            'name_ru' => 'required|string',
            'name_eng' => 'required|string',
            'description_ru' => 'required|string',
            'description_eng' => 'required|string',
            'background_image' => 'sometimes|nullable|mimes:jpg,jpeg,bmp,png,webp|max:4096',
        ];
    }
}
