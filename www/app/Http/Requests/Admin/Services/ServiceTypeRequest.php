<?php

namespace App\Http\Requests\Admin\Services;

use Illuminate\Foundation\Http\FormRequest;

class ServiceTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name_ru" => 'required|string',
            "announcement_ru" => 'required|string',
            "description_ru" => 'required|string',
            "title_meta_ru" => 'required|string',
            "keywords_meta_ru" => 'required|string',
            "description_meta_ru" => 'required|string',
            "name_eng" => 'required|string',
            "announcement_eng" => 'required|string',
            "description_eng" => 'required|string',
            "title_meta_eng" => 'required|string',
            "keywords_meta_eng" => 'required|string',
            "description_meta_eng" => 'required|string',
            'background_image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,webp|max:4096'
        ];
    }
}
