<?php

namespace App\Http\Controllers;

use App\Models\AboutCertificate;
use App\Models\AboutMeta;
use App\Models\AboutReviews;
use App\Models\AboutTeam;
use App\Models\Countries;
use App\Models\InformAgency;
use App\Models\Interactions;
use App\Models\MainClients;
use App\Models\MainPage;
use App\Models\Page;
use App\Models\Sale;
use App\Models\Services;
use App\Models\ServiceType;
use App\Models\ServiceVariations;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $news = InformAgency::where('type', InformAgency::TYPE_NEWS)->where('language', $lang)->limit(5)->latest()->get();
        $events = InformAgency::where('type', InformAgency::TYPE_EVENTS)->where('language', $lang)->limit(5)->latest()->get();
        $publications = InformAgency::where('type', InformAgency::TYPE_PUBLICATIONS)->where('language',
            $lang)->limit(5)->latest()->get();
        $page = Page::where('category', Page::PAGE_MAIN)->where('language', $lang)->first();
        $mconsult = Page::where('category', Page::PAGE_SRVM)->where('language', $lang)->first();
        $visa = Page::where('category', Page::PAGE_VISA_SUPPORT)->where('language', $lang)->first();
        $travel = Page::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language', $lang)->first();
        $relocation = Page::where('category', Page::PAGE_RELOCATION)->where('language', $lang)->first();
        $accreditation = Page::where('category', Page::PAGE_REGISTRY_ACCREDITATION)->where('language',
            $lang)->first();
        $clients_ru_first = MainClients::where('type', MainClients::TYPE_RUSSIAN)->where('slide', 1)->get();
        $clients_ru_second = MainClients::where('type', MainClients::TYPE_RUSSIAN)->where('slide', 2)->get();
        $clients_int_first = MainClients::where('type', MainClients::TYPE_INTERNATIONAL)->where('slide', 1)->get();
        $clients_int_second = MainClients::where('type', MainClients::TYPE_INTERNATIONAL)->where('slide', 2)->get();
        $footer_info = MainPage::where('language', $lang)->first();

        return view('pages.index')->with([
            'page' => $page,
            'news' => $news, 'events' => $events, 'publications' => $publications, 'mconsult' => $mconsult,
            'visa' => $visa, 'travel' => $travel, 'relocation' => $relocation, 'accreditation' => $accreditation,
            'clients_ru_first' => $clients_ru_first, 'clients_ru_second' => $clients_ru_second,
            'clients_int_first' => $clients_int_first, 'clients_int_second' => $clients_int_second,
            'footer_info' => $footer_info, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function about($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $page = Page::where('category', Page::PAGE_ABOUT)->where('language', $lang)->first();
        $reviews = AboutReviews::where('language', $lang)->get();
        $team = AboutTeam::all();
        $certificates = AboutCertificate::all();
        $meta = AboutMeta::where('language', $lang)->first();

        return view('pages.about')->with([
            'page' => $page, 'reviews' => $reviews, 'team' => $team, 'certificates' => $certificates, 'meta' => $meta,
            'locale' => $locale
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createReview(Request $request)
    {
        $review = new AboutReviews();
        $review->author = $request->author;
        $review->description = $request->description;
        $review->language = $request->language;
        $review->save();

        return back();
    }

    /**
     * @param $file
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function aboutGetFile($file)
    {
        $myFile = public_path('presentations/'.$file);
        $headers = ['Content-Type: application/octet-stream'];

        return response()->download($myFile, $file, $headers);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function event($locale)
    {
        return view('pages.event')->with(['locale' => $locale]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function mconsult($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $page = Page::where('category', Page::PAGE_SRVM)->where('language', $lang)->first();
        $service_types = ServiceType::where('page', Page::PAGE_SRVM)->get();
        $services = Services::where('page', Page::PAGE_SRVM)->where('language', $lang)->get();
        $interactions = Interactions::where('category', Page::PAGE_SRVM)->where('language', $lang)->get();
        $sales = Sale::where('category', Page::PAGE_SRVM)->where('language', $lang)->get();

        return view('pages.services.mconsult')->with([
            'page' => $page, 'service_types' => $service_types, 'services' => $services,
            'interactions' => $interactions, 'sales' => $sales, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function registrationAccreditation($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $page = Page::where('category', Page::PAGE_REGISTRY_ACCREDITATION)->where('language',
            $lang)->first();
        $service_types = ServiceType::where('page', Page::PAGE_REGISTRY_ACCREDITATION)->get();
        $services = Services::where('page', Page::PAGE_REGISTRY_ACCREDITATION)->where('language',
            $lang)->get();
        $interactions = Interactions::where('category', Page::PAGE_REGISTRY_ACCREDITATION)->where('language',
            $lang)->get();
        $sales = Sale::where('category', Page::PAGE_REGISTRY_ACCREDITATION)->where('language',
            $lang)->get();

        return view('pages.services.registration-and-accreditation')->with([
            'page' => $page, 'service_types' => $service_types, 'services' => $services,
            'interactions' => $interactions, 'sales' => $sales, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function relocation($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }
        $page = Page::where('category', Page::PAGE_RELOCATION)->where('language', $lang)->first();
        $service_types = ServiceType::where('page', Page::PAGE_RELOCATION)->get();
        $services = Services::where('page', Page::PAGE_RELOCATION)->where('language', $lang)->get();
        $interactions = Interactions::where('category', Page::PAGE_RELOCATION)->where('language',
            $lang)->get();
        $sales = Sale::where('category', Page::PAGE_RELOCATION)->where('language', $lang)->get();

        return view('pages.services.relocation-services')->with([
            'page' => $page, 'service_types' => $service_types, 'services' => $services,
            'interactions' => $interactions, 'sales' => $sales, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function travel($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }
        $page = Page::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language',
            $lang)->first();
        $service_types = ServiceType::where('page', Page::PAGE_TRAVEL_RELAXATION)->get();
        $services = Services::where('page', Page::PAGE_TRAVEL_RELAXATION)->where('language',
            $lang)->get();
        $interactions = Interactions::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language',
            $lang)->get();
        $sales = Sale::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language',
            $lang)->get();

        return view('pages.services.travel-and-leisure')->with([
            'page' => $page, 'service_types' => $service_types, 'services' => $services,
            'interactions' => $interactions, 'sales' => $sales, 'locale' => $locale
        ]);
    }

    /**
     * @param Request $request
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function visa(Request $request, $locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }
        $page = Page::where('category', Page::PAGE_VISA_SUPPORT)->where('language', $lang)->first();
        $interactions = Interactions::where('category', Page::PAGE_VISA_SUPPORT)->where('language',
            $lang)->get();
        $sales = Sale::where('category', Page::PAGE_VISA_SUPPORT)->where('language', $lang)->get();
        $countries = Countries::all();
        $services = Services::where('number', $request->country)->where('language', $lang)->get();

        return view('pages.services.visa-support')->with([
            'page' => $page, 'services' => $services, 'countries' => $countries, 'interactions' => $interactions,
            'sales' => $sales, 'locale' => $locale
        ]);
    }

    /**
     * @param int $number
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function mconsultInner($locale, int $number)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }
        if (ServiceVariations::where('service_number', $number)->exists()) {
            $variations = ServiceVariations::where('service_number', $number)->get();
        } else {
            $variations = null;
        }
        $service = Services::where('number', $number)->where('language', $lang)->first();
        $other_services = Services::where('page', $service->page)->where('language', $lang)->get();

        return view('pages.services.mconsult-inner')->with([
            'variations' => $variations, 'service' => $service, 'page' => $service, 'other_services' => $other_services, 'locale' => $locale
        ]);
    }

    /**
     * @param int $number
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function mconsultSale($locale, $number)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $sale = Sale::where('number', $number)->where('language', $lang)->first();

        return view('pages.services.sale')->with(['sale' => $sale, 'page' => $sale, 'locale' => $locale]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function news($locale)
    {
        return view('pages.news')->with(['locale' => $locale]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function publication($locale)
    {
        return view('pages.publication')->with(['locale' => $locale]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sale($locale)
    {
        return view('pages.sale')->with(['locale' => $locale]);
    }
}
