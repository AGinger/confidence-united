<?php

namespace App\Http\Controllers;

use App\Models\MainClients;
use App\Models\MainPage;
use App\Models\Page;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_MAIN)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_MAIN)->where('language', Page::LANGUAGE_ENG)->first();
        $text_ru = MainPage::where('language', Page::LANGUAGE_RU)->first();
        $text_eng = MainPage::where('language', Page::LANGUAGE_ENG)->first();
        $clients = MainClients::all();

        return view('admin.pages.main')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng,
            'text_ru' => $text_ru, 'text_eng' => $text_eng,
            'clients' => $clients
        ]);
    }
}
