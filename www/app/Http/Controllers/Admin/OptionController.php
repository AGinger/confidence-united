<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OptionRequest;
use App\Models\Option;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metaOptions = Option::where('type', Option::TYPE_META)->get();

        return view('admin.pages.option.index', compact('metaOptions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function edit($key)
    {
        $option = Option::where('key', $key)->first();

        return view('admin.pages.option.edit', compact('option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $key
     * @return \Illuminate\Http\Response
     */
    public function update(OptionRequest $request, $key)
    {

        $updatableOption = Option::where('key', $key)->first();

        $updatableOption->update($request->all());

        return redirect(route('admin.option.index'));
    }

}
