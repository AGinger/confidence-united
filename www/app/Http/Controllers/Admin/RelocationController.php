<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interactions;
use App\Models\Page;
use Illuminate\Http\Request;

class RelocationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_RELOCATION)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_RELOCATION)->where('language', Page::LANGUAGE_ENG)->first();
        $interaction_ru = Interactions::where('category', Page::PAGE_RELOCATION)->where('language',
            Page::LANGUAGE_RU)->get();
        $interaction_eng = Interactions::where('category', Page::PAGE_RELOCATION)->where('language',
            Page::LANGUAGE_ENG)->get();

        return view('admin.pages.relocation.index')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng, 'interaction_ru' => $interaction_ru,
            'interaction_eng' => $interaction_eng
        ]);
    }
}
