<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MainClients;
use App\Models\MainPage;
use App\Models\Page;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_MAIN)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_MAIN)->where('language', Page::LANGUAGE_ENG)->first();
        $text_ru = MainPage::where('language', Page::LANGUAGE_RU)->first();
        $text_eng = MainPage::where('language', Page::LANGUAGE_ENG)->first();
        $clients_ru = MainClients::where('type', MainClients::TYPE_RUSSIAN)->get();
        $clients_int = MainClients::where('type', MainClients::TYPE_INTERNATIONAL)->get();

        return view('admin.pages.main.index')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng,
            'text_ru' => $text_ru, 'text_eng' => $text_eng,
            'clients_ru' => $clients_ru, 'clients_int' => $clients_int
        ]);
    }

    /**
     * @param Request $request
     * @param bool $lang
     */
    public function textStore(Request $request, bool $lang)
    {
        unset($request['_token']);
        MainPage::where('language', $lang)->update($request->all());

        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clientStore(Request $request)
    {
        $request->validate([
            'slide' => 'required|digits_between:1,3',
        ]);

        $imageName = time().'.'.$request->file('image')->extension();
        $filepath = $request->file('image')->move('images/clients', $imageName);

        MainClients::create([
            'image' => $filepath,
            'slide' => $request->slide,
            'type' => $request->type
        ]);

        return back();
    }

    /**
     * @param MainClients $client
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function clientEdit(MainClients $client)
    {
        return view('admin.pages.main.clients')->with(['client' => $client]);
    }

    /**
     * @param Request $request
     * @param MainClients $client
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clientUpdate(Request $request, MainClients $client)
    {
        $filepath = $client->image;

        if ($request->file('image') !== null) {
            $imageName = time().'.'.$request->file('image')->extension();
            $filepath = $request->file('image')->move('images/clients', $imageName);
        }

        $client->update([
            'image' => $filepath,
            'slide' => $request->slide,
            'type' => $request->type,
            'name' => $request->name
        ]);

        return redirect()->route('admin.main.index');
    }

    /**
     * @param MainClients $client
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function clientDelete(MainClients $client)
    {
        $client->delete();

        return redirect()->route('admin.main.index');
    }
}
