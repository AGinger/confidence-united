<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Countries;
use App\Models\Page;
use App\Models\Services;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $country = new Countries();
        $country->name_ru = $request->name_ru;
        $country->name_eng = $request->name_eng;
        $country->service_number = $request->service_number;
        $country->purpose = $request->purpose;
        $country->save();

        return back();
    }

    /**
     * @param Countries $country
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Countries $country)
    {
        $services = Services::where('page', Page::PAGE_VISA_SUPPORT)->where('language', Page::LANGUAGE_RU)->get();

        return view('admin.pages.visa-support.country')->with(['country' => $country, 'services' => $services]);
    }

    /**
     * @param Request $request
     * @param Countries $country
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Countries $country)
    {
        $country->update($request->all());

        return redirect()->route('admin.visa.index');
    }
}
