<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Countries;
use App\Models\Interactions;
use App\Models\MIA;
use App\Models\Page;
use App\Models\Services;
use Illuminate\Http\Request;

class VisaSupportController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_VISA_SUPPORT)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_VISA_SUPPORT)->where('language', Page::LANGUAGE_ENG)->first();
        $interaction_ru = Interactions::where('category', Page::PAGE_VISA_SUPPORT)->where('language',
            Page::LANGUAGE_RU)->get();
        $interaction_eng = Interactions::where('category', Page::PAGE_VISA_SUPPORT)->where('language',
            Page::LANGUAGE_ENG)->get();
        $countries = Countries::all();
        $services = Services::where('page', Page::PAGE_VISA_SUPPORT)->where('language', Page::LANGUAGE_RU)->get();
        $mias = MIA::where('category', 'strany')->where('language', Page::LANGUAGE_RU)->get();

        return view('admin.pages.visa-support.index')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng, 'interaction_ru' => $interaction_ru,
            'interaction_eng' => $interaction_eng, 'countries' => $countries, 'services' => $services, 'mias' => $mias
        ]);
    }
}
