<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interactions;
use App\Models\Page;
use Illuminate\Http\Request;

class MethodController extends Controller
{
    /**
     * @param Request $request
     * @param string $page
     * @param bool $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, string $page, bool $lang)
    {
        unset($request['_token']);
        Page::where('category', $page)->where('language', $lang)->update($request->all());

        return back();
    }

    /**
     * @param Request $request
     * @param string $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeImage(Request $request, string $page)
    {
        $request->validate([
            'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
        ]);
        $fileName = time().'.'.$request->file('background_image')->extension();
        $filePath = $request->file('background_image')->move('images/'.$page, $fileName);

        $pages = Page::where('category', $page)->get();

        foreach ($pages as $page) {
            $page->update([
                'background_image' => $filePath
            ]);
        }

        return back();
    }

    /**
     * @param Request $request
     * @param Interactions $interaction
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeInteraction(Request $request, Interactions $interaction)
    {
        $filePath = $interaction->background_image;

        if ($request->hasFile('background_image')) {
            $request->validate([
                'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
            ]);

            $fileName = time().'.'.$request->file('background_image')->extension();
            $filePath = $request->file('background_image')->move('images/interactions'.$interaction->category,
                $fileName);
        }
        $interaction->update([
            'title' => $request->title,
            'unit_client' => json_encode($request->client),
            'unit_consultant' => json_encode($request->consultant),
            'background_image' => $filePath
        ]);

        return back();
    }
}
