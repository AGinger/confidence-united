<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Services\MIARequest;
use App\Models\MIA;
use App\Models\Page;
use App\Models\ServiceType;
use Illuminate\Http\Request;

class MIAController extends Controller
{
    /**
     * @param ServiceType|null $service_type
     * @param int $number
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(ServiceType $serviceType, int $number)
    {
        $mia_ru = MIA::where('number', $number)->where('language', PAGE::LANGUAGE_RU)->first();
        $mia_eng = MIA::where('number', $number)->where('language', PAGE::LANGUAGE_ENG)->first();

        return view('admin.pages.service-type.mia.edit')->with([
            'mia_ru' => $mia_ru, 'mia_eng' => $mia_eng,
        ]);
    }

    /**
     * @param Request $request
     * @param MIA $mia
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MIARequest $request, MIA $mia)
    {
        $mia->update($request->all());

        return back();
    }

    /**
     * @param Request $request
     * @param int $number
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateImage(MIARequest $request, int $number)
    {
        $mias = MIA::where('number', $number)->get();

        foreach ($mias as $mia) {
            $filePath = $mia->background_image;

            if ($request->hasFile('background_image')) {
                $request->validate([
                    'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
                ]);

                $fileName = time().'.'.$request->file('background_image')->extension();
                $filePath = $request->file('background_image')->move('images/mia/'.$mia->number,
                    $fileName);
            }
            $mia->update([
                'background_image' => $filePath
            ]);
        }

        return back();
    }

    /**
     * @param string $page
     * @param string $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(string $page, string $category = null)
    {
        return view('admin.pages.service-type.mia.add')->with(['type' => $page, 'category' => $category]);
    }

    public function store(MIARequest $request)
    {

       $mia = new MIA();
       $number = $mia->getNumber($request->type);

        $filePath = null;

        if ($request->hasFile('background_image')) {
            $fileName = time().'.'.$request->file('background_image')->extension();
            $filePath = $request->file('background_image')->move('images/inform-agency/'.$number, $fileName);
        }

        $this->createMIA($request->category, $number, $request->name_ru,
            $request->announcement_ru, $request->description_ru, $request->title_meta_ru,
            $request->keywords_meta_ru, $request->description_meta_ru, $filePath, Page::LANGUAGE_RU);

        $this->createMIA($request->category, $number, $request->name_eng,
            $request->announcement_eng, $request->description_eng, $request->title_meta_eng,
            $request->keywords_meta_eng, $request->description_meta_eng, $filePath, Page::LANGUAGE_ENG);

        if ($request->type != Page::PAGE_VISA_SUPPORT) {
            return redirect()->route('admin.service-type.index', [
                'page' => $request->type, 'category' => $request->category
            ]);
        } else {
            return redirect()->route('admin.visa.index');
        }
    }

    /**
     * @param MIA $mia
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(MIA $mia)
    {
        $mia->delete();

        return back();
    }


    protected function createMIA(
        $category,
        $number,
        $name,
        $announcement,
        $description,
        $title_meta,
        $keywords_meta,
        $description_meta,
        $image,
        $language
    ) {
        $service = new MIA();
        $service->category = $category;
        $service->number = $number;
        $service->name = $name;
        $service->announcement = $announcement;
        $service->description = $description;
        $service->title_meta = $title_meta;
        $service->keywords_meta = $keywords_meta;
        $service->description_meta = $description_meta;
        $service->background_image = $image;
        $service->language = $language;
        $service->save();
    }
}
