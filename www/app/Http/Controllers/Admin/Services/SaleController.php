<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Services\SaleRequest;
use App\Models\Page;
use App\Models\Sale;
use Illuminate\Http\Request;

class SaleController extends Controller
{

    /**
     * @param string $page
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(string $page)
    {
        $sales = Sale::where('category', $page)->where('language', Page::LANGUAGE_RU)->get();
        $parentPage = Page::where('category', $page)->where('language', Page::LANGUAGE_RU)->first();

        return view('admin.pages.service-type.sale.index')->with([
            'sales' => $sales,
            'page' => $page,
            'parentName' => $parentPage->name,
            'parentRoute' => $parentPage->getParentPageRoute()
        ]);
    }

    /**
     * @param Sale $sale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Sale $sale)
    {
        $sale_ru = $sale;
        $sale_eng = Sale::where('number', $sale_ru->number)->where('language', Page::LANGUAGE_ENG)->first();

        return view('admin.pages.service-type.sale.edit')->with(['sale_ru' => $sale_ru, 'sale_eng' => $sale_eng]);
    }

    /**
     * @param Request $request
     * @param Sale $sale
     */
    public function update(SaleRequest $request, Sale $sale)
    {
        $sale->update($request->all());

        return redirect()->route('admin.service-type.sale', ['page' => $sale->category]);
    }

    /**
     * @param Request $request
     * @param int $number
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateImage(Request $request, int $number)
    {
        $sales = Sale::where('number', $number)->get();

        foreach ($sales as $sale) {
            $filePath = $sale->background_image;

            if ($request->hasFile('background_image')) {
                $category = $sale->category;
                $request->validate([
                    'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
                ]);
                $fileName = time().'.'.$request->file('background_image')->extension();
                $filePath = $request->file('background_image')->move('images/services/'.$category.'/'.$sale->number,
                    $fileName);
            }
            $sale->update([
                'background_image' => $filePath
            ]);
        }

        return redirect()->route('admin.service-type.sale', ['page' => $category]);
    }

    /**
     * @param string $page
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(string $page)
    {
        return view('admin.pages.service-type.sale.add')->with(['type' => $page]);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function store(SaleRequest $request)
    {
        $sale = new Sale();
        $number = $sale->getNumber($request->type);

        $filePath = null;

        if ($request->hasFile('background_image')) {
            $fileName = time().'.'.$request->file('background_image')->extension();
            $filePath = $request->file('background_image')->move('images/inform-agency/'.$number, $fileName);
        }

        $this->createSale($request->type, $number, $request->date, $request->name_ru, $request->description_ru,
            $filePath, Page::LANGUAGE_RU);
        $this->createSale($request->type, $number, $request->date, $request->name_eng, $request->description_eng,
            $filePath, Page::LANGUAGE_ENG);

        return redirect()->route('admin.service-type.sale.index', ['page' => $request->type]);
    }

    /**
     * @param $type
     * @param $number
     * @param $date
     * @param $name
     * @param $description
     * @param $filePath
     * @param $language
     */
    protected function createSale($type, $number, $date, $name, $description, $filePath, $language)
    {
        $sale = new Sale();
        $sale->category = $type;
        $sale->number = $number;
        $sale->date = $date;
        $sale->name = $name;
        $sale->description = $description;
        $sale->background_image = $filePath;
        $sale->language = $language;
        $sale->save();
    }
}
