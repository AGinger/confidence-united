<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use App\Models\ServiceType;
use App\Models\ServiceVariations;
use Illuminate\Http\Request;

class VariationController extends Controller
{
    /**
     * @param ServiceType $serviceType
     * @param ServiceVariations $variation
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(ServiceType $serviceType, ServiceVariations $variation)
    {
        return view('admin.pages.service.variation.index')->with(['variation' => $variation]);
    }

    /**
     * @param Request $request
     * @param ServiceVariations $variation
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, ServiceVariations $variation)
    {
        $variation->update($request->all());

        return back();
    }
}
