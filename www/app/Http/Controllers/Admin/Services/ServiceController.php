<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Services;
use App\Models\ServiceType;
use App\Models\ServiceVariations;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * @param ServiceType $serviceType
     * @param int $number
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(ServiceType $serviceType = null, int $number)
    {
        $service_ru = Services::where('number', $number)->where('language', PAGE::LANGUAGE_RU)->first();
        $service_eng = Services::where('number', $number)->where('language', PAGE::LANGUAGE_ENG)->first();
        $variations = ServiceVariations::where('service_number', $number)->get();
        $mainParentPage = Page::where('category', $serviceType->page)->where('language', Page::LANGUAGE_RU)->first();

        return view('admin.pages.service.edit')->with([
            'service_ru' => $service_ru,
            'service_eng' => $service_eng,
            'variations' => $variations,
            'serviceType' => $serviceType,
            'mainParentName' => $mainParentPage->name,
            'mainParentRoute' => $mainParentPage->getParentPageRoute()

        ]);
    }

    /**
     * @param Request $request
     * @param Services $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Services $service)
    {
        $service->update($request->all());

        return back();
    }

    /**
     * @param Request $request
     * @param int $number
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateImage(Request $request, int $number)
    {
        $services = Services::where('number', $number)->get();

        foreach ($services as $service) {
            $filePath = $service->background_image;

            if ($request->hasFile('background_image')) {
                $request->validate([
                    'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
                ]);

                $fileName = time().'.'.$request->file('background_image')->extension();
                $filePath = $request->file('background_image')->move('images/services/'.$service->number,
                    $fileName);
            }
            $service->update([
                'background_image' => $filePath
            ]);
        }

        return back();
    }
}
