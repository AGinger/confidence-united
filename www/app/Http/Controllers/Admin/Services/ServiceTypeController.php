<?php

namespace App\Http\Controllers\Admin\Services;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Services\ServiceRequest;
use App\Http\Requests\Admin\Services\ServiceTypeRequest;
use App\Models\MIA;
use App\Models\Page;
use App\Models\Services;
use App\Models\ServiceType;
use Illuminate\Http\Request;

class ServiceTypeController extends Controller
{
    /**
     * @param string $page
     * @param string $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(string $page, string $category = null)
    {
        $service_type = ServiceType::where('page', $page)->where('category', $category)->first();
        $services = Services::where('category', $category)->where('page', $page)->where('language',
            Page::LANGUAGE_RU)->get();
        $parentPage = Page::where('category', $page)->where('language', Page::LANGUAGE_RU)->first();
        $mias = MIA::where('category', $category)->where('language', Page::LANGUAGE_RU)->get();

        return view('admin.pages.service-type.index')->with([
            'service_type' => $service_type,
            'services' => $services,
            'mias' => $mias,
            'category' => $category,
            'page' => $page,
            'parentName' => $parentPage->name,
            'parentRoute' => $parentPage->getParentPageRoute()
        ]);
    }


    /**
     * @param Request $request
     * @param ServiceType $serviceType
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ServiceRequest $request, ServiceType $serviceType)
    {
        $serviceType->update($request->all());

        return back();
    }

    /**
     * @param string $page
     * @param string $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(string $page, string $category = null)
    {
        return view('admin.pages.service.add')->with(['type' => $page, 'category' => $category]);
    }

    /**
     * @param string $page
     * @param string $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addMIA(string $page, string $category = null)
    {
        return view('admin.pages.service-type.mia.add')->with(['type' => $page, 'category' => $category]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(ServiceTypeRequest $request)
    {

        $serviceType = new ServiceType();
        $number = $serviceType->getNumber($request->type);

        $filePath = null;

        if ($request->hasFile('background_image')) {
            $request->validate(['background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',]);

            $fileName = time().'.'.$request->file('background_image')->extension();
            $filePath = $request->file('background_image')->move('images/inform-agency/'.$number, $fileName);
        }

        $this->createService($request->type, $request->category, $number, $request->name_ru,
            $request->announcement_ru, $request->description_ru, $request->title_meta_ru,
            $request->keywords_meta_ru, $request->description_meta_ru, $filePath, Page::LANGUAGE_RU);

        $this->createService($request->type, $request->category, $number, $request->name_eng,
            $request->announcement_eng, $request->description_eng, $request->title_meta_eng,
            $request->keywords_meta_eng, $request->description_meta_eng, $filePath, Page::LANGUAGE_ENG);

        if ($request->type != Page::PAGE_VISA_SUPPORT) {
            return redirect()->route('admin.service-type.index', [
                'page' => $request->type, 'category' => $request->category
            ]);
        } else {
            return redirect()->route('admin.visa.index');
        }

    }

    /**
     * @param Services $service
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Services $service)
    {
        $service->delete();

        return back();
    }


    protected function createService(
        $page,
        $category,
        $number,
        $name,
        $announcement,
        $description,
        $title_meta,
        $keywords_meta,
        $description_meta,
        $image,
        $language
    ) {
        $service = new Services();
        $service->page = $page;
        $service->category = $category;
        $service->number = $number;
        $service->name = $name;
        $service->announcement = $announcement;
        $service->description = $description;
        $service->title_meta = $title_meta;
        $service->keywords_meta = $keywords_meta;
        $service->description_meta = $description_meta;
        $service->background_image = $image;
        $service->language = $language;
        $service->save();
    }
}
