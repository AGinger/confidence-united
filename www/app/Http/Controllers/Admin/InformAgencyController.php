<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\InformAgency;
use App\Models\Page;
use Illuminate\Http\Request;

class InformAgencyController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_INFORM_AGENCY)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_INFORM_AGENCY)->where('language', Page::LANGUAGE_ENG)->first();
        $news = InformAgency::where('type', InformAgency::TYPE_NEWS)->where('language', Page::LANGUAGE_RU)->get();
        $events = InformAgency::where('type', InformAgency::TYPE_EVENTS)->where('language', Page::LANGUAGE_RU)->get();
        $publications = InformAgency::where('type', InformAgency::TYPE_PUBLICATIONS)->where('language',
            Page::LANGUAGE_RU)->get();

        return view('admin.pages.news-agency.index')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng, 'news' => $news, 'events' => $events,
            'publications' => $publications
        ]);
    }

    /**
     * @param int $number
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $number)
    {
        $agency_ru = InformAgency::where('number', $number)->where('language', Page::LANGUAGE_RU)->first();
        $agency_eng = InformAgency::where('number', $number)->where('language', Page::LANGUAGE_ENG)->first();

        return view('admin.pages.news-agency.edit')->with(['agency_ru' => $agency_ru, 'agency_eng' => $agency_eng]);
    }

    /**
     * @param Request $request
     * @param InformAgency|null $agency
     */
    public function update(Request $request, InformAgency $agency = null)
    {
        $agency->update($request->all());

        return back();
    }

    /**
     * @param Request $request
     * @param int $number
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateImage(Request $request, int $number)
    {
        $agencies = InformAgency::where('number', $number)->get();

        foreach ($agencies as $agency) {
            $filePath = $agency->background_image;

            if ($request->hasFile('background_image')) {
                $request->validate([
                    'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
                ]);

                $fileName = time().'.'.$request->file('background_image')->extension();
                $filePath = $request->file('background_image')->move('images/inform-agency/'.$agency->number,
                    $fileName);
            }
            $agency->update([
                'background_image' => $filePath
            ]);
        }

        return back();
    }

    /**
     * @param int|null $number
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add(int $number = null, string $type)
    {
        return view('admin.pages.news-agency.add')->with(['type' => $type]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function create(Request $request)
    {
        if ($request->type === 'news') {
            $number = 1100 + random_int(10, 99);
        } elseif ($request->type === 'events') {
            $number = 1200 + random_int(10, 99);
        } else {
            $number = 1300 + random_int(10, 99);
        }

        $filePath = null;

        if ($request->hasFile('background_image')) {
            $request->validate([
                'background_image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:500',
            ]);

            $fileName = time().'.'.$request->file('background_image')->extension();
            $filePath = $request->file('background_image')->move('images/inform-agency/'.$number, $fileName);
        }

        $agency = new InformAgency();
        $agency->type = $request->type;
        $agency->number = $number;
        $agency->name = $request->name_ru;
        $agency->description = $request->description_ru;
        $agency->title_meta = $request->title_meta_ru;
        $agency->keywords_meta = $request->keywords_meta_ru;
        $agency->description_meta = $request->description_meta_ru;
        $agency->background_image = $filePath;
        $agency->language = Page::LANGUAGE_RU;
        $agency->save();

        $agency = new InformAgency();
        $agency->type = $request->type;
        $agency->number = $number;
        $agency->name = $request->name_eng;
        $agency->description = $request->description_eng;
        $agency->title_meta = $request->title_meta_eng;
        $agency->keywords_meta = $request->keywords_meta_eng;
        $agency->description_meta = $request->description_meta_eng;
        $agency->background_image = $filePath;
        $agency->language = Page::LANGUAGE_ENG;
        $agency->save();

        return redirect()->route('admin.agency.index');
    }
}
