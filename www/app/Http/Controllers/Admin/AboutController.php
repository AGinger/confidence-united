<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AboutUs\CertificateRequest;
use App\Models\AboutCertificate;
use App\Models\AboutMeta;
use App\Models\AboutReviews;
use App\Models\AboutTeam;
use App\Models\Page;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * PageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_ABOUT)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_ABOUT)->where('language', Page::LANGUAGE_ENG)->first();
        $meta_ru = AboutMeta::where('language', Page::LANGUAGE_RU)->first();
        $meta_eng = AboutMeta::where('language', Page::LANGUAGE_ENG)->first();
        $reviews_ru = AboutReviews::where('language', Page::LANGUAGE_RU)->get();
        $reviews_eng = AboutReviews::where('language', Page::LANGUAGE_ENG)->get();
        $team = AboutTeam::all();
        $certificates = AboutCertificate::all();

        return view('admin.pages.about.index')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng, 'meta_ru' => $meta_ru, 'meta_eng' => $meta_eng,
            'reviews_ru' => $reviews_ru, 'reviews_eng' => $reviews_eng, 'team' => $team, 'certificates' => $certificates
        ]);
    }

    /**
     * @param Request $request
     * @param bool $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function metaStore(Request $request, bool $lang)
    {
        $request->request->remove('_token');
        $request['video_link'] = str_replace('watch?v=', 'embed/', $request->video_link);
        if ($request['presentation'] !== null) {

            $fileName = time().'.'.$request->file('presentation')->extension();
            $filePath = $request->file('presentation')->move('presentations', $fileName);
            AboutMeta::where('language', $lang)->update(['presentation' => $filePath->getPathname()]);
        } else {
            AboutMeta::where('language', $lang)->update($request->all());
        }

        return back();
    }

    /**
     * @param AboutReviews $review
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function reviewEdit(AboutReviews $review)
    {
        return view('admin.pages.about.reviews')->with(['review' => $review]);
    }

    /**
     * @param Request $request
     * @param AboutReviews $review
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reviewUpdate(Request $request, AboutReviews $review)
    {
        $review->update($request->all());

        return redirect()->route('admin.about.index');
    }

    /**
     * @param AboutReviews $review
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reviewDelete(AboutReviews $review)
    {
        $review->delete();

        return redirect()->route('admin.about.index');
    }

    /**
     * @param AboutTeam $team
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function teamEdit(AboutTeam $team)
    {
        return view('admin.pages.about.team')->with(['team' => $team]);
    }

    /**
     * @param Request $request
     * @param AboutTeam $team
     * @return \Illuminate\Http\RedirectResponse
     */
    public function teamUpdate(Request $request, AboutTeam $team)
    {
        $image_first = $team->image_first;
        $image_second = $team->image_second;

        if ($request->file('image_first') !== null) {
            $imageName = time().'.'.$request->file('image_first')->extension();
            $image_first = $request->file('image_first')->move('images/team', $imageName);
        }

        if ($request->file('image_second') !== null) {
            $imageName = time().'.'.$request->file('image_second')->extension();
            $image_second = $request->file('image_second')->move('images/team', $imageName);
        }

        $team->update([
            'name_ru' => $request->name_ru,
            'name_eng' => $request->name_eng,
            'image_first' => $image_first,
            'image_second' => $image_second
        ]);

        return redirect()->route('admin.about.index');
    }

    /**
     * @param AboutTeam $team
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function teamDelete(AboutTeam $team)
    {
        $team->delete();

        return redirect()->route('admin.about.index');
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function certificateAdd()
    {
        return view('admin.pages.about.certificate_add');
    }

    /**
     * @param Request $request
     * @param AboutCertificate $certificate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function certificateStore(CertificateRequest $request)
    {
        $certificate = new AboutCertificate();

        if ($request->file('image') !== null) {
            $imageName = time().'.'.$request->file('image')->extension();
            $image = $request->file('image')->move('images/certificates', $imageName);
        }

        $certificate->name  = $request->name;
        $certificate->image = $image;
        $certificate->save();

        return redirect()->route('admin.about.index');
    }

    /**
     * @param AboutCertificate $certificate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function certificateEdit(AboutCertificate $certificate)
    {
        return view('admin.pages.about.certificate')->with(['certificate' => $certificate]);
    }

    /**
     * @param Request $request
     * @param AboutCertificate $certificate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function certificateUpdate(CertificateRequest $request, AboutCertificate $certificate)
    {
        $image = $certificate->image;

        if ($request->file('image') !== null) {
            $imageName = time().'.'.$request->file('image')->extension();
            $image = $request->file('image')->move('images/certificates', $imageName);
        }

        $certificate->update([
            'name' => $request->name,
            'image' => $image
        ]);

        return redirect()->route('admin.about.index');
    }

    /**
     * @param AboutTeam $team
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function certificateDelete(AboutCertificate $certificate)
    {
        $certificate->delete();

        return redirect()->route('admin.about.index');
    }
}
