<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interactions;
use App\Models\Page;
use Illuminate\Http\Request;

class TravelRelaxationController extends Controller
{
    public function index()
    {
        $page_ru = Page::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language', Page::LANGUAGE_RU)->first();
        $page_eng = Page::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language', Page::LANGUAGE_ENG)->first();
        $interaction_ru = Interactions::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language',
            Page::LANGUAGE_RU)->get();
        $interaction_eng = Interactions::where('category', Page::PAGE_TRAVEL_RELAXATION)->where('language',
            Page::LANGUAGE_ENG)->get();

        return view('admin.pages.travel.index')->with([
            'page_ru' => $page_ru, 'page_eng' => $page_eng, 'interaction_ru' => $interaction_ru,
            'interaction_eng' => $interaction_eng
        ]);
    }
}
