<?php

namespace App\Http\Controllers\InformAgency;

use App\Http\Controllers\Controller;
use App\Models\InformAgency;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * @param string $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(string $locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $news = InformAgency::where('type', InformAgency::TYPE_NEWS)->where('language', $lang)->limit(5)->latest()->get();
        $events = InformAgency::where('type', InformAgency::TYPE_EVENTS)->where('language', $lang)->limit(5)->latest()->get();
        $publications = InformAgency::where('type', InformAgency::TYPE_PUBLICATIONS)->where('language',
            $lang)->limit(5)->latest()->get();

        return view('pages.inform-agency.index')->with([
            'news' => $news, 'events' => $events, 'publications' => $publications, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function news($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $events = InformAgency::where('type', InformAgency::TYPE_EVENTS)->where('language',
            $lang)->limit(5)->latest()->get();
        $publications = InformAgency::where('type', InformAgency::TYPE_PUBLICATIONS)->where('language',
            $lang)->limit(5)->latest()->get();
        $news = InformAgency::where('type', InformAgency::TYPE_NEWS)->where('language',
            $lang)->paginate(2);

        return view('pages.inform-agency.news')->with([
            'events' => $events, 'publications' => $publications, 'news' => $news, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function publications($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $events = InformAgency::where('type', InformAgency::TYPE_EVENTS)->where('language',
            $lang)->limit(5)->latest()->get();
        $publications = InformAgency::where('type', InformAgency::TYPE_PUBLICATIONS)->where('language',
            $lang)->paginate(2);
        $news = InformAgency::where('type', InformAgency::TYPE_NEWS)->where('language',
            $lang)->limit(5)->latest()->get();

        return view('pages.inform-agency.publications')->with([
            'events' => $events, 'publications' => $publications, 'news' => $news, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function events($locale)
    {
        if ($locale == 'ru') {
            $lang = Page::LANGUAGE_RU;
        } else {
            $lang = Page::LANGUAGE_ENG;
        }

        $events = InformAgency::where('type', InformAgency::TYPE_EVENTS)->where('language',
            $locale)->paginate(2);
        $publications = InformAgency::where('type', InformAgency::TYPE_PUBLICATIONS)->where('language',
            $locale)->limit(5)->latest()->get();
        $news = InformAgency::where('type', InformAgency::TYPE_NEWS)->where('language',
            $locale)->limit(5)->latest()->get();

        return view('pages.inform-agency.events')->with([
            'events' => $events, 'publications' => $publications, 'news' => $news, 'locale' => $locale
        ]);
    }

    /**
     * @param $locale
     * @param string $type
     * @param InformAgency $agency
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function item($locale, string $type, InformAgency $agency)
    {
        $name = (new \App\Models\InformAgency)->getType($agency->type);

        return view('pages.inform-agency.item')->with(['page' => $agency, 'name' => $name, 'locale' => $locale]);
    }
}
