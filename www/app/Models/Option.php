<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed key
 * @property mixed value
 * @property mixed description
 */
class Option extends Model
{
    use HasFactory;

    const LANGUAGE_RU = 0;
    const LANGUAGE_ENG = 1;
    const TYPE_META = 'meta';
    const TYPE_MAIN = 'main';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'options';

    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'key',
        'value',
        'description',
        'type',
        'language'
    ];

    public static function getMetaOptions(){
        $metaTypeOptionsAll = Option::where('type', self::TYPE_META)->get()->keyBy('key');

        return $metaTypeOptionsAll;
    }
}
