<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name
 * @property mixed image
 */
class AboutCertificate extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'image'
    ];
}
