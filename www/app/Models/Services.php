<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed category
 * @property mixed number
 * @property mixed name
 * @property mixed announcement
 * @property mixed description
 * @property mixed title_meta
 * @property mixed keywords_meta
 * @property mixed description_meta
 * @property mixed background_image
 * @property mixed language
 * @property mixed page
 */
class Services extends Model
{
    use HasFactory;

    protected $fillable = [
        'category', 'number', 'name', 'announcement', 'description', 'title_meta', 'keywords_meta', 'description_meta',
        'background_image',
        'language'
    ];
}
