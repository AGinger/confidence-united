<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed service_number
 * @property mixed process_time
 * @property mixed government_duty
 * @property mixed price
 */
class ServiceVariations extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_number', 'process_time', 'government_duty', 'price'
    ];
}
