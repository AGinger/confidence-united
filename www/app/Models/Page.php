<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed category
 * @property mixed name
 * @property mixed keywords_meta
 * @property mixed title_meta
 * @property mixed description_meta
 * @property mixed language
 * @property mixed description
 * @property mixed|null background_image
 */
class Page extends Model
{
    use HasFactory;

    const LANGUAGE_RU = 0;
    const LANGUAGE_ENG = 1;
    const PAGE_MAIN = 'main';
    const PAGE_SRVM = 'migracionnyj-konsalting';
    const PAGE_ABOUT = 'about';
    const PAGE_RELOCATION = 'relokacionnye-uslugi';
    const PAGE_VISA_SUPPORT = 'vizovaya-podderzhka';
    const PAGE_INFORM_AGENCY = 'informagentstvo';
    const PAGE_TRAVEL_RELAXATION = 'puteshestviya-i-otdyh';
    const PAGE_REGISTRY_ACCREDITATION = 'registraciya-i-akkreditaciya';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name', 'description', 'title_meta', 'keywords_meta', 'description_meta', 'background_image', 'language'
    ];

    protected $hidden = [

    ];

    /**
     * @return array
     */
    public static function getLanguage()
    {
        return [
            self::LANGUAGE_RU => __('Russian'),
            self::LANGUAGE_ENG => __('English'),
        ];
    }

    /**
     * @return mixed
     */
    public function getParentPageRoute(){
        $routeList = [
            self::PAGE_SRVM => 'admin.srvm.index',
            self::PAGE_RELOCATION => 'admin.relocation.index',
            self::PAGE_VISA_SUPPORT => 'admin.visa.index',
            self::PAGE_TRAVEL_RELAXATION => 'admin.travel.index',
            self::PAGE_REGISTRY_ACCREDITATION => 'admin.accreditation.index'
        ];

        return $routeList[$this->category];
    }
}
