<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed announce
 * @property mixed video_link
 * @property mixed presentation
 * @property mixed language
 */
class AboutMeta extends Model
{
    use HasFactory;

    protected $fillable = [
      'announce', 'video_link', 'presentation', 'language'
    ];
}
