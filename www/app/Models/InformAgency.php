<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed type
 * @property mixed number
 * @property mixed name
 * @property mixed description
 * @property mixed title_meta
 * @property mixed keywords_meta
 * @property mixed description_meta
 * @property mixed background_image
 * @property mixed language
 */
class InformAgency extends Model
{
    use HasFactory;

    const TYPE_NEWS = 'news';
    const TYPE_PUBLICATIONS = 'publications';
    const TYPE_EVENTS = 'events';

    const LANGUAGE_RU = 0;
    const LANGUAGE_ENG = 1;

    /**
     * @return array
     */
    public static function getLanguage()
    {
        return [
            self::LANGUAGE_RU => __('Russian'),
            self::LANGUAGE_ENG => __('English'),
        ];
    }

    protected $fillable = [
        'type', 'number', 'name', 'description', 'title_meta', 'keywords_meta', 'description_meta', 'background_image',
        'language'
    ];

    /**
     * @param $type
     * @return string
     */
    public function getType($type): string
    {
        if ($type == 'news') {
            $name = 'Новости';
        } elseif ($type == 'events') {
            $name = 'Мероприятия';
        } else {
            $name = 'Публикации';
        }

        return $name;
    }
}
