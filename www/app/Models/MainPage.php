<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @property mixed services
 * @property mixed clients
 * @property mixed information_agency
 * @property mixed social_networks
 * @property mixed social_networks_second
 * @property mixed social_networks_third
 * @property mixed social_networks_fourth
 * @property mixed footer_contacts
 * @property mixed footer_office
 * @property mixed footer_business_hours
 * @property mixed language
 */
class MainPage extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'services', 'clients', 'information_agency', 'social_networks', 'social_networks_second',
        'social_networks_third', 'social_networks_fourth', 'footer_contacts', 'footer_office', 'footer_business_hours',
        'language'
    ];

    /**
     * @var array
     */
    protected $hidden = [
    ];
}
