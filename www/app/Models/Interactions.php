<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed category
 * @property mixed slide
 * @property mixed title
 * @property mixed unit_client
 * @property mixed unit_consultant
 * @property mixed language
 * @property mixed icon
 * @property mixed background_image
 */
class Interactions extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'category', 'slide', 'title', 'unit_client', 'unit_consultant', 'icon', 'background_image', 'language'
    ];
}
