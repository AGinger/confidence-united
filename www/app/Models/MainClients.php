<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed slide
 * @property mixed type
 * @property mixed name
 * @property mixed image
 */
class MainClients extends Model
{
    use HasFactory;

    const TYPE_RUSSIAN = 0;
    const TYPE_INTERNATIONAL = 1;

    /**
     * @return array
     */
    public static function getType()
    {
        return [
            self::TYPE_RUSSIAN => __('ru'),
            self::TYPE_INTERNATIONAL => __('en'),
        ];
    }

    protected $fillable = [
        'slide', 'type', 'name', 'image'
    ];
}
