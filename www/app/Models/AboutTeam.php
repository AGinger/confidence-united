<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name_ru
 * @property mixed name_eng
 * @property mixed image_first
 * @property mixed image_second
 */
class AboutTeam extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_ru', 'name_eng', 'image_first', 'image_second'
    ];
}
