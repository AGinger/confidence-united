<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name_ru
 * @property mixed name_eng
 * @property mixed service_number
 * @property mixed purpose
 */
class Countries extends Model
{
    use HasFactory;

    const PURPOSE_WORK = 'work';
    const PURPOSE_TOURISM = 'tourism';
    const PURPOSE_VISIT = 'visit';

    protected $fillable = [
      'name_ru', 'name_eng', 'service_number', 'purpose'
    ];
}
