<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed category
 * @property mixed number
 * @property mixed name
 * @property mixed announcement
 * @property mixed description
 * @property mixed title_meta
 * @property mixed keywords_meta
 * @property mixed description_meta
 * @property mixed language
 * @property mixed background_image
 */
class MIA extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'category', 'number', 'name', 'announcement', 'description', 'title_meta', 'description_meta',
        'background_image', 'language'
    ];

    /**
     * @param string $type
     * @return int
     * @throws \Exception
     */
    public function getNumber(string $type)
    {
        if ($type === Page::PAGE_SRVM) {
            $response = 27000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_VISA_SUPPORT) {
            $response = 28000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_RELOCATION) {
            $response = 29000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_REGISTRY_ACCREDITATION) {
            $response = 30000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_TRAVEL_RELAXATION) {
            $response = 31000 + random_int(10, 99);
        }

        return $response;
    }
}
