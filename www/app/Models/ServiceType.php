<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed page
 * @property mixed category
 * @property mixed name_ru
 * @property mixed name_eng
 */
class ServiceType extends Model
{
    use HasFactory;

    protected $fillable = [
      'page', 'category', 'name_ru', 'name_eng'
    ];

    /**
     * @param string $type
     * @return int
     * @throws \Exception
     */
    public function getNumber(string $type)
    {
        if ($type === Page::PAGE_SRVM) {
            $response = 270000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_VISA_SUPPORT) {
            $response = 280000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_RELOCATION) {
            $response = 290000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_REGISTRY_ACCREDITATION) {
            $response = 300000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_TRAVEL_RELAXATION) {
            $response = 310000 + random_int(10, 99);
        }

        return $response;
    }
}
