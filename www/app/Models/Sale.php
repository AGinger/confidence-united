<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed category
 * @property mixed number
 * @property mixed name
 * @property mixed description
 * @property mixed background_image
 * @property mixed language
 */
class Sale extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'category', 'number', 'name', 'description', 'background_image', 'language'
    ];

    protected $dates = [
        'date', 'created_at', 'updated_at'
    ];

    /**
     * @param string $type
     * @return int
     * @throws \Exception
     */
    public function getNumber(string $type)
    {
        if ($type === Page::PAGE_SRVM) {
            $response = 9000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_VISA_SUPPORT) {
            $response = 8000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_RELOCATION) {
            $response = 7000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_REGISTRY_ACCREDITATION) {
            $response = 6000 + random_int(10, 99);
        } elseif ($type === Page::PAGE_TRAVEL_RELAXATION) {
            $response = 5000 + random_int(10, 99);
        }

        return $response;
    }
}
