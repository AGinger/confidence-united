<?php

namespace App;

use Illuminate\Support\Facades\URL;

class Helper {
    public static function changeLang( $new_lang ) {
        //$url                  = request()->route()->uri();
        if (request()->route() !== null) {
            $parameters = request()->route()->parameters();
            $parameters['locale'] = $new_lang;
            return route( request()->route()->getName()??'home', $parameters );
        } else {
            $parameters = [];
            $parameters['locale'] = $new_lang;
            return route( 'home', $parameters );
        }
    }


    public static function getTimeZoneSelect( $selectedZone = null ) {
        if ( class_exists( 'IntlTimeZone' ) ) {

            $regions = array(
                'Africa'     => \DateTimeZone::AFRICA,
                'America'    => \DateTimeZone::AMERICA,
                'Antarctica' => \DateTimeZone::ANTARCTICA,
                'Aisa'       => \DateTimeZone::ASIA,
                'Atlantic'   => \DateTimeZone::ATLANTIC,
                'Europe'     => \DateTimeZone::EUROPE,
                'Indian'     => \DateTimeZone::INDIAN,
                'Pacific'    => \DateTimeZone::PACIFIC
            );

            $structure = [];
            $zones     = [];
            foreach ( $regions as $mask ) {
                $zones = array_merge( $zones, \DateTimeZone::listIdentifiers( $mask ) );
            }
            $zones = self::prepareZones( $zones );

            foreach ( $zones as $zone ) {
                $continent = $zone['continent'];
                $city      = $zone['city'];
                $subcity   = $zone['subcity'];
                $p         = $zone['p'];
                $z         = $zone['z'];
                $timeZone  = $zone['time_zone'];


                $structure[ $timeZone ] = [ $city . ' (UTC' . $p . ')', $timeZone, $p, $z ];
            }

            return $structure;
        } else {
            return [
                'Africa' => [
                    [ 'Test (UTC+5)', 'Europe/Moscow' ],
                    [ 'Test2 (UTC+6)', 'Europe/London' ],
                ]
            ];
        }
    }

    private static function prepareZones( array $timeZones ) {
        $list = array();
        foreach ( $timeZones as $zone ) {
            $time = new \DateTime( null, new \DateTimeZone( $zone ) );
            $p    = $time->format( 'P' );
            $z    = $time->format( 'Z' );
            if ( $p > 13 ) {
                continue;
            }
            $parts = explode( '/', $zone );
            $tz    = \IntlTimeZone::createTimeZone( $zone );
            if ( $tz->getID() === 'Etc/Unknown' or $zone === 'UTC' ) {
                $name = isset( $parts[1] ) ? $parts[1] : '';
            } else {
                $name = $tz->getDisplayName( false, 3, app()->getLocale() );
            }
            $list[ $time->format( 'P' ) ][] = array(
                'time_zone' => $zone,
                'continent' => isset( $parts[0] ) ? $parts[0] : '',
                'city'      => $name,
                'subcity'   => isset( $parts[2] ) ? $parts[2] : '',
                'p'         => $p,
                'z'         => $z
            );
        }

        ksort( $list, SORT_NUMERIC );

        $zones = array();
        foreach ( $list as $grouped ) {
            $zones = array_merge( $zones, $grouped );
        }

        return $zones;
    }
}
