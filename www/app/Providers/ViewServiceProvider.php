<?php

namespace App\Providers;


use App\Models\Option;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            'layouts.head',
            'layouts.footer.footer',
            'layouts.footer.footer-second',
            'layouts.inform-agency',
            'pages.inform-agency.item'
        ],
        function ($view){
            $view->with('metaOptions', Option::getMetaOptions());
        });
    }
}
