include .env

CURRENT_DIR := $(shell pwd)
CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)
CURRENT_USER := $(CURRENT_UID)":"$(CURRENT_GID)
APP_DIR := $(CURRENT_DIR)"/www"

initenv:
	@cp ${CURRENT_DIR}/www/.env.example ${CURRENT_DIR}/www/.env
	@sed -i 's/APP_NAME=confidence-united/APP_NAME=${APP_NAME}/g' ${CURRENT_DIR}/www/.env
	@sed -i 's/APP_URL=http:\/\/localhost/APP_URL=${PROTOCOL}:\/\/${NGINX_HOST}/g' ${CURRENT_DIR}/www/.env
	@sed -i 's/DB_HOST=127.0.0.1/DB_HOST=${MYSQL_HOST}/g' ${CURRENT_DIR}/www/.env
	@sed -i 's/DB_DATABASE=laravel/DB_DATABASE=${MYSQL_DATABASE}/g' ${CURRENT_DIR}/www/.env
	@sed -i 's/DB_USERNAME=root/DB_USERNAME=${MYSQL_USER}/g' ${CURRENT_DIR}/www/.env
	@sed -i 's/DB_PASSWORD=/DB_PASSWORD=${MYSQL_PASSWORD}/g' ${CURRENT_DIR}/www/.env

install:
	@docker-compose build
	@make initenv
	@docker-compose run --rm phpfpm composer update
	@docker-compose run --rm phpfpm php artisan key:generate

uninstall:
	@docker-compose down -v
	@rm ${CURRENT_DIR}/www/.env
	@rm -rf ${CURRENT_DIR}/www/vendor
	@rm -rf ${CURRENT_DIR}/logs/nginx/*.log

migrate:
	@docker-compose run --rm phpfpm php artisan migrate

seed:
	@docker-compose run --rm phpfpm php artisan db:seed

# App
up:
	@docker-compose up -d

start:
	@docker-compose start

stop:
	@docker-compose stop

resetdb:
	@docker-compose run --rm phpfpm php artisan migrate:fresh

php:
	@docker exec -it ${APP_NAME}_phpfpm_1 /bin/bash

php_root:
	@docker exec -u root -t -i ${APP_NAME}_phpfpm_1 /bin/bash